---
title: Presenting Work
tags:
  - portfolio
blurb: null
coverImage: null
date: '2023-10-15T23:25:42+02:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
This website is primarily my portfolio—my way of showing off what I’ve worked on in the past to prove my skills to potential clients. Yet, I find it incredibly hard to do just that: show off my work. The entries online right now are outdated and while I’m still proud of them, I feel like they can’t quite transfer the delightful experience I want them to.

I’ve already blogged about portfolios in general [back in July](/blog/2023/creating-portfolios/), which I guess goes to show how much I think about this topic.

-   Talk about GDA and Print Portfolio
    
-   Talk about wanting a site redesign but still not knowing for sure how to present work
