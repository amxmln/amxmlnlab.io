---
title: Locally Encrypted SPAs
tags: null
blurb: null
coverImage: null
date: '2024-07-09T21:30:19+02:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
Post about using staticrypt to create a locally encrypted password protected website / SPA for a better UX than using HTTP Basic Auth prompts.
