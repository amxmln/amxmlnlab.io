---
title: Taking Dexie to the Cloud
tags:
  - dexie
  - sync
  - review
blurb: null
coverImage: null
date: '2024-04-17T23:12:08+02:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
When I taught my class on advanced webdesign and my students were adding databases to their apps, one question kept coming up: how can I make it sync? And back then, I’d have to tell them: you’d need a server for that and that’s beyond the scope of this course.

Well, not anymore, because my favourite library for working with IndexedDB for storing data in local-first apps just got superpowers. Dexie Cloud was finally released after years of being in development—and now adding sync to a local-first app couldn’t be easier.
