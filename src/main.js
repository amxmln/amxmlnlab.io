// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import DefaultLayout from '@/layouts/Default.vue';

import '@fontsource/inter';
import '@fontsource/inter/700.css';
import '@fontsource/inter/900.css';
import '@fontsource/roboto-mono';
import '@/assets/styles/base.styl';

// eslint-disable-next-line no-unused-vars
export default function (Vue, { router, head, isClient }) {
  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout);

  router.options.scrollBehavior = function customScrollBehavior(to, from, savedPosition) { // eslint-disable-line no-param-reassign
    let position = {};
    if (savedPosition) position = savedPosition;
    else if (to.hash) {
      if (to.path === from.path) return { selector: to.hash, offset: { x: 0, y: 128 } };
      position = { selector: to.hash, offset: { x: 0, y: 128 } };
    } else position = { x: 0, y: 0 };
    return new Promise((resolve) => {
      this.app.$root.$once('triggerScroll', () => {
        resolve(position);
      });
    });
  };

  // Add a custom noscript style tag, fixes g-image and missing noscript support when JS is disabled
  head.noscript = [ // eslint-disable-line no-param-reassign
    { innerHTML: '<style>.g-image--loading{ display: none !important; }</style>' },
  ];
}
