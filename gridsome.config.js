// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

const appData = require('./src/data/apps.json');

module.exports = {
  siteName: 'Amadeus Maximilian',
  siteDescription: 'Hi, I’m Amadeus Maximilian Stadler, a designer and developer from Munich, Germany. This is the right place to see my portfolio or to get in touch with me!',
  siteUrl: 'https://amxmln.com',
  titleTemplate: '%s | Amadeus Maximilian',
  icon: {
    favicon: './src/assets/images/favicon.png',
    touchicon: './src/assets/images/touchicon.png',
  },
  images: {
    defaultBlur: 10,
    defaultQuality: 90,
  },
  outputDir: 'public',
  plugins: [
    {
      use: '~/plugins/source-pwas',
      options: appData,
    },
    {
      use: 'gridsome-plugin-rss',
      options: {
        contentTypeName: 'BlogPost',
        feedOptions: {
          title: 'Amadeus Maximilian’s Blog',
          description: 'The place for my thoughts on various topics surrounding design and technology',
          feed_url: 'https://amxmln.com/rss.xml',
          site_url: 'https://amxmln.com/blog/',
          image_url: 'https://amxmln.com/twitterimg.png',
          language: 'en',
          categories: ['design', 'ui', 'ux', 'web', 'development', 'pwa', 'webdev', 'vue'],
        },
        feedItemOptions: (node) => ({
          title: node.title,
          description: node.blurb,
          url: `https://amxmln.com${node.path}`,
          categories: node.tags,
          date: node.date,
        }),
        latest: true,
        dateField: 'date',
        maxItems: 20,
        output: {
          name: 'rss.xml',
        },
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'Project',
        path: './content/projects/*/*.json',
        resolveAbsolutePaths: true,
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'BlogPost',
        path: './content/blog/*/*.md',
        resolveAbsolutePaths: true,
        refs: {
          tags: {
            typeName: 'Tag',
            create: true,
          },
        },
        remark: {
          plugins: [
            'remark-emoji',
            ['gridsome-plugin-remark-shiki', { theme: 'material-theme-default', skipInline: true }],
          ],
          slug: false,
          fixGuillements: false,
          grayMatter: {
            excerpt: true,
            excerpt_separator: '<!-- more -->',
          },
        },
      },
    },
  ],
  templates: {
    BlogPost: '/blog/:year/:title',
    Project: '/projects/:year/:title',
    Tag: '/blog/tagged/:title',
  },
};
