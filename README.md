# Portfolio V2

This is the latest version of my portfolio website. It’s built using
[Gridsome](https://gridsome.org) and hosted on GitLab Pages.

## Important

Images are stored in Git LFS, so make sure that’s installed before you clone!
