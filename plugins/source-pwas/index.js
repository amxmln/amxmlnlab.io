const fetch = require('node-fetch');

class PwasSource {
  constructor(api, { apps, themeColorOverrides, useCors, typeName }) {
    this.apps = apps || [];
    this.themeColorOverrides = themeColorOverrides || {};
    this.useCors = useCors || [];
    this.typeName = typeName || 'App';

    api.loadSource(async ({ addCollection }) => {
      await this.loadCollections(addCollection);
    });
  }

  async loadCollections(addCollection) {
    const queue = [];

    this.apps.forEach((app) => {
      queue.push(fetch(`${app}/manifest.json`));
    });

    const fetchedApps = await Promise.allSettled(queue);
    const appData = await Promise.allSettled(fetchedApps.map((app) => app.value && app.value.json()));

    const collection = addCollection({
      typeName: this.typeName,
    });

    appData.forEach((app, i) => {
      if (app.value) {
        const node = {
          url: this.apps[i], // works because Promise.all preserves Array order
          name: app.value.name,
          description: app.value.description,
          icons: app.value.icons,
          backgroundColor: app.value.background_color,
          themeColor: this.themeColorOverrides[this.apps[i]] || app.value.theme_color,
          useCors: this.useCors.includes(this.apps[i]),
        };
        collection.addNode(node);
      }
    });
  }
}

module.exports = PwasSource;
