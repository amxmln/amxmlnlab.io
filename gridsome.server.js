// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = (api) => {
  api.loadSource(({ addSchemaTypes, getCollection, schema }) => {
    addSchemaTypes(`
      # Image type with metadata (Image is taken by Gridsome)
      type TaggedImage {
        src: Image!
        alt: String
        caption: String
      }

      type FullWidthImageBlock implements Block {
        template: String!
        image: TaggedImage
      }

      type SingleImageBlock implements Block {
        template: String!
        image: TaggedImage
      }

      type ImageAndTextBlock implements Block {
        template: String!
        image: TaggedImage
        text: String
        imageFirst: Boolean
      }

      type TwoImageBlock implements Block {
        template: String!
        images: [TaggedImage]
      }

      type ThreeImageBlock implements Block {
        template: String!
        images: [TaggedImage]
      }

      type EmbedAndTextBlock implements Block {
        template: String!
        aspectRatio: String
        src: String
        text: String
        embedFirst: Boolean
        showFrame: Boolean
      }

      type EmbedBlock implements Block {
        template: String!
        aspectRatio: String
        src: String
        showFrame: Boolean
      }

      type ComponentBlock implements Block {
        template: String!
        aspectRatio: String
        name: String
      }

      type ComponentAndTextBlock implements Block {
        template: String!
        aspectRatio: String
        name: String
        text: String
        componentFirst: Boolean
      }

      type Project implements Node {
        id: ID!
        title: String
        thumbnail: Image
        hero: Image
        goal: String
        date: Date
        type: String
        status: String
        sortOrder: Int
        website: String
        tags: [String]
        details: String
        blocks: [Block]
      }

      type BlogPost implements Node @infer {
        blurb: String
        edited: Date
        coverImage: Image
      }
    `);

    // This is needed so we can properly associate the Type of each block
    // See: https://stackoverflow.com/questions/52088172/how-do-you-handle-an-array-of-multiple-types-ex-different-content-blocks-in-g
    // It needs to be done like this since addSchemaResolvers can’t find a Block union/interface to add the custom resolver to
    addSchemaTypes([
      schema.createInterfaceType({
        name: 'Block',
        fields: {
          template: 'String',
        },
        resolveType(obj) { // eslint-disable-line consistent-return
          if (obj.template === 'full-width-image') return 'FullWidthImageBlock';
          if (obj.template === 'single-image') return 'SingleImageBlock';
          if (obj.template === 'image-and-text') return 'ImageAndTextBlock';
          if (obj.template === 'two-images') return 'TwoImageBlock';
          if (obj.template === 'three-images') return 'ThreeImageBlock';
          if (obj.template === 'embed-and-text') return 'EmbedAndTextBlock';
          if (obj.template === 'embed') return 'EmbedBlock';
          if (obj.template === 'component-and-text') return 'ComponentAndTextBlock';
          if (obj.template === 'component') return 'ComponentBlock';
        },
      }),
    ]);

    if (process.env.NODE_ENV === 'production') { // ignore unpublished posts in production
      const blogPosts = getCollection('BlogPost');

      blogPosts.data().forEach((post) => {
        if (post.published !== true) blogPosts.removeNode(post.id);
      });
    }
  });

  api.createPages(async ({ graphql, createPage }) => { // eslint-disable-line no-unused-vars
    const { data } = await graphql(`{
      allProject(sort: [{by: "sortOrder", order: ASC}, {by: "date"}]) {
        edges {
          node {
            id,
            path,
          }
        }
      }
    }`);

    data.allProject.edges.forEach(({ node }, i, edges) => {
      const prev = edges[i - 1];
      const next = edges[i + 1];

      createPage({
        path: node.path,
        component: './src/templates/ProjectTemplate.vue',
        queryVariables: {
          id: node.id,
          prevId: prev ? prev.node.id : null,
          nextId: next ? next.node.id : null,
        },
      });
    });
  });
};
