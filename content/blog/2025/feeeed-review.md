---
title: Spicing up your feeeeds
tags:
  - app review
  - feeeed
  - review
  - rss
  - news
blurb: >-
  Sometimes you casually stumble upon a tiny little app and a couple of days
  later you realise how much joy it brings back into your life. feeeed
  (lower-case ‘f’, four ‘e’s) is one of those apps for me.
coverImage: null
date: '2025-01-25T13:30:00+01:00'
edited: '2025-01-26T00:33:13+01:00'
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
Sometimes you casually stumble upon a tiny little app and a couple of days later you realise how much joy it brings back into your life. [feeeed](https://feeeed.nateparrott.com/) (lower-case ‘f’, four ‘e’s) is one of those apps for me.

As some readers may recall, I’ve been searching for [the perfect news feed](/blog/2024/creating-the-perfect-newsfeed/) for some time now. That meant going back to using RSS feeds, which is an excellent, and in my opinion underrated, way of keeping up with a lot of things on the web.

## What I’m Coming From

For a while, I had a somewhat comfortable solution with Omnivore, but not only was that app discontinued in 2024, I also found it lacking as an RSS client. You see, some of the news outlets I like to follow crank out many new articles in a single day. Most of these don’t matter to me at all, especially since they’ve been growing more and more clickbait-y or repeat the same information in a dozen different ways. So the ability to quickly mark a lot of articles as read became essential for me, especially if I didn’t check my feed for a day or two. Omnivore didn’t have that option.

So even before the app was bought out and discontinued, I had switched to NetNewsWire as my RSS client of choice. It’s a great free and open source application for Apple users as it works on iOS as well as macOS and even comes with the option of syncing feeds via iCloud. But even with the handy “mark everything as read” button, checking my feed became a chore.

In NetNewsWire, all posts are shown as a list of titles and excerpts. There’s the option to add the favicon of the website and how much text is displayed, but every item looks the same. There’s nothing to catch the eye but more and more sensational headlines that really say nothing. Waking up to a feed of a hundred of these items or checking my phone after work to find about the same amount again felt like a chore, not a pleasant browsing experience like I was used to from Google’s newsfeed when I still used Android. In the end, I found myself basically just clicking “mark everything as read” after opening the app and knew something had to change.

## Looking for Something Better

I began looking into other options and eventually stumbled across feeeed buried as the fourth or fifth option in one of those endless listicles. It was touted as a breath of fresh air, but with too many features that weren’t RSS. I was sceptical, but I decided to try it anyway, since it was free and the screenshots looked appealing.

Suffice to say that you wouldn’t be reading this post if I regretted my decision.

## Enter feeeed

The app supports an OPML-import, so getting all my feeds from NetNewsWire to it was a breeze. Once that was done, my feeeed (haha) immediately started populating. Initially, I was a bit confused, because I was seeing old posts I had already read in a strange, non-chronological order. A glance into the app’s settings revealed three sorting options for the feed:

-   Best variety (the default)
    
-   Latest
    
-   Latest unviewed
    

The last one is probably what most people would be familiar with coming from other RSS would be familiar with: a reverse chronological feed of unread items, and that’s what I initially went for, too. However, I quickly noticed that that wasn’t what I wanted. feeeed’s “Best variety” sorting option groups multiple articles from the same source into neat little cards, while “featuring” others in larger cards with big images. It truly feels like Google’s “Discover” feed, but my own.

All cards show the date (or time) the article was published, what source they came from, and at least the full heading and sometimes even a small blurb of the content. The UI uses colours throughout which seem to be extracted from the source itself, perhaps from the website’s accent colour or the main colour of the feed icon? Whatever it is, it makes for an incredibly clean and easy to use UI.

## Settings and Reading Experience

Diving into the settings, there’s even more options to truly make the app your own, from custom accent colours and app icons to adding and removing tabs from the bottom bar, changing how the automatic reader view looks and where links open, to how you’d like your feed to be displayed.

The reading experience itself is pleasant as well, the automatic reader mode works well enough even though it isn’t perfect, but there’s always an easy button to open the post in the original website and feeeed is smart enough to remember posts from which websites you’d rather read in their original form. There’s also a third button among the reading options: a summary button, which will use an LLM to summarise articles for you. I’m not the biggest fan of that, but the results seem decent enough, very reminiscent of another app…but more on that later.

Swiping to the left to right from an article closes it, revealing the feed, but swiping right to left opens the next article, as denoted by the little pagination circles at the bottom of the reader view. This makes it very easy to quickly flick through articles like in a magazine, and every swipe is accompanied by nice haptics.

## The Difference Shows

Initially, I thought the feed would be unending, always showing more articles as I kept scrolling down, even old ones, but that wasn’t the case. Eventually, the feed ends with a little message: “That’s all for now ;)” I cannot overstate how refreshing that is. In fact, feeeed keeps track of which items in your feed you scrolled over and marks those as “read” automatically, even without you having to open them, which is so convenient. And if you don’t have time to read an article immediately, all you have to do is long-press it to add it to your reading list.

Another feature helping with the seemingly infinite flood of new content are the daily digests, which (if enabled) show at the top of the feed from time to time and are LLM-generated summaries of the most recent posts. I didn’t find them particularly useful, though, so I quickly disabled them again.

What is very useful, however, is that feeeed not only works as a share target so you can save articles to your reading list from anywhere, it also periodically sprinkles in some of those items on your reading list into your home-feed. That way, your saved articles don’t rot away unseen forever. The only addition to that feature I’m missing is the ability to distinguish between “read” and “unread” items on the reading list, but perhaps that could be added at a later point.

## And There’s More

So far, feeeed sounds like an excellent RSS reader, doesn’t it? Well, it doesn’t stop at RSS. You can add a plethora of other sources to your feed, from YouTube channels to Mastodon and Bluesky accounts. It can even help you study with a flashcards source!

This all seems above and beyond a free app without ads and [no real tracking](https://feeeed.nateparrott.com/privacy). So that and the LLM integrations beg the question: how is it all monetised? I reached out to the app’s creator, [Nate Parrott](https://mstdn.social/@nate), over on Mastodon, but unfortunately all I heard back was that the app uses a “model that’s not so smart / not so expensive” and so is cheap to run.

## Too Good to be True

This app has a focus on design, playful new concepts, delightful haptics, innovative features and (entirely optional) LLM features. Like I mentioned earlier, this reminds me of another free app…Arc. It’s no coincidence, since Nate worked on Arc, and we all know what happened to Arc…

It makes me slightly worried about the future of this application, as all the integrations undoubtedly mean it’ll have to keep being updated to work correctly over time, but those worries don’t keep me from enjoying it right now.

**Update:** after this post was published, Nate reached out to assure me the project is a passion project with no VC funding and may not go on forever (as passion projects tend to do). However, it might be open sourced down the line and should keep working even without constant care, as many features around RSS run on-device and thus are independent from a backend server and other APIs.

The app is a breath of fresh air for my news consumption, a true delight to use. So if you’re in search for the perfect newsfeed, or at least something very close to it, check out [feeeed](https://feeeed.nateparrott.com/).

As always, feel free to let me know your thoughts over on [Mastodon](https://mastodon.design/@amxmln), and thank you for reading!

---

*I am not associated with feeeed in any way or form and was not asked to write this article, nor did I receive compensation for doing so.*
