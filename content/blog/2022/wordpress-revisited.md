---
title: WordPress Revisited
tags:
  - content management systems
  - cms
  - wordpress
  - developer experience
  - learning experience
blurb: >-
  After almost six years, I took another look at WordPress and really tried to
  dive into it this time instead of relying on yet another theme and a bunch of
  plugins. This journey certainly gave me some new perspectives...
coverImage: null
date: '2022-03-31T21:07:00+02:00'
edited: '2022-03-31T21:13:37+02:00'
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
The last time I actively did something with WordPress besides managing little bits of content here and there was in 2016 while I was working in Dublin. Back then I helped build a company website with the BeaverBuilder plugin and was quickly reminded why I don’t like working with WYSIWYG editors.

Fast forward to 2022 and my current employer asked me to familiarise myself with WordPress to take some workload off of our other developer. Thankfully, I was given enough time to truly immerse myself in this behemoth of a content management system and really look beyond the surface instead of learning yet another theme and plugin.

WordPress has been around for so long by now that I won’t go into it beyond stating that it is an extensible and quite flexible content management system that was originally intended to be used as a blogging platform, but has since been adapted to power everything from a single landing page to giant e-commerce platforms.

## WordPress is a Many-Faced Dinosaur

I’ve never made much of a secret of the fact that I don’t particularly like WordPress. It seems like a remnant from a bygone era to me, a relic from the past that is only relevant today because of its quasi-monopoly. Seemingly every second website uses WordPress, it’s a well-known brand, and it has been around for decades, so I guess it’s only natural that clients gravitate towards it.

What irks me is that most people seem to misunderstand what WordPress actually is. I don’t blame them: nowadays it’s really hard to tell. There are plugins and themes that make it seem like it’s a free alternative to services like Wix and Squarespace, where even a non-coder can build any website. On the other end of the spectrum, there are entire agencies dedicated to building custom WordPress experiences for their clients—and really adventurous developers are even said to have used WordPress, the epitome of a monolithic content management system, as a headless CMS powering modern static websites built in React and Vue.

Non-developers on a budget usually want a quick, easy and cheap way to build a website. WordPress can do that. Big corporations want their large websites built on a stack that is reliable and has an ecosystem around it so they can rely on it for decades to come. WordPress can do that. Small and medium-sized businesses want a properly designed and developed website, but also the ability to change the content by themselves so they aren’t tied to an external agency or developer. WordPress can do that, too.

It often feels like WordPress can do everything *and then some*. To me, that also means that it’s hardly ever the right tool for the job, because it doesn’t excel at anything.

Which brings me back to my point that users misunderstand what WordPress is. It is not some holy grail to fix all their problems and anticipate all their needs. It’s not the only content management system out there. It’s not even the only dinosaur in the scene.

## A New Perspective

Despite all my problems with it—its insecurity, outdated developer experience, sluggishness, the community’s over-reliance on plugins, bloat by legacy code, PHP, unreliable documentation, and more—I have recently learned to see it in a new light. As I dove deeper into this behemoth, I began to appreciate the extreme lengths the developers went through to make the CMS as extensible and flexible as it is.

There are hooks for *everything*, allowing a developer to execute custom functions at nearly any point during the lifecycle of the application. There are plugins for plugins (!) made possible by this impressive architecture. Not only can I influence every pixel line of code of the frontend of my project, but the backend as well! I can freely add and remove settings-pages from the admin panel, I can skin everything to make it look like something entirely different, I can finely control who can do what on not only one, but an entire network of websites.

I’m starting to see why there are entire companies who made WordPress the core of their business. Why there are dedicated WordPress developers who do nothing but develop for and with WordPress. I think I understand why this tool—which is completely open-source by the way—powers a majority of all websites. And last, but not least, I can appreciate the absolutely massive amount of work that went into building this project, especially [after building my own CMS](/blog/2021/i-m-building-a-thing).

I doubt I’ll ever make WordPress my go-to tool for everything, but I’m glad to be able to add it to my tool belt for the times that I might need it. It’s always worth trying new things and broadening our own horizon.

What is your go-to tool for website projects? Feel free to let me know over on [Twitter](https://twitter.com/amxmln) and as usual, thank you very much for reading!
