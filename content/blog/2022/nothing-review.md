---
title: I got a new phone
tags:
  - review
  - nothing phone
  - phone review
  - hardware
blurb: >-
  Last month, I finally took the plunge and got myself a new phone: the Nothing
  Phone (1). Now that I've been using it daily for about a month, here are my
  notes on the device itself and my experience.
coverImage: null
date: '2022-08-28T13:15:00+02:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
We live in a world where all the puns on the company *Nothing* have been made, so I’ve refrained from calling this article “I bought Nothing—here’s what I think”, since that wouldn’t be accurate. All I did was buy a Phone (1) from them, which I’ve been using as my daily driver for a month now. Naturally, I have some thoughts on the matter, so read on to learn what I found using the device to be like.

## The Status Quo, or Why I Switched

The smartphone market feels a little asleep these days. Long gone are the days where getting an upgrade actually felt like an **upgrade**. It almost seems like in the past few years the trend was *refinement*. Refinement and gimmicks—although nothing seems to really have stuck.

When I got my last phone, a Xiaomi Mi Mix 3, the gimmick-trend at the time was uninterrupted displays. No notch, no holepunch for the front camera. There were mostly motorised designs as seen on Oppo and OnePlus phones of that generation, and there were purely mechanical solutions like the spring-loaded mechanism on my beloved Mix 3. A phone which at the time I bought it cost less than 400€ and not only came with a quite neat case (not your bog standard rubbery clear case) but also a *wireless charger* in the box.

The phone was built extremely well, the mechanism for the front camera never wore out in the four years I’ve used it, and despite me not actually using the included case (or any case) for long, the ceramic back looks as pristine as it did the day I got the phone. So no, if there ever is an iPhone or a Pixel with a ceramic back, neither Apple nor Google invented that.

While the software had some quirks and I did have some concerns regarding privacy in the beginning—it was my first phone from a Chinese company and the terms and conditions, as well as the privacy policy and the crazy permissions some preinstalled apps asked didn’t exactly inspire confidence that my data was being handled responsibly—most of these issues got resolved in subsequent updates and I never felt like my privacy wasn’t respected (although I obviously cannot know for sure).

I loved that phone and genuinely wish I could’ve used it for much longer. That’s a feeling I haven’t had about a smartphone for a very long time, since there always seemed to be something better, something cooler right around the corner. So what happened?

Well, although something can be really good, nothing is perfect and in the case of my Mix 3 it was the battery life after four years of daily use and the fact that the last security update came in October 2020. In the end, the phone barely lasted for an entire day without being recharged and if I did something like a video call on it, I could actually watch the battery percentage drop as I talked.

## A Change of Plans

One of the things I loved most about that phone was the fact that nothing interrupted that display. Sure, there was a chin that kind of bothered me, but at least that was easier to miss than a holepunch or notch. So my initial plan was to wait with an upgrade until the under-display cameras that have popped up in the last year were good enough to be completely invisible and featured on a phone easily accessible in Europe. Considering how far the tech has come in recent years, I still think that that could happen by 2025, but unfortunately I doubt my poor old Mix could’ve lasted that long, especially since I was feeling more and more concerned about running such outdated and potentially insecure software.

I knew that I probably wasn’t going to get an uninterrupted screen anytime soon, so instead I focussed on other things that were important to me while looking for an alternative:

-   Wireless charging capabilities
    
-   Symmetrical screen bezels
    
-   Something to differentiate itself from the rest of the market
    
-   Solid software and guaranteed updates for more than two years
    
-   At least an all-day battery life
    
-   A sub 600€ price tag
    

You see, I don’t need a flagship processor. I don’t even need the best cameras on the market. I don’t need a 120hz screen. I value build quality, design, and something unusual instead. So technically a mid-range phone would be perfect, but unfortunately I couldn’t find a single one with wireless charging.

Looking at the list above, the only phone that could satisfy those requirements (aside from the price tag) would’ve been an iPhone. And I really considered getting one, for the first time in my life. I liked the idea of five years or more of software updates. Of a really solid build quality. Of symmetrical screen bezels.

Unfortunately, I really dislike iOS. The notification system seems sub par and there’s some other software design decisions that I find questionable, but the real deal breaker for me is the lacking support of progressive web apps. Nearly all my own apps are PWAs and I use some of them on a daily basis—heck, I’m even writing this review in a PWA on my new phone!

I understand why Apple wants to encourage developers to publish their apps in the AppStore, it makes sense from an economic standpoint, but I simply cannot use and support an OS that doesn’t want to properly support PWAs. So I simply won’t get an iPhone as my main phone anytime soon.

## Out of Nothing: Nothing

I knew of the wireless earbuds Nothing makes, and I think I had heard rumors that they were working on a phone, but the launch event completely caught me by surprise. As I watched the event, I was mentally checking off boxes on the feature list in my head and was more than surprised to learn that the phone actually checked all my boxes (minus the uninterrupted screen).

It features:

-   Wireless charging
    
-   Symmetrical screen bezels
    
-   A promise of three years of OS grades and up to four years of security updates every two months
    
-   A gimmick that makes it stand out from the crowd
    
-   Nearly stock Android
    
-   A sub 500€ price tag
    

So I went and bought one.

## Unboxing and First Impressions

The order process was a little weird and for the longest time I wasn’t actually sure it had worked, but eventually, I received a deceivingly small box in the mail. It featured a cool print reminiscent of the phone's transparent back and a unique opening mechanism with a cardboard pull-tab that separated a small lid from the side of the box allowing the phone and a smaller box with a neatly designed SIM tray ejector tool, paperwork and a USB-C cable to slide out.

That was it though, there weren't any extras in the box, no USB-C to audio jack adapter, no case, no charger. A very minimal experience compared to that of the Mi Mix 3, which had an abundance of extras and even a message from the company's CEO. What I can say though was that I liked the Nothing Phone's unboxing experience until I compared it to that of my old phone. Times have changed and while it may not feature as many extras, it still sets itself apart from most other mid-range phone unboxing experiences.

The phone itself felt surprisingly light for its size, but all in all well built except for a mysterious rattle when shook which doesn't seem to come from the buttons as it persists even when those are held down. It very obviously resembles an iPhone, especially when turned off and viewed from the front—so much so that nearly every person that noticed I had a new phone asked me if I had switched to an iPhone.

I don't mind that though, because one of the things I like most of Apple products is their industrial design. The phone has just the right size for my hands, I like how minimalistic it looks with its flat sides, especially in the black variant that I got (although the white one is definitely the flashier of the two as far as I can tell). I never had problems holding it, but I did notice how the smooth glass on the back makes it one of the most slippery phones I've ever owned. It has slipped off my wireless charger once already, causing it to not charge over night and it already is the phone that I have dropped more than any other I have owned before, despite only having it for a month. I kind of wish the back was a matte frosted glass with some texture to it, but unfortunately that wouldn't be possible without ruining this phone's gimmick.

## The Glyph Interface

As soon as it is turned around, it becomes immediately apparent that the Nothing Phone is not a complete iPhone clone. The back glass is transparent, showing a layer of (fake) internals below as well as an interesting glyph comprised of hundreds of little LEDs that can light up in different patterns to show incoming notifications, the charging state, or can even be used as an impromptu fill light in the camera.

Nothing seems very proud of this feature, but it's practically useless for me beyond showing it off to people. Despite the phone coming with a preinstalled plastic screen protector (which was so misaligned and just felt gross to touch that I took it off after a couple of days), I just wouldn't want to place it screen down on any surface. I know there are other people that do that and maybe they can enjoy the flashy light patterns, but for me they are practically useless.

I also charge my phone wirelessly pretty much exclusively since the USB-C port was without fail the first part to break on any phone without wireless charging I owned, so I don't even benefit from the charging state indicator, that I couldn't even get to work reliably when I tried it out.

Yes, the back definitely gives the hardware of the Nothing Phone (1) a much needed touch of uniqueness, but it is no reason to get this phone over another.

## Daily Use

Thankfully, there are other reasons that set it apart from the crowd which show during daily use. Starting with the battery life, which comfortably gets me through two entire days with usually 5 to 15% of battery to spare at the end of the second day.

That being said, the phone is brand new and I am definitely not a mobile power user. I use my phone to read news, reply to messages and the occasional email, to run utility apps and watch a video here and there. I'm sure the battery wouldn't last as long if I played games or recorded a lot of videos, but from what I can tell so far, I feel comfortable that it would last an entire day even with some heavier use.

Speaking of recording videos, I haven't tested the two cameras on the back and the single one on the front very much, but from what I can tell they take good looking pictures with good light and fall flat in darker shots. There is a good amount of natural bokeh on the main camera and videos look sharp and we'll stabilised.

The haptics of the phone are a definite upgrade to the ones on the Mix 3, feeling tight and pleasant, giving the experience a modern touch instead of feeling like an electric toothbrush.

Call quality was great from what I could tell and none of the people I talked to complained about not being able to hear me properly even when I was in noisier environments—but who uses their phone to call people these days anyway, right? 😉

The screen with its symmetrical bezels and 120hz refresh rate is very pleasant to use. It's my first time using a high refresh rate phone and I must admit that I can definitely tell a difference. Things feel snappier and smoother. Colour reproduction is solid from what I can judge and the automatic brightness adjustment works well most the time. The only times the phone blinds me in dark environments is when I use the under screen fingerprint reader to unlock it, as it seemingly brightens the entire screen when shining a light at my thumb to read my fingerprint. Despite that, however, I find having a fingerprint sensor on the front of the device much more useful than on the back.

The holepunch for the front camera in the top left is easy enough to get used to, although the phone would definitely look better with an uninterrupted screen. It's also a shame that the camera is slightly farther away from the left edge of the screen than the top, creating an ugly gap that disturbs the clean aesthetic, but I assume that's due to a technical limitation.

When using the phone for a longer period of time or in more taxing situations like video calls and navigation, it doesn't heat up as much as my old one did, which I find pleasant. Despite it not having a flagship processor, I've also never run into any slowdowns or performance hiccups aside from some reproducible lags that seem more like software bugs to me.

And last but not least, in the month I've been using this phone, it has already gotten three software updates, which inspires confidence that it won't be abandoned in a year or two—although obviously only time will tell.

## Annoyances

While the phone is definitely pleasant to use on a daily basis and does its job well, unfortunately the experience is not as good as it could be, at least for me. Many of these annoyances may be just due to my person preferences, so they may not apply to you.

For example, I find the nearly-stock Android skin rather ugly. I loved Google's first iteration of Material Design, but this whole Material You isn't for me. Things are unnecessarily large, making others hard to reach and the corner radii have gotten out of hand. While I applaud that Nothing has opted to include no bloatware whatsoever, their customisation of the system feels unnecessary and in some places even detrimental to the experience.

For example, they use a dot matrix font for some widgets and in headings in the system settings. This font is extremely hard to read and looks blurry on smaller sizes. It ties in with the more “retro” brand Nothing is trying to establish, but it takes away a lot of the sleekness of the device and seems poorly executed. On top of that, some animations, like the touch-ripple with a TV static effect genuinely made me think there was something wrong with my screen when I first saw them.

Customisation options are also pretty limited and I found them rather useless, while other parts of the UI like the password prompt on the lockscreen just feel unfinished. I really hope future updates will mitigate some of these issues.

Someone coming from another phone with a more stock Android UI might not be as deterred by Nothing's software skin, but I really loved Xiaomi's MIUI and I wish I could have a similar experience on this phone—although I must admit I'm extremely thankful that I can finally use Gboard to enter my password on the lockscreen again, because whatever Xiaomi was using was so horrible I didn't even get used to it after four years.

Something else I liked much better on my Mix 3 was the fact that I could completely hide the gesture bar at the bottom of the screen, making the apps truly full-screen. This is not possible on the Nothing Phone, often causing a black bar to appear housing the white gesture pill, ironically giving the screen a chin, which completely ruins the symmetrical bezels. I'm not going to lie, it bothers me more than it should and I really hope they will add an option to hide it in the future.

From a hardware standpoint, besides the general slipperiness of the phone, I also find the button placement slightly less than ideal. The volume down and power button are pretty much at the same height on opposing sides of the screen, so I have accidentally taken a lot of screenshots while trying to put the phone into standby. The power button also seems a little too sensitive regarding the double press to open the camera gesture, since I have now accidentally opened the camera multiple times now.

Unfortunately, there are also a lot of other little bugs that I came across in my daily use that overall make me feel like I'm using a beta version of a device instead of a final product.

The most annoying of these bugs seems to be an issue with the touchscreen sensitivity, as I have the feeling that the phone has a tendency to interpret single touches as double taps and scrolling motions as single taps. There also have been numerous ghost touches, although they got slightly better after I removed the screen protector.

I also noticed that whenever I'm casting something to my TV from the YouTube app, the phone wants to close the playlist whenever I switch back to the YouTube app for some reason. Also, the double-tap to turn on the screen while it's asleep doesn't seem to work reliably and there's no way to double tap to turn it back off from the lockscreen.

Sometimes, the phone doesn't completely turn on when unlocking it with my fingerprint, causing the display to flicker and refresh at a very low rate until I turn it off and on again.

The worst part is, that I have not found an obvious way to report these bugs and feature requests and did not receive an answer when I tweeted at the support account. I came across a Google Form once, but it asked for a lot of personal data, which seemed excessive and unprofessional. I get that Nothing is a small and new company, but this is an area that definitely should've been well thought out in my opinion.

## Conclusion

In the end, I don't think this phone is as ideal for me as my Mix 3 was. There just are too many little paper cuts to make the experience fully enjoyable. However, I feel like a lot of these issues can be fixed with further software updates and I do enjoy the device enough to stick with it and feel curious about where it will go in the future.

I hope that the software updates will keep coming regularly and many of my complaints will be fixed in time, so I can stick to this phone for at least as long as I did with my last one.

Considering the features it brings to the table and the price it's asking for them, I think this phone could be an option for many people who would tend to reach for a flagship without needing all the features those devices offer at a price tag that is often more than double than that of the Nothing Phone (1).

As usual, if you have any questions or comments, feel free to reach out using one of the channels linked below and thank you very much for reading! 😊

---

*I am in no way affiliated with Nothing, nor was I asked or received any compensation for writing this review. I bought the device myself at its retail price and all the opinions expressed in this post are solely my own.*
