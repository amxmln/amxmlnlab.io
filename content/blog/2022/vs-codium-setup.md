---
title: Using VS Codium
tags:
  - atom sunset
  - developer experience
  - vs codium
  - vs code
  - text editor
  - ide
  - programming
blurb: >-
  Adobe wants to buy Figma and it’s giving me flashbacks to earlier this year
  when Microsoft announced that Atom will be sunset by the end of the year. So
  here’s an update on how I’m dealing with that!
coverImage: null
date: '2022-10-01T19:30:00+02:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
So apparently, [Adobe is going to buy Figma](https://www.figma.com/blog/a-new-collaboration-with-adobe/) next year. Of course, they’re saying nothing will change, it’s all just a big fat new opportunity for both companies to grow, but this transaction reportedly also involves $20 billion.

This whole situation gives me some strong flashbacks to the time Microsoft bought GitHub and promised nothing would change—especially nothing regarding Atom, my beloved text editor and direct competitor to VS Code. Kind of how Adobe XD and Figma are competing directly. Well, we all know [how that went](/blog/2022/goodbye-atom/).

While I’m still hoping that that Adobe × Figma merger won’t happen, there’s nothing I can do about the Atom sunset other than accept it, so I thought I might as well give you an update on how I’ve been dealing with that.

## What I’ve Tried

I’ve already mentioned [Lite XL](https://lite-xl.com) before, which seems like a very nice and capable editor, while at the same time not being too bloated. Unfortunately, it is also still under heavy development. Over the last few months, I’ve played around with development builds a little more, but I missed having robust snippet support, a linter and last but not least syntax highlighting for Vue-components. I still see a lot of potential in this editor, and I’m looking forward to the next release, which will address many of my general UX issues with it (although not the three big reasons I can’t see myself using it I mentioned above).

Other editors that were suggested to my by various sources—and which supposedly are more compatible with my workflow—were Sublime Text and WebStorm, but both come with a subscription-based price tag, which I’m really not a big fan of. I checked out their respective project websites and looked at some forum entries regarding plugins in Sublime 4 and decided that they weren’t for me, at least not until I gave VS Codium a fair shot.

I also tried the “real” VS Code by Microsoft, but I just couldn’t shake the feeling of being watched. It felt too Microsoft-y, even though that’s not exactly the most rational reason for not using it. So the next thing I did was install the VS Codium Flatpak and play around with that. While it was definitely a change when compared to Atom, I could slowly work my way through the enormous list of settings.

Unfortunately, the Flatpak crashed. Not very often, but often enough to be bothersome in a tool that I rely on. So instead, I went with the “unofficial” apt-package version in the end for Linux, which so far only crashed on me once. The DMG package for macOS that I use for work never had that issue.

Once installed and set-up, I played around with a few different themes, which aren’t split into editor- and syntax-themes as they were in atom and cannot do much more than change colours at the moment, which I find a bit of a shame. I wish there was a little more flexibility in that regard.

Other than the few extensions I’ll outline in the next section, I also tried to find a replacement for Atom’s “Advanced New File” plugin, which is a cornerstone of my daily usage, but none of the ones I found worked very well, since VS Code doesn’t support type-ahead autocomplete in input fields, apparently. Developers have tried working around this problem by either splitting the process into two distinct steps (navigating to the directory *then* typing the file name), or just having you manually select entries with the arrow keys and navigating that way.

Out of the two approaches, I prefer the latter one, which can be found in [this](https://marketplace.visualstudio.com/items?itemName=jit-y.vscode-advanced-open-file) plugin, but it is still not quite as smooth as the way it was in Atom, where I could use `Tab` instead of `Enter` to confirm a path selection.

Regardless of these limitations, I’ve settled on a slightly customised version of VS Codium, which I’ve been actively using at work as well as recently on my personal machines as well.

## What I’ve Settled On

For my theme, I chose “One Dark Pro” for obvious reasons (it is closely modelled after Atom’s One Dark theme). I wish the colours in the syntax-highlighting were a little brighter, but I’ll get used to it. You can see how this looks below:

![A screenshot of VS Codium showing the installed extensions on the left and version control with Git on the right](/content/uploads/2022/Bildschirmfoto-vom-2022-09-23-21-28-08.png)

In the screenshot I’ve also opened the list of installed extensions on the left, so you can see everything I’m using at a glance, but usually that’s where I’d have the file-explorer. I’ve hidden the “Activity Bar” and moved the file history (“Timeline”) as well as the Git-Integration to the secondary sidebar on the right. This layout closely resembles how Atom looked (although it doesn’t quite work the same way).

Installing extensions in VS Codium is a little more complicated than in “normal” VS Code, since the Microsoft marketplace is disabled and the Open VSX marketplace doesn’t offer all extensions yet, but once I found out I could simply download the `*.vsix` files from the Microsoft marketplace and install them that way that issue turned into “just” yet another nuisance (which I’m willing to accept for the added privacy).

Thankfully, the “Atom Keymap” extension allows me to keep most of the keyboard shortcuts I’m used to, although I still had to adjust some of the shortcuts to my liking. For example, by default, there’s no way to close the panel at the bottom by pressing `Esc` when it’s opened, say after using the project-wide search feature. It’s subtle UX differences like these that make switching to this editor frustrating for me.

On a more positive note, the “ESLint” and “stylus” extensions allow me to use those two tools pretty much as I was used to, while “Volar” is supposed to make my Vue-development-life easier.

## What I (Strongly) Dislike

So VS Code is supposed to be [the officially recommended editor](https://vuejs.org/guide/scaling-up/tooling.html#ide-support) for developing in Vue, right? How is it then that it has *fewer* features than Atom had, even though the plugin to integrate Vue hasn’t received updates since the beginning of 2020? I don’t really care for intellisense and all the other cruft Volar brings, I care about using Stylus in my `<style>` tags and [that’s not supported](https://github.com/johnsoncodehk/volar/issues/1122), even though Stylus is a supported language for Vue single file components.

Sure, syntax highlighting works, but I’m really used to having autocompletion for CSS properties, especially the more obscure ones. Yes, I could switch to SCSS or Less, but I’ve already made too many concessions with this whole sunset debacle, and I’ve started to notice how all these changes have started sucking the fun out of coding for me.

Another thing I noticed VS Code handles differently is the way fuzzy-matching works during autocompletion. In Atom, I could for example use ‘br’ and hit `Tab` to expand it into ‘border-radius’. That doesn’t work in VS Code, because for some reason it expands into `border-right` instead. I think it’s being too smart about trying to predict what property I mean, instead of just using an alphabetical order.

The same goes for indentation. Perhaps I have something configured wrongly, but whenever I paste lines or VS Code does one of its “smart” automatic code additions, like an automatic import, it messes up my indentation and I have to manually re-indent lines, which is especially frustrating because there seems to be no equivalent to Atom’s “Auto Indent” command.

## Final Thoughts

Phew, I think I really needed to get that off my chest. All in all, using VS Codium works for me, but I’m not really happy with it yet. There’s just too many paper cuts right now. My workflow is much slower with it, and especially the Git integration feels so clunky to use that it makes me want to switch back to using the command line for it.

For now, it’ll do, and I’m sure that I’ll eventually get used to this changed workflow, since I’m facing it every day. Perhaps I’d also do well in stopping the comparisons with Atom and just accept this new reality to reduce the frustration. I’ll keep tweaking my configuration and installed extensions.

And hey, if Figma really should go away or become unaffordable / unusable after the Adobe merger—there’s always [PenPot](/blog/2021/penpot-figma-but-open-source/), right? And I’ll be a little more prepared to switch to a different tool courtesy of this whole Atom → VS Codium experience.

If you’re interested in my exact editor configuration and customised keyboard shortcuts, feel free to let me know on [Twitter](https://twitter.com/amxmln) or [Mastodon](https://mastodon.social/@amxmln) and I may go into more detail in another post or share to the `*.json` files. In any case, as always, thank you for reading—and I’ll do my best to be more on time with my October post, since this one was meant to go online a week ago. :wink:
