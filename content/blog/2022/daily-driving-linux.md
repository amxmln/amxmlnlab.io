---
title: 'My OS of Choice: Linux'
tags:
  - linux
  - os
  - personal setup
  - ubuntu
blurb: >-
  I’ve been using Linux as my daily driver for more than a decade and in light
  of the newest release of Ubuntu, I felt like it was a food time to share my
  experience so far...
coverImage: null
date: '2022-04-29T22:11:00+02:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
With the recent release of Ubuntu 22.04, I think it’s about time I talked about the operating system I’ve used daily for more than a decade by now.

## Way Back When…

I still remember the day I got my first laptop way back in 2006, an old ThinkPad still running Windows XP. I was so scared I might break something that my hands were shaking while I frantically tried keeping up with my computer science teacher at the front of the classroom as they showed us how to set up our new devices. That day marked the start of a long journey that brought me to where I am today: a passionate developer and designer.

But I’m getting ahead of myself. Let’s stay at the beginning. It wasn’t before long that I noticed my classmates were starting to do all sorts of things to their own laptops, like customising the colours of their window decorations, installing alternative launchers and things like that, but they never told me how they did those things and I didn’t know how to find out for myself. Years went by and I grew more and more confident at the keyboard, but that initial wish to customise my operating system never faded.

## Throwing Off the Shackles

So when I learned about this thing called Linux and how it promised ultimate freedom, I was very intrigued. Especially after seeing all the fancy screenshots and 360p screen-recordings of people showing off their awesome desktops. Installing a new operating system seemed far beyond my skillset at the time, however. I didn’t even know what an ISO was!

Luckily, there was a tool called “Wubi”. It was a simple Windows executable that would install a version of Ubuntu and set up a dual boot system, so I could try out the magical world of Linux in a relatively safe environment. Reading and writing speeds were a little slower than a proper install since a Wubi-install didn’t run on its own partition, but I didn’t care: I could finally customise my desktop to my heart’s content and in ways that were almost impossible to achieve for my Windows-bound friends.

I had it all: multiple workspaces and a 3D desktop cube to navigate between them. Windows that wobbled and went up in flames when they were closed. A procedurally generated screensaver that looked just like the code from the Matrix, unlimited GTK and icon themes—I was in heaven and my friends were green with envy.

## Noticing the Limits…

The only thing I couldn’t do in my Ubuntu wonderland was game. Then again, I’ve never been much of a gamer anyway. I usually enjoy the challenge of installing something more than actually playing it. This goes so far that I can easily spend hours doing so, but once it’s running, I play for an hour or less before I get bored and never touch the game again. 😂

So before long, I burned my first proper installation DVD—it would be years before I’d use a USB-drive for my OS installs—and I took the plunge, first installing a proper dual-boot system with an Ubuntu partition, and quickly after wiping Windows off my hard drive entirely.

Around the same time, I really got into open-source software. From OpenOffice (before it was forked into LibreOffice) to GIMP, Inkscape and Blender. I discovered my passion for game and graphic design, even took my very first steps programming in Python in the Blender Game Engine.

## …and not Being Bothered by Them

Of course, I was eventually intrigued by proprietary tools like Cinema 4D and the Adobe Creative Suite—I even wanted to try out the new versions of Microsoft Office because I really liked how the text-cursor moved so smoothly, but I just couldn’t afford them and by the time I had a chance to try them out on school computers or a friend’s device, I had grown so used to the flexibility and freedom of my open-source alternatives that what should be.

When I eventually had to get a MacBook for my studies, I grew terribly worried that I would lose access to all my files if anything happened to that laptop, because by then I was so used to just being able to download and open my projects on pretty much any device, or even from a live-USB environment. I enjoy the hardware Apple builds, but whenever I’m away from my Linux for too long, I notice how I miss the freedom to just make things work the way I want them to.

## A Myriad of Progress

I’m not going to lie, in the early days of my Linux journey I had to tinker a lot to keep things working. My devices froze regularly, embarrassing me during presentations and making me lose work on occasion. But I stuck with Linux—and Ubuntu especially—through those hiccups. I felt how my systems got more and more stable with every release. I was one of the first to give Gnome 3 a chance, and later Unity, when it was still referred to as “Ubuntu Netbook Remix”. I even had my phase of evangelism when I tried converting each an every one of my family members and friends to Linux—my mom still happily uses it every day!

When I grew tired of Unity, I used Ubuntu GNOME, then elementary OS for a time. I installed Arch, Mint, Fedora, but in the end, I always returned to my roots. I watched as GNOME 3 grew more and more polished, needing fewer and fewer tweaks and extensions.

Today, in 2022, using Windows or even macOS and Chrome OS often feels like walking with a crutch compared to Linux. Of course, I’m used to this operating system by now, just as someone who has used Windows all their life is married to that system, so I’m obviously biased. Yet, I still think many people out there could benefit to give modern variants of Ubuntu or other distributions a try.

## Operating System Discrimination

I’m almost *appalled* at how few people seem to take Linux seriously, at least in the consumer market. I know the market-share is tiny and the free and malleable nature of Linux can make it hard to develop and package applications, but the amount of times I have faced downright harassment because of my choice of operating system almost warrants a blog post on its own.

I don’t know how to convince the world that Linux is not just some operating system for nerds and enthusiasts who enjoy doing things from the terminal, but a serious productivity powerhouse. Installing it is as easy as clicking a few buttons and in my experience much less painful than setting up Windows.

It pains me that I simply cannot fully use Linux in my professional life because big companies, who definitely have the funds and development power, refuse to support Linux properly and clients require the use of proprietary file formats and the respective programs to deal with them. I like to think that I *could* make it work, but it’s just not viable in an environment where I have to collaborate with users of proprietary operating systems in my team.

So in the end, all I can do is educate and advocate in the hope that maybe, one day, Linux on the desktop won’t be sneered at and it could truly be “*the year of the Linux desktop*”.

## Home

These days, I don’t customise my installations very much. I don’t have the time to tinker and experiment. I need an environment that just works. Which is reliable, secure and efficient.

Even my five year old laptop still runs fast and smooth with Ubuntu 22.04 on it. It feels like a brand new device. When my nine year old desktop seems to barely be able to run a fresh install of Windows 10, it soars with Ubuntu on it. Not to mention the cheap netbook I got a while back so I could program in bed if an idea struck me in the middle of the night (I miss being a student! 😅).

I’m extremely grateful to all the awesome people that make this operating system possible and who allowed me to embark on this journey. I’m fortunate enough to have tried many different operating systems in my life, but none of them make me feel quite as much at home as Ubuntu does.
