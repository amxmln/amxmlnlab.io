---
title: '2022: A Year in Review'
tags:
  - year in review
  - '2022'
  - personal
blurb: >-
  Here’s my look back at what happened in my life in the past twelve months and
  where I see things going in 2023.
coverImage: null
date: '2022-12-26T16:45:00+01:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
In the wake of the Christmas holidays and with finally more than a couple of moments of free time at my hands, I wanted to keep the tradition I started last year alive and take a look at the past twelve months.

It’s no secret that 2021 wasn’t the happiest year in my life, but I’m glad to say that I’ve made more steps towards my recovery and have found my way back into a semblance of normality. Losing a loved one is never easy, but I’ve had to accept and learn to live with it. It’s still not easy, and the event still ripples through areas of my life, but at least on most days it is just a candle at the back of my mind instead of the all-consuming blaze it was last year.

2022 was the first year I entirely worked full-time after being a student, so I had my hands full. Working with my colleagues at Flavour Communication is a delight, and facing new challenges together brought me forward on both a personal and professional level. Towards the middle of the year, I took on an entire project all by myself and thus got to dip my toes into project management with on top of my usual tasks in design and development.

## Personal Growth and Projects

Being part of the workforce wasn’t the only change this year, however. I also [lost my favourite text editor](/blog/2022/goodbye-atom/), Atom, and had to find [a new home](/blog/2022/using-vs-codium/) in VS Codium (reluctantly). I dipped my toes into [Vite and the Vue 3 Composition API](/blog/2022/let-s-talk-about-vue-3-and-change/), gave [WordPress](/blog/2022/word-press-revisited/) another, more serious chance and expanded my social media presence [into Mastodon](/blog/2022/a-look-at-mastodon/). Most recently, I’ve also started using [Astro as my go-to static site generator](/blog/2022/reaching-for-the-stars-with-astro/).

As I feared at the end of last year, working full-time meant much, much less time to work on my personal projects. Thankfully, I got some time from my employers to forward the development of [Mattrbld](https://mattrbld.com), which enabled me to release a first feature-complete beta version. The documentation of the project also gained a few new sections and is finally nearing completion.

In the summer, I managed to finish and release [Hydrt](https://hydrt.amxmln.com), an app that allows you to track your water intake in order to help you stay better hydrated. There were also a couple of other projects I started, but never found the time and energy to finish, until towards the end of the year I picked up my creative writing app Qami again and [published a new website](https://qami-writer.com) teasing a new release that I’m working on.

## Plans for the Future

This new version of Qami will definitely be my focus on the personal-project-front for next year. I care a lot for the app, and it’s overdue for a new version—I’m definitely hoping that it will revitalise my creative writing habits, which absolutely took a back-seat in 2022. Maybe writing doesn’t give me as much back as it used to, but I’m not quite ready to give up on my writer’s heart just yet.

If I find the time, I’d also like to rewrite this website in Astro, just for practice and a tighter integration into Mattrbld, but it works well in its current state, so that’s not a priority.

In general, I’d also like to just consume less and create more next year, I feel like I fell into a sort of rut where most of my evenings after work were consumed by watching one show after another on various streaming services and it became hard to get back off the couch and do something creative that wasn’t related to my day-job. I have already integrated a “creative hour” into my schedule that I’d like to use for this purpose.

## Lessons Learned on Leisure Time

Not taking any time off in the first half of the year and only doing single-week vacations in the second half taught me that I should definitely plan for and take longer periods of time off next year, even if that means that it’ll be fewer. I’m hoping that’ll also help me feel less stretched thin and low on energy. It definitely made me value my free time much more.

I’m still figuring this part of adult life out, but I guess that’s all just part of the experience, right?

## Signing off for 2022

I hope all of you had good and relaxing holidays and wish you all the best for 2023! Thank you for checking in and taking the time to read my posts. As always, feel free to reach out on [Mastodon](https://mastodon.design/@amxmln) (now on mastodon.design instead of .social!) or [Twitter](https://twitter.com/amxmln) if you’d like to share any thoughts or comments, or have any questions.

I’ll be back next year. :wink:
