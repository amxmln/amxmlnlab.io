---
title: Goodbye Atom!
tags:
  - atom sunset
  - text editor
  - ide
  - programming
blurb: >-
  On June 8th, GitHub announced that they would officially sunset their Atom
  text editor by the end of the year, which sucks. Here are my thoughts on the
  matter, and some alternatives you can switch to.
coverImage: null
date: '2022-06-17T13:45:00+02:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
On June 8th, GitHub announced that they would officially sunset their Atom text editor by the end of the year. You can read the full announcement [here](https://github.blog/2022-06-08-sunsetting-atom/).

This isn’t exactly surprising, since GitHub was bought by Microsoft, which already has their own web-tech-based extensible text editor they would like the entire world to use: Visual Studio Code. Having a fully-open-source and free product alongside that doesn’t make much sense from a business perspective—but it still sucks that this had to happen.

## Why This Sucks

Ever since the acquisition, development of Atom moved at a snail’s pace, despite GitHub originally announcing that they would [keep Atom alive](https://thenextweb.com/news/githubs-new-ceo-promises-to-save-atom-post-microsoft-acquisition) post-acquisition, but at that point Atom was stable and fully featured enough for everything I needed it to do, so I didn’t mind much. Especially, since the plugin-ecosystem still thrived.

I have used Atom ever since it was introduced in 2014 after a long period of searching for an editor that felt *just right* to me. It’s the editor I recommended to my students when teaching web design. Hardly a day passes on which I *don’t* open Atom. Before I built [Mattrbld](https://mattrbld.com), I even used it as my go-to Markdown editor to write the posts for this blog and some of my other projects.

So learning that this beloved editor will no longer be developed felt a little like what I’m imagining being evicted from your home feels like. For me as a developer, choosing my text editor is a deeply personal decision since I basically live in it for most of my work and personal projects.

But losing my “developer home” isn’t the only reason I’m unhappy about this decision by GitHub/Microsoft. VS Code is everywhere these days. Many big projects like Vue even seem to only provide first-party support for it. While Sublime seemed to be all the rage ten years ago, I hardly hear it mentioned anymore. Short of specialised IDEs, I feel like there isn’t much mainstream momentum behind other editors anymore.

## What Alternatives There Are

Nonetheless, I was still able to find quite a few alternatives to Atom after some research, some of which do seem promising to me.

### The Obvious One: VS Code

Duh, of course Microsoft would want you to switch to VS Code. That is probably part of why they went ahead and sunset Atom in the first place!

It arguably *is* the most sensible option to switch to, since it is so similar to Atom in many ways, feature-wise, but also from a technological standpoint. It has a huge community behind it, tons of extensions and plugins available, and it seems to be embraced by many frameworks and libraries in the web development space.

However, there are some aspects I dislike about it (beyond the fact that it effectively killed Atom!), some of which may very well be personal, but others I think are relevant to know if you are considering switching to it.

In my brief time testing it out, it felt very bloated. Just overloaded with features. The UI is cluttered and when I opened the settings, I felt completely overwhelmed by semi-cryptic descriptions and strange defaults.

The Git integration, while present, also feels a lot less elegantly solved than it was in Atom, requiring many more clicks and being kind of opaquely structured. In my personal opinion, VS Code just looks and feels much worse out of the box than Atom did, although in all fairness, there probably are themes and extensions to remedy that to a certain degree.

Microsoft being Microsoft also collects a range of telemetry data from every user of VS Code. Some of that data sharing can be disabled from the settings, but is enabled by default and without the user being aware of it—unlike Atom, which also had an option to collect telemetry data, but asked the user whether they wanted to enable that feature when the editor was first launched.

Last, but not least, VS Code is not actually fully open-source, despite being branded as such. Yes, the source code is available under an MIT license, but the binaries distributed by Microsoft contain proprietary modifications to that source code as well as some exclusive extensions. This smells fishy to me, especially in the context of sunsetting Atom.

Who says that Microsoft eventually won’t make VS Code fully proprietary and start charging with more data or money for it? Something like a FOSS Community-Edition with fewer features and a subscription-based Pro-Edition? I wouldn’t put it beyond them.

### Less Evil: VS Codium

Thankfully, there is a project that wants to provide a truly open soure and privacy respecting version of VS Code called “VS Codium”, an analogy to “Chromium” the FOSS base of Google Chrome.

It basically offers most of the benefits of VS Code without the telemetry and proprietary changes. There are [some quirks](https://github.com/VSCodium/vscodium/blob/master/DOCS.md) as a byproduct of stripping out those parts of the application, but all in all it could be a viable alternative to Atom.

If you are considering switching to VS Codium, you may want to take a look at the One Dark Pro theme and the Atom Keybindings extension to ease your transition—however be aware that there are quite a few differences between the two editors that aren’t solved by those packages!

### It’s Open Source: So Fork It!

Atom’s source is fully available and there have already been forks of the project following the announcement of the sunset (even before it), but unfortunately they seem abandoned or too fragmented to really succeed.

Atom is a huge and complicated project, which requires quite a bit of time and effort and infrastructure to maintain, so unless the community can really behind a single fork and gather some funding, I’m afraid it won’t succeed. (Please feel free to prove me wrong though! The best alternative to Atom would be Atom after all. 😅)

### You Already Have a Text Editor

It’s true, most operating systems come with one or multiple text editors, which technically are enough to program with.

Some of these are:

-   Gedit
    
-   GNOME Text
    
-   Kate
    
-   Notepad
    
-   TextEdit
    

The problem with them is, that they are usually much less fully featured than Atom was and at least for me lack crucial features such as Vue SFC and Eslint support. They may be an option for some of you though!

### The Classics

Of course there are also the old classics: Vim and Emacs. They could probably do whatever I needed them to do, but they unfortunately also aren’t exactly suited to modern tastes, require a lot of custom configuration and have a steep learning curve.

Maybe they are a last resort if everything else fails—it’s very unlikely that they’ll ever be sunset after all! But for now, I’m hoping I can find something more appealing.

While we are on the subject of text editors running in your terminal, there are also Nano and Micro, the latter at least trying to don modern features such as multiple cursors and a more intuitive keymap.

### The Surprise: Lite / Lite XL

[Lite](https://github.com/rxi/lite) and its more fully featured fork [Lite XL](https://lite-xl.com/) were the last editors I came across during my research, but despite the fact that I had never heard of them before, they really surprised me with their speed and simplicity.

They are implemented in Lua and are definitely geared towards more technologically advanced users, but they feel extremely snappy and well thought out. Lite itself is no longer developed, as it is considered feature complete, but Lite XL has some momentum behind it and improves upon Lite by making it more fully featured and adding features like multiple cursors.

As a smaller project, Lite and Lite XL do lack a bit of polish in some areas, but they have quite a few plugins available, for example a linter and a snippets plugin (although I couldn’t get that one to work). I’m especially curious about the upcoming 2.1 release of Lite XL, which addresses some of the issues I have with the current version. I may even write a full review on it when it comes out!

Trying out Lite XL, I felt strangely at home, especially after enabling the One Dark colour theme which was inspired by Atom and adjusting some of the keyboard shortcuts to what I’m used to. I’m actually quite excited about the project!

However, I am afraid that I won’t quite be as productive as I was with Atom should I switch to it. While there is a Vue syntax highlighter being worked on by a community member, it is not fully working because of limitations with the highlighting engine. On top of that, Lite XL lacks the Git integration I’ve come to rely on in Atom, the autocompletion is quite limited and there are more than a few UX paper cuts here and there that I came across in my brief time trying it out.

### Other Options

There are also other options that aren’t really something for me, but you may fund useful, so I have included them here:

-   [Zed](https://zed.dev/), a new editor under development, by the former Atom team focussed on collaboration, but without a public version as of now
    
-   Brackets, a community continuation of the former Adobe project of the same name geared towards simple web development
    
-   Sublime, another old classic
    
-   [Light Table](http://lighttable.com/), which sounds like a photo editor, but isn’t
    

## What I’m Going to Do

For now, I’ll stick with Atom for most of my work, while trying out alternatives in my free time. We still have half a year before Atom reaches EOL and it may continue working afterwards as well, albeit without any (security) updates.

Projects like VS Codium definitely are a real alternative, despite not being quite up to par looks- and UX-wise, but they definitely should have all the needed features. Perhaps something like Lite / Lite XL could also be a real viable alternative, for my usecase at least, but they would require some significant time-investment to implement the plugins (like snippets and HTML autocompletion) needed for me to do my work efficiently.

Unfortunately, I feel tempted by the “it just works” pull VS Code / Codium have. Like I’ve mentioned, it almost feels to me like VS Code is establishing a de-facto monopoly on the extensible text editor market, since most big projects like Vue and Astro only seem to fully support VS Code and not other editors. I don’t like that—but using something like VS Codium definitely feels like the path of least resistance when switching from Atom at the moment.

## Final Farewell

Of course there’s still the hope that a fork of Atom will spring up and keep going strong, but until that happens: goodbye Atom, you will be sorely missed. 😢

As always, thank you very much for reading! 😊 What are your opinions on the subject? What editor are you using? What will you be switching if you were currently using Atom? Did I miss an alternative? Feel free to let me know via [Twitter](https://twitter.com/amxmln) or [Mastodon](https://mastodon.social/@amxmln).

I may write some follow-up articles to this one as the situation develops further. You will find them under the “atom sunset” tag, or linked below when they come out.
