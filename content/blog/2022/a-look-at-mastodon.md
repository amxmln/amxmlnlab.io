---
title: A Look at Mastodon
tags:
  - review
  - social media
  - mastodon
blurb: >-
  The world leaves Twitter for Mastodon! Well, perhaps not quite, but with all
  the talk about it recently, I decided to try out Mastodon for a month and here
  are some of my thoughts and experiences from that time.
coverImage: null
date: '2022-05-31T12:30:00+02:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
![An animation showing the Mastodon logo being added to the social links at the footer of this page](/content/uploads/2022/mastodon-add.gif)

Ever since Elon Musk announced he would buy Twitter, a vocal group of users started announcing an exodus from the platform. They would move over to a different social network that offered a similar experience, but was fully decentralised so no single entity could ever buy and control it. The name of that network is **Mastodon**.

I had heard of it before, even considered making an account, but never did. With the renewed hype suddenly surrounding it, I thought I’d finally give it a shot. So at the beginning of May I chose an instance and took the plunge.

## Into the Fediverse

Mastodon is part of a new breed of decentralised social networks collectively called the *Fediverse*. They are built using the common protocol “ActivityPub” which allows not only different instances of the same network to talk to each other, but also the communication *between* different social networks, as long as they implement that protocol.

This means that there is no central entity hosting and administering the servers for Mastodon. Instead, everyone can host their own server (called an instance) with their own rules and still participate in the global discussion. Users can choose from a variety of instances, many centred around specific topics like art, music, movies and more. Instances are usually free to join, but often rely donations to be kept running.

## Like Twitter but Different

This decentralisation is a major difference from Twitter. The others are the fact that posts (called “Toots”) can contain up to 500 characters by default (although this can be adjusted by the admin of an instance) and that there is no algorithm spreading content. Timelines are ad-free and strictly chronological. Content is only shared (“boosted”) by users and thus your timeline will only contain posts by the people you follow and the content they either create themselves or re-share.

Of course, many Mastodon instances and other parts of the Fediverse claim increased privacy and data security compared to large scale corporations, since their code is open source and they are not selling your data to advertisers. However, keep in mind that you will always be at the whim of the administrator of your chosen instance. They have access to all your content and data and could theoretically misuse it just as much as any large corporation.

Otherwise, the experience of using Mastodon didn’t feel much different than using Twitter to me—until I discovered the local and federated timelines, but more on that later.

## Friendly Faces and a Lot of Silence

Following the suggestions of my chosen instance ([mastodon.social](https://mastodon.social) if you’re curious), I filled out my profile and sent out my first Toot announcing my presence on Mastodon. Unsurprisingly, it didn’t get any attention. 😅

I did send off some more Toots however, which actually did gather a bit of a response, especially those concerning Mastodon or the Fediverse itself. These interactions made me feel strangely at home, they were very friendly and kind of fit the more intimate context of a smaller social network. Nonetheless, I still felt like I was screaming into a void with most of my Toots.

Towards the end of the month, I learned that to really gain some visibility, you have to make use of #hastags, which are searchable (while the text of the Toot is not)—so keep that in mind if you want to build a large Mastodon-following. 😉

Now, I’m everything but an active social media user, so that may be part of why my experience with Mastodon wasn’t very…social. I use them to share the occasional thought, but mostly to keep myself informed by following news-outlets (that’s why I loved Google+ back in the day, Twitter has never quite managed to fully replace it unfortunately). Unfortunately, due to its niche-nature and therefore lack of mainstream media outlets using it, Mastodon will not be able to scratch that itch for me, but that’s okay. I did find it an excellent source of FOSS-related news though!

## The Real Time-Sink

One feature that I found extremely intriguing—and which led to me experiencing a sort of social-media-flow-state for the first time—are the aforementioned local and federated timelines Mastodon offers (but sadly only on the web app). As far as I can tell, these are the primary way to discover new content aside from the typical hashtags. They are basically dynamic lists that show all Toots floating through Mastodon in real-time as they are created.

The local timeline shows all Toots on your home instance, while the federated timeline shows them across all Mastodon instances. The speed at which these lists refresh (and sometimes lose the position you had scrolled to) can be a bit overwhelming, but it makes the social network feel alive and full of content. If you’d like more control, there’s even a setting to enable a “slow mode”, which allows you to manually load new content, but unfortunately that setting spans across all of the different timelines (including your notifications), so it’s a bit of a trade-off.

I find it incredibly interesting to just stare at the local or federated timelines for a while and more than once I stumbled across a really interesting piece of content that way, for example [this thread of a story about the Wright Brothers and the Smithsonian](https://octodon.social/@jalefkowit/108383029727579210).

## Paradox of Choice and Other Issues

Unfortunately, as with many other FOSS projects, Mastodon suffers a bit from fragmentation. New users seem overwhelmed by the sheer amount of instances they can choose from while creating their account and while it’s commendable that there are so many different ways to access Mastodon, these different apps and interfaces often only support a limited set of features, not to mention that different instances of Mastodon may be running different versions of the software.

In my experience, there’s really no way to pick the *right* instance. Simply pick one that seems likeable to you—you’ll still be able to interact with users on other instances. And regarding the app, the web interface certainly seems the most fully featured (and is available as a PWA!), but I found the official Android app to provide a less fully-featured, but more enjoyable and *smooth* experience on mobile.

In general, the UX of Mastodon is a bit less intuitive than on other social networks, at least to me. It took me a scarily long amount of time to find a permalink to share the thread I linked to above (clicking on the date of the post of all things allowed me to open the thread and copy the link to it).

There are a lot of options for adding content warnings to your toots and setting the language and visibility of your content, but it seems a bit opaque to me how exactly they work. And while I think it’s good that there are so many different settings and options (for example to manage your follows and followers, or setting up automatic deletion of your old Toots), I can imagine them being quite overwhelming to non-technical or inexperienced users.

## Final Thoughts

Despite those little drawbacks compared to more established social networks, I think Mastodon (and by extension the Fediverse) is definitely worth keeping an eye on—especially if you, like me, are a fan of free and open source software and concepts.

Trying out Mastodon felt like a genuinely novel experience to me, like I was setting foot into uncharted territory, hunting for adventure and yet untapped potential. I’m not going to lie: it was fun and exciting and I enjoyed the prospect of perhaps reaching another audience and building a small following!

Seeing the posts of the users I had interacted with show up in local or even federated timelines gave me the feeling of being part of a smaller community where I was more than just an anonymous username.

That being said, it is yet another social network to sink your time in—and for me it’s yet another platform I’m probably not going to give the amount of attention I should if I really wanted to use it to its full potential. I’ve always preferred creating over posting about it, but I do genuinely think it does have some value, for example to gain feedback and spread the word of my creations.

Which is why I won’t be deleting my Mastodon account now that this little experiment has ended. Instead, I’m going to try and be more active on it and use it as another outlet for my content, comparable to my Twitter. I’m even going so far as to add it to the list of my contact options in the footer of this website.

If this article made you curious to try out Mastodon for yourself, feel free to join using [my invitation link](https://mastodon.social/invite/rJ5762Dc) and say hi! 😊 In any case, I hope you found this little review insightful and thank you for reading!

---

*I’m not affiliated with Mastodon in any way, nor was I asked to or received any form of compensation for writing this article. These are simply my subjective thoughts and experiences after using the product for a month.*
