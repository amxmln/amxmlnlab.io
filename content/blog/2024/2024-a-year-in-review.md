---
title: '2024: A Year in Review'
tags:
  - year in review
  - '2024'
  - personal
blurb: >-
  Another December means it’s time for another year in review. Here’s some
  thoughts on 2024 and a brief recap of what happened during the last twelve
  months.
coverImage: null
date: '2024-12-29T16:00:00+01:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
It feels like summer was just a week ago, but then again my nose has been running for at least three, so something doesn’t add up. Apparently the pandemic started half a decade ago, but that also feels like last year. Time is flying, the past twelve months have come and gone in a blink, so I want to take this moment to reflect on what happened, where I started and where I am at now.

It feels like 2024 was the year everything calmed down. Finally. There weren’t any big changes, perhaps that’s why it seems like the months passed without me having much to show for it. I feel like I’m in a much better place than at the end of last year, though—so maybe I’m doing something right.

## Is Less More?

The end of 2023 and the first two months of 2024 put me in a spot where I questioned a lot of things about my job, my life, and my hobbies. Throughout the rest of the year, I slowly came to realise that two things are crucial for my own personal happiness: time and space for myself and my projects. Whenever I take on too much, be it work or social obligations, I become unhappy. I need days, sometimes weeks, where I can just be by myself and do whatever I feel like doing. Those times are when my creativity flares up, when I can be excited and productive pushing my own ideas and projects forward.

Overall, that meant I probably created much less this past year, and I do feel a little bad looking at all my side projects and seeing how little progress I’ve made. I don’t think more progress would’ve been worth less happiness, though.

## Work that’s not Work

Speaking of side projects, people seem surprised when I call them “work”, but they definitely are. They’re fun work, but work nonetheless. Every app I build is another piece of software I need to maintain, especially if it has users. Everything I publish is a little more responsibility on my plate. Back when I was a student, that didn’t matter much, because the bites were small and the plate was huge. But now that I’m a working adult that is supposed to be a valuable member of society, the plate only has grown fuller.

So I’ve grown a bit more selective about which projects to pursue and publish. I don’t need to replace *all* the apps on my home screen, maybe just the ones that matter. That doesn’t mean I didn’t work on side projects at all this year, though.

My biggest accomplishment of the year in this space is probably the rewrite and subsequent release of my content management system [Mattrbld](https://mattrbld.com/) as an open source project. In the beginning of the year, I had set this as a goal, and I’m very happy that I could reach it, despite my [fears and reservations](/blog/2024/open-source/) about it. I even got to rebuild the project website and flex my web development muscles with Astro a bit, which was fun.

Other projects moved more slowly, but [Hydrt](https://hydrt.amxmln.com/) and [Untold Stories](https://untoldstoriesmuc.de/) gained a [very slick new back-gesture](https://mastodon.design/@amxmln/112594493335041455) on iOS devices because [I started using an iPhone](/blog/2024/i-bought-an-i-phone/) and couldn’t handle the absolute glitchy mess that are back-gestures in installed PWAs on the platform. It makes me sad in what a poor state PWAs (or rather “installed web apps”) are on Apple devices. Safari truly is the new Internet Explorer, and it’s clear that Apple has no interest in changing that so long as it keeps pushing people to enter their developer program so they can control and police them (and get a good share of revenue even from free apps).

## The Appleification of my Life

It seems paradoxical or maybe even slightly hypocritical, but 2024 was also the year I fully transitioned to Apple hardware for my computing devices. I’ve been using only Macs at work for a while and even did most of my side projects on a MacBook, but I still had a Linux-powered desktop PC. In November, I turned that tower into a dedicated gaming machine and moved it to a different room (mostly so my partner can play The Sims while she’s staying at my place) and cleaned up my desk by switching to a brand new Mac Mini.

This, alongside my iPhone and iPad now means that all my daily drivers come from Apple, and I’m fully invested into their ecosystem. I’m not so sure whether that’s a good thing, but I can’t deny the convenience and quality of their hardware. Now if only the software could match the experience…

## Software Keeps Leaving Me

While I’m slowly coming to terms with the fact that Atom is gone, this year hasn’t been without losses in the software department. The one that probably pains me the most is the loss of [Fable](/blog/2023/animating-with-fable/), an extremely powerful animation app that ran in the browser. There are alternatives that I’d like to explore, but sadly none of the ones I’ve tried so far feel as complete or as intuitive as Fable did. While I’m not a motion designer by any means, I do love to dabble in the field and wanted to do more, but the death of Fable threw a wrench in that.

In my [search for the perfect news feed](/blog/2024/creating-the-perfect-newsfeed/), I also started using Omnivore more seriously, which unfortunately got bought and subsequently shut down by an “AI” company. So I’m on NetNewsWire for the time being and have started using Raindrop.io as my “read it later” service. It works, but it’s not ideal. I might have to look into a better solution next year.

Tumblr, on the other hand, still exists—but *I* decided to leave *it*. I’ve had a Tumblr blog for my creative writing since 2012 and even though I had used it significantly less in the past few years, it was still a great archive of my young-adulthood. So while I had deleted the increasingly enshittified app a while ago, I liked having my works accessible for the people on that platform. Until Automattic started training LLMs with it. That’s when I drew a line and pulled the plug. I’ve been meaning to create a self-hosted archive of all the work since May or so, but unfortunately didn’t ~~find~~ make the time for it yet.

I’ve also recently took the plunge and switched from Chrome to Arc as my primary browser—only to learn that it is being put into “maintenance mode”, i.e. soft-abandoned in favour of a more profitable product, shortly after. It continues to work and receive security updates for now, but who knows for how long. I had a look at an open source alternative, [Zen](https://zen-browser.app/), but while a commendable project, it’s based on Firefox and just not quite there yet UX-wise for me.

## Content Consumption

I did play a lot of games this year, most of them with my partner. It’s just a great way to spend some time with each other without having to think too much. We’re currently heavily invested into Luma Island, which I’ve found to be a very relaxing alternative to the already relaxing farming sim genre, and have enjoyed a bunch of hours in Guild Wars 2 together.

On my own time, I’ve started playing my very first Dragon Age when The Veilguard was released, but haven’t got very far, mostly because the larger part of my gaming time was taken up by the excellent Fields of Mistria.

I also watched a bunch of shows in 2024, but the one that stuck with me most was Pantheon, which I would recommend to anyone who doesn’t mind an animated series and is interested in technology and near-future sci-fi. I would’ve thought Arcane season two would fill this spot for me, but unfortunately I didn’t quite like the second season as much as the first. The visuals are awesome, but the story felt too rushed and a little too philosophical for me in the end.

I rekindled my love for reading last year, and I’m happy to admit that I’ve continued the trend of reading more in 2024 as well. I’ve tried to give a mini review of each book (or series) I read over on [Mastodon](https://mastodon.design/@amxmln), but here they all are in a chronological list:

-   Scholomance Trilogy, Naomi Novik (9/10)
    
-   Fragile Threads of Power, V.E. Schwab (6/10)
    
-   Something Ends, Something Begins, Andrzej Sapkowski (6/10)
    
-   Codex Alera Series, Jim Butcher (8/10)
    
-   Animal Crossing, Kelsey Lewin (6/10)
    
-   Build, Tony Fadell (6/10)
    
-   Buried Deep and other stories, Naomi Novik (8/10)
    
-   The Anomaly, Hervé Le Tellier (4/10)
    

Especially the Scholomance Trilogy stood out to me because it helped me through a tough time *and* inspired me to keep working on a novel I had started a while ago. Unfortunately, that motivation teetered off again around June, but I’ve been wanting to get back to it. It’s been far too long since I last finished a larger creative writing project.

## Switching Years

So yes, 2024 definitely feels like the year things calmed down. It was also the year I [turned thirty](/blog/2024/turning-thirty/) years old. I’ve come to terms with that and some other things, and I think I’m in a pretty good place as long as I can manage to keep making time and space for myself.

In an effort to do so, I haven’t set any fixed goals for 2025 yet. With Mattrbld fully released, I’m hoping I’ll be able to develop some of my current projects more and explore new and exciting ideas. I even got a nice camera, and I’m hoping I’ll get a bit more into photo- and videography. We’ll see.

For now, I want to leave you with a word of thanks for taking the time to read these articles of mine and wish you a very happy 2025! 🎉 Take care and see you next year.
