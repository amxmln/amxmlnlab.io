---
title: I Bought an iPhone
tags:
  - review
  - iphone
  - Apple
blurb: >-
  This could’ve been a very different article—but thankfully Apple came to their
  senses and I can write the review I originally intended to write after buying
  an iPhone for the first time in my life
coverImage: null
date: '2024-03-27T21:15:00+01:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
This could’ve been a very different article—but thankfully, Apple decided at the last possible moment that the update to iOS 17.4 would **not** break installed web apps (Apple doesn’t like calling them PWAs) like they had originally intended. So instead of a rant by a *very* upset me who just bought a *very* expensive phone that couldn’t run a third of the apps I want to use on it daily, I’m writing what I had originally hoped to write when I got myself an iPhone for the first time in my life back in November: a review of the new iPhone 15 Pro Max from the perspective of a long-time Android user.

## Wait, you have an iPhone now?

Yeah, yeah, by now I’ve heard them all. For context: I’ve spent the first two decades of my life being *very* opposed to Apple products. In 2018, I caved and got myself a MacBook for uni, and ever since [it’s been downhill](/blog/2023/am-i-turning-into-a-mac-user/) from there. The truth is that I’ve been working 32 hours a week on a Mac for nearly three years now, and I’ve been doing most of my reading on an iPad for just as long. So I think it is only natural that I eventually started playing with the idea of getting an iPhone, too.

Back in 2022, I bought myself [the first Nothing phone](/blog/2022/i-got-a-new-phone/), and I was quite happy with it for a time (although nothing beats my Xiaomi Mi Mix 3), but then they released the Nothing 2 and the updates for the original one slowed to a crawl. When they finally released the update…it looked awful. Something about the size of the icons, the spacing, the cut-off corner radius of the ‘At a glance’ widget just rubbed me the wrong way. But I kept using it, because there wasn’t anything on the market that I thought would be better, and I had been hoping to use that phone for at least two or three years.

But then Apple released the iPhone 15 in October 2023, and it finally had matte sides and most importantly: a USB-C port. Black Friday rolled around, and I took the plunge after weeks of overthinking it. I bought an iPhone 15 Pro Max and because I love tech and I apparently can’t live without a smartwatch any more, either, I also got the new Apple Watch alongside it (because it’s the only smartwatch that works properly with iPhones). It’s the most I’ve ever paid for a phone—the thing cost more than the MacBook Air I’m using to write this article. Ridiculous.

But yeah, I have an iPhone now.

## The Good

I do have many thoughts about this device, and I’ve been using it daily since November, so I believe I’m fairly used to it by now. A lot of these thoughts are negative, so I want to start with the good ones first. It’s hard. I do not want to rant, but there’s so much to rant about.

### It’s Pretty

Hardware-wise, it’s a gorgeous device. It is a bit on the heavy side, but not heavier (or bigger) than the phones I’ve used in the past five years. I like the matte finish of the frosted glass back, the flat and matte metal side-rails that *almost* match the dark blue colour of my MacBook. They’ve softened the edges a bit, which makes it more comfortable to hold than my old Nothing, and the materials just feel much more *solid*. Which comes as no surprise, considering it cost more than three times as much as my previous phone.

### The Speakers and Screen are Great

I also like the speakers and especially the display. I’m a sucker for thin bezels and can’t stand for anything less than completely even ones around all four sides of the screen—which the iPhone famously has had since the X. With this generation, they are especially thin on the Pro model, so much so that the dynamic island housing the front facing camera and the Face ID hardware almost looks huge in comparison. Surprisingly, I mind that pretty little. Yes, I still wish I had a completely uninterrupted display like on the Mix 3, but I doubt we’ll get there anytime soon, and I enjoy the few but well executed micro interactions the dynamic island provides.

### Battery Life is Awesome

What surprised me most about this phone, however, is the battery life. I can comfortably get through two full days with it, often even three, before I need to charge it. I haven’t had a smartphone that managed this since…well, ever, I think. I’m sure this battery life will degrade over time, but I’m feeling comfortable that even two, three years in, I’ll still get through two full days with it, which bodes well for actually keeping it for the foreseeable future.

Apple has not just promised, but proven that they do update their hardware for at least four years, often even longer. Hardware and ecosystem aside, that was one of the biggest reasons I contemplated getting one in the first place. I don’t have time to keep switching phones, and besides, there’s hardly any innovation happening any more these days. So having a phone that will comfortably (and securely) last for four years or longer is a huge plus for me.

### Focus Modes are kind of Cool

Something else I like are the different focus modes. On Android, I only had the choice to enable or disable do not disturb, while on iOS I have my focus for sleep and the one for work, and they were easy enough to set up and work reliably. It’s a shame that you can’t pause them for a period of time though, for example it would be great to pause my alarm in the sleep focus and the entire work focus for the duration of a holiday without having to remember to manually re-enable it at the end of said holiday. Which brings me to the (many) things I do not like about this phone.

## The Bad

Let’s face it, by now I’m completely indoctrinated into the walled garden that is the Apple ecosystem. It’s cool to be able to pick up phone calls from my laptop, Apple Pay works way faster than Google Pay, and yay, I can finally communicate with those friends of mine that only use iMessage. However, that doesn’t mean I like being forced along the Apple Way™ and whatever ridiculous things they come up with (like killing off PWAs for good). Android was a lot more flexible and even though I hardly ever made use of that flexibility, it gave me a sense of freedom iOS just lacks.

### Oh, Dear the Keyboard

For example, I can install third party keyboard apps, but they do decidedly feel third party—and the Apple keyboard still pops up all the time anyway, making the experience beyond inconsistent. Yeah, it’s probably done for security reasons, but still. So I’m stuck with the stock keyboard, and that keyboard is just ridiculous. How can it be that with all this screen real estate, there is no room for a comma or a period button? Why does switching to the emoji keyboard sometimes switch the layout of the letter keyboard to another language? Why do autosuggestions sometimes not get activated when tapping on them? And how is it possible that a keyboard that supports multiple layouts and a bunch of different languages still struggles with diacritics in 2024? The stock keyboard is shit, and I believe the only people calling it intuitive are those who don’t know how bad they have it.

### Apple Still Hates PWAs

Coming back to PWAs, I find it ridiculous that Apple still doesn’t properly support them. It’s all well and good that they didn’t kill them off completely (although I’m still worried they might at some point in the future, they have set a precedent now), but they’re still severely crippling them by intentionally not supporting features that would make PWAs able to truly compete with native apps. Sure, it makes sense from a business point of view, but native apps also earn Google more money, and yet they actively work on making PWAs more viable on Android. Intentional crippling aside, Safari is the new Internet Explorer and all browsers have to be Safari (technically other engines are supported now in the EU, but somehow I doubt any browser makers will port their engines any time soon, considering how hard Apple made it to do so). There’s just so many quirks, bugs and glitches in that browser… I can understand why iOS users feel like web experiences are so far inferior to native ones. They just never got to enjoy a proper browser.

### No Consistent Way to Go Back

Guess what’s good about the browser, though? It’s the only place in all of iOS that has a consistent gesture to go back (even though the mandatory animation breaks page transitions in PWAs, because of course it does). The fact that in some apps, swiping from left to right works for going back, while in some others you have to swipe from top to bottom and others again only support tapping on a close or back button is possibly the thing that took the longest for me to get used to. Android is not without its issues, but the consistent back button / gesture that works everywhere is one of the most intuitive and practical features it has. And it works from both edges of the screen! Which should be a no-brainer because not everyone has large enough hands to reach all the way to the left edge of the screen of their huge phone.

## The Ugly

### Notifications? More like Nopetifications!

I miss GBoard and I miss my back gesture, but what I miss most from Android are the notifications. The notification system in iOS, iPadOS and watchOS is broken. Notifications are essential to the smartphone experience. Google seems to have got that right. Apple? They don’t even seem to be trying any more. It starts from the fact that I have to go all the way up to the top of the screen and pull down to get to my notifications, which is practically impossible to do one-handed, even with large hands. And you have to pull all the way down, you can’t just take a peek. At that point, it’s easier to lock the screen and immediately reactivate it to look at the notifications on the lock screen.

There, notifications display either as a small number at the bottom, stacks of grouped notifications, or one long list. At least in theory. Neither the number nor the list seem to be working correctly and if you’re using stacks, it’s not exactly clear how notifications are grouped and when they get moved into the “notification centre” that you only get to when swiping to the bottom of the list of stacks. Tapping a stack shows the notifications in the stack, tapping a single notification usually opens the related app. Sometimes, you can long-press a single notification to expand it and even get additional options such as marking a reminder as done or replying to a message. Except there is usually no way of seeing the entire message, because it gets cut off.

Unless the notification came from iMessage, then long-pressing it opens sort of a floating iMessage window, but replying is hard and once you dismiss that the notification is gone as well…seriously? So what I end up doing for the most part is looking if one of the apps on my home screen has a little badge on it, letting me know that there are unread notifications. Which of course leads to plenty of outdated notifications piling up in the “notification centre”, because somehow they don’t get cleared when I open the app they’re from? It’s a mess, and I really think Apple should copy Android’s notification system before they add a more flexible home screen grid or “AI” features to iOS.

### On Fire—in a Bad Way

Technically, the new chip is impressive. It’s built on a 3 nanometre process and should pack a punch (which I probably have never required so far). It’s probably also the reason the battery life is so good, it’s supposed to be really efficient. Except, how can something that produces so much heat be efficient?

And I mean, this thing gets *hot*. There were some reports of it shortly after the release, but Apple published some software updates and nobody talked about it any more, so I thought the issue was fixed. Imagine my surprise when I took a couple of videos with the (excellent) cameras and barely could hold on to my phone after just a minute or two of filming. It was just too hot to the touch.

Fair enough, filming a high quality video is probably one of the most taxing things you can do on a device like that, it’s allowed to get a little warm. But it getting so hot actually makes me worry about its longevity—so somehow I take even less pictures now, even though the better camera was one of my reasons for getting a Pro!

What’s worse is that the phone also gets that hot randomly while browsing the web or doing nothing in particular. It just heats up. It happens less now, but it shouldn’t happen at all. Not with that price tag, and I’m genuinely worried what might happen in summer when ambient temperatures rise above 20 °C. Or if I do ever try and make use of that supposed “gaming” power that gets boasted about in the marketing materials. At least I have a convenient hand warmer in my pocket at all times?

## Coming to an End

There’s so much more I could’ve complained about in this article, like how I do not receive any emails without opening the mail app or how the power button sometimes just doesn’t lock my phone, but most of these paper cuts are actually things I can live with. There’s plenty of those on Android as well, if I’m completely honest, and you just adapt your workflow around them after a while.

In the end, I think if you’re on Android, stay on Android—and if the craving gets too strong, see if you can use a friend’s old iPhone for a week, just a day even. iOS is not intuitive, it just induces Stockholm syndrome because people want to like the phone they just spent a month’s worth of their salary on.

I can’t wait for that to settle in, because I’m stuck with this phone now.

(And yes, I’m aware of how hypocritical that sounds after all this. 😅)

As always, thank you for reading. You can reach me on [Mastodon](https://mastodon.design/@amxmln), if you have any comments or thoughts on this or one of my other articles. Happy Easter, if you celebrate, and until next time!
