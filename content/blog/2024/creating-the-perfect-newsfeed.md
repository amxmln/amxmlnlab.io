---
title: Creating the Perfect Newsfeed
tags:
  - news
  - RSS
  - omnivore
blurb: >-
  Google Feed has become a mess—so I’m looking for alternatives. I’ve started
  using Omnivore to curate a selection of RSS feeds, but that has its own
  challenges. Here are some notes on my search for the perfect news feed…
coverImage: null
date: '2024-01-30T21:45:00+01:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
I’ve been an avid user of Google’s Feed to the left of the leftmost home screen for most of my time on Android. Even when I couldn’t have that because the launcher didn’t support it (like on my Xiaomi Mi Mix 3—nostalgia!), I accessed it through the Google app itself.

And then Google riddled it with ads. And then it started refreshing after I came back to it because I was using an external browser app to read the articles instead of the built-in one. So using it was no longer fun and I started looking into alternatives. I had been wanting to get into RSS and curating my own feed for a long time, after all.

## RSS and Feed Readers

RSS is pretty old by now, and for a time it was quite popular, but then it slid off into obscurity again. However, it never really went away and especially in light of the Twitter debacle and the ensuing decentralisation of news sources, it seems to have gone through a bit of a renaissance.

Others can probably explain it much better than I, but briefly summarised, an RSS feed is an XML document that contains snippets or full articles published on a website, which can be consumed by an app often referred to as a feed reader or news aggregator. Effectively, this means that it becomes possible to “follow” websites all across the web and consume their content from a single place. There are quite a few reader apps out there, with varying price tags and features.

I’ve settled on [Omnivore](https://omnivore.app/), because it can also double up as a “read it later” service and is free and open source. It can even serve as a home for email newsletters, although I haven’t made use of that feature yet.

## Using Omnivore

I actually tried Omnivore before, about a year or two ago, when I lost most of my bookmarked posts on Mastodon because of an overly zealous clean-up algorithm. Back then, my experience was less than stellar. The UI felt clunky and dated and while I’m usually all for PWAs, somehow it just didn’t work right for me. So I deleted my account and left it be.

I briefly tried out Artifact as well, but it just didn’t feel right either, and I went back to the good old Google Feed, until I just couldn’t take it any more. So last November, I made a conscious effort to collect all the RSS feeds of the sites that would get recommended to me in the Google Feed and imported them first into [NetNewsWire](https://netnewswire.com/), a really popular free RSS app, and later into a brand-new Omnivore account.

Omnivore had got better in the meantime and the iOS app even got a pretty nifty update late last year that fixed most of my issues with it, so I’ve actually been using it for reading news for almost two months now. Every day, my “Following” feed is filled with the latest posts from various websites and I sift through them, moving the ones I’m interested in to my inbox. I’ve also started making use of Omnivore’s read-it-later feature—I like the UX of that one, I can simply share a link to the app, and it gets filed away—to slowly replace my habit of storing articles I want to read in my Mastodon bookmarks.

There are some extensive categorising and labelling features as well, which I’m sure I’ve only scratched the surface of, but all in all it *works*. However, I’m far from satisfied.

## Missing Algorithms

You see, while some sites offer pre-filtered feeds, most only have a single RSS feed that is flooded with a ton of clickbait-y articles every day. I end up being interested in a tiny amount of them, while most need to manually be cleared out so I can feel like I looked through everything and haven’t missed anything.

Plus, I only see articles from sites I’m already following—I’m stuck in my own kind of echo-chamber there. It’s a bit frustrating that I cannot feel like I’m at the bleeding edge of things because I have to wait (and hope) that one of the publications I follow picks up on it.

Both are aspects that the Google Feed was really excellent at. It knew what I liked and only showed me some of the content of the sites I am interested in, and it organically surfaced new articles from new sources that I might enjoy, allowing me to stay informed and discover new things along the way.

That’s something I really miss and haven’t been able to recreate by curating my own feed. I never thought I’d miss an algorithm, but I do.

## The Perfect Newsfeed: WIP

I am not happy with how things are now. I haven’t checked the Google Feed in a while now (I might actually have uninstalled the Google App from my phone, I’m not sure), so that’s a plus and I don’t really feel like I’m missing out on anything, but I’m also reading less news and posts than I used to, at least outside of Mastodon.

Managing my feed, manually filtering out content I’m not interested in, just takes too much time and effort. I can’t just browse, because it feels like I’m building up a huge backlog. A way to automatically clear the “Following” list in Omnivore after 24 hours or so could mitigate that, but still, I doubt it could beat the feeling of seeing the right thing at the right time that Google managed to pull off with their creepy tracking of everything I looked at.

Generally, I find it alarming of how every article from a larger publication seemingly is clickbait nowadays. A feed reader can’t fix that. Maybe I’m just following the wrong feeds, but there’s just no joy in it.

## Reading On

For now, I just keep scrolling on Mastodon, which does somewhat surface interesting content organically because of the boosts of some of the people I follow. I do keep bookmarking interesting stuff there as well, although I really should change my habit for good, so those things get saved to Omnivore as well, because bookmarks are notoriously hard to access in most Mastodon apps and might disappear at any moment when the server purges the cache.

I’m convinced the perfect solution is somewhere out there, I just haven’t found it yet. How do you keep up-to-date with things? Feel free to let me know on [Mastodon](https://mastodon.design/@amxmln). I hope you had a great first month of 2024, and I’ll be back with another post soon!
