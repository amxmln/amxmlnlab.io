---
title: Open Source
tags:
  - open source
  - foss
  - personal
blurb: >-
  I’ve been writing software for more than a decade now—yet I haven’t released
  any of it as an open-source project, despite knowing how important that is for
  our society. Here’s why, and why that’s going to change.
coverImage: null
date: '2024-06-29T12:45:00+02:00'
edited: '2024-06-29T12:42:40+02:00'
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
This year is the year I will publish my first open-source project. If you’ve been following along with the development of my headless content management system [Mattrbld](https://mattrbld.com), you probably knew about that already. What you don’t know is why it took me so long to finally give something back to this awesome model of software distribution.

That’s what I want to go into today.

## On Being a Leech – or Feeling Like One

There’s no denying I have hugely benefitted from open-source projects for most of my life. Be it simple access to tools like GIMP, Inkscape, and Blender at a time when I couldn’t afford proprietary solutions, or using a cutting-edge operating system that made me feel at home even on older hardware. All of my own software projects, my entire web development journey—all of it wouldn’t have been possible without great open-source languages, frameworks, and libraries. Not to mention all the knowledge that’s distributed for free online—even under explicitly open-source licences on websites like StackOverflow. And those are only the things I know about!

I’d argue that pretty much all pieces of software these days make use of at least one open-source library. Even on Apple operating systems there’s usually a little note about open-source licences somewhere—they have to include it, even if they do hide it pretty well. So even exempting the FOSS tools, libraries, and knowledge I do consciously use, I’ve been benefitting open-source projects.

And ever since I realised that, I’ve been feeling bad about being a leech and not contributing back enough. Sure, I do try to open helpful issues on the projects I use when I stumble across a bug. I have helped people with their questions in discussion forums, I have even contributed actual code or writing to a couple projects, but in a way that doesn’t feel like *enough*. Not to mention that none of those contributions were of a financial matter.

So in a sense, all I did ever give back to the community were my apps which I built using all those open-source projects I’ve been benefitting from. They’re all usable for free, at no extra cost—but none of them are actually *open-source*.

## Free (as in Freedom) Matters

Yes, my apps all follow my philosophy that the size of one's wallet shouldn’t limit one’s access to technology—but that’s no longer enough in the world we’re living in. Just look at the stunt Adobe recently pulled, holding people’s work hostage to get them to agree to a new set of terms and conditions.

We are far too dependent on these closed-source, proprietary pieces of hardware and software, myself included. I cannot deny there’s a certain draw to the polish and seamlessness of some of these experiences, but I keep having this terrible nagging feeling that I’m making a mistake by locking myself more and more into these walled gardens.

In a perfect world, governments would pay the billions of currency not to these companies, but invest them into open-source software and infrastructure. There are steps in the right direction, but the lobbies are too strong, it seems, very valid reasons such as lacking accessibility in many FOSS solutions aside.

It is no longer enough for a piece of software to be freely (as in beer) available, users should also have the benefit of the transparency and literal freedom that comes with an official open-source licence.

## Yes, but…

Talk is cheap, I can keep converting people to Linux and away from proprietary software all I want, but my statements will sound hypocritical as soon as they notice me using closed-source systems and software—or the fact that my own apps may be free, but are proprietary. Why is that?

I’ve gone over the hardware side of this argument in [other posts](/blog/2023/am-i-turning-into-a-mac-user/), but the reason I haven’t open-sourced any of my software is simply because I’m scared.

When I first got into development, I was terrified that someone would steal my ideas, my code and become rich with them. I was afraid that I would never be able to make a living selling my software if anyone had access to the source. Now that I’m older, I’m not making any money from my software anyway, even though nobody could “steal” it because it’s already free to take. I have realised that I don’t have the time and motivation to do proper marketing, nor have I the patience and bandwidth to deal with all the legal issues surrounding selling something in my country.

That realisation doesn’t help with another fear, however. Once my code is out there, people will *see* it. They will judge it. They might laugh at it, ridicule the architecture, mock redundancies, make fun of inefficiencies. People online can be vicious, especially when they get envious—or, far more likely, frustrated because of a bug or an issue they’re having with my software.

I worry that my code isn’t good enough, clean enough, *perfect* enough for people to look at. I’m afraid that I will get attacked when I won’t have time to work on a project for weeks, months, perhaps even years at a time. I’m scared that I’m simply not good enough to properly manage an open-source project with all the administrative overhead and confrontations that come along with that.

At the end of the day, I just want to have fun building useful software that brings a bit of delight into people’s lives. Open-sourcing that software feels like it might become an obstacle.

## Courage is Facing Your Fears

All of those thoughts and fears only exist in my head. I have no proof that they will happen—I can make educated guesses based on what I’ve seen, but there’s no guarantee that all those things will happen to me.

On the other hand, the benefits of open-sourcing my software will be enormous. Especially for a project like Mattrbld, that will only have a chance of reaching some form of wide adoption if people can be sure that it will keep existing even independently of myself. I understand their need for reassurance and transparency, they are taking a leap of faith by integrating it into their own and their clients’ projects after all.

So I am going to take a leap of faith as well. I will open-source it by the end of the year, no matter if I think the code is clean or good enough to be seen by others. No matter if someone will instantly fork it and make a way more successful project based on my code and ideas.

A lot could go wrong, but I hope by taking this extremely important step, a lot could go right as well.

---

If you have any thoughts, tips, or tricks for open-sourcing projects and not getting overwhelmed after doing so, I’d love to hear about them on [Mastodon](https://mastodon.design/@amxmln). If you want to keep up with the road to open source for Mattrbld, I do have a dedicated [Mastodon account](https://indieapps.space/@mattrbld) for it as well and post updates on the [dedicated blog](https://mattrbld.com/blog).

As always, thank you for reading!
