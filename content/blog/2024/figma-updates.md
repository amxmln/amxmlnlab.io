---
title: Figma Updates
tags:
  - figma
  - product update
blurb: null
coverImage: null
date: '2024-08-30T21:30:00+02:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
I know I’m a bit late to the party, but it took a while for me to get access to the new features—and by the time I did, the generative “AI” to make designs had already been disabled again. Not that I’m terribly unhappy about that, in my experience it takes just about as much time to get a sort-of passable result from a generative transformer model as it does when doing something similar by hand.

Now that I’ve had a little time to play around with the refreshed UI and the newly introduced features, here are my two cents on the latest Figma updates from Config 2024.

## Home is where Figma is

I’ve mentioned it a couple of times already (probably an understatement), but Figma is where 90% of my design work happens these days. Not only do I mostly design for screens, which is its primary purpose, Figma also has some excellent vector editing tools for icon and illustration work. Plus, it makes mood- and whiteboarding really easy. And now there’ll also be presentations? I’m getting ahead of myself.

My design journey started on GIMP and Inkscape, until I eventually moved on to Invision Studio (anyone remember that?). I had a brief stint with Affinity Designer, before settling with Gravit Designer, because it was web-based and thus worked in any of my devices. I was really invested in Gravit Designer, so much so that I was in active communication with their team and regularly gave feedback. Ironically, it was through one of those conversations that I learned about Figma.

I still remember the day I checked out their landing page and learned about vector networks for the first time. I was on my way to catch a train, on my phone, so I couldn’t even try it out for myself. Despite that, I was hooked. So when Corel (of Corel Draw fame) eventually bought up Gravit Designer, I took the plunge and switched to Figma. I have not looked back since.

Well, at least not permanently. As with every successful tech product, I can feel the changes, dare I say it, the *enshittification* settling in. So I have my eye out for projects like [PenPot](/blog/2024/penpot-2-0/), although unfortunately, they aren’t quite there yet.

## Config 2024

Config is Figma’s yearly conference, where they typically present bold new features. While I don’t watch the keynote, I still follow the updates on their blog during that time. Figma has become such a crucial tool on my belt that I like to keep up with what’s changing and, especially, what’s coming.

This year was a big year. And with 2024 being 2024, of course the headlining features revolved around the introduction of “AI”, which ironically had to be pulled out again shortly after release because it was very obviously plagiarising. Thankfully, the generative “AI” features weren’t the only things announced.

Still, having to manually opt out of your own files being used for training said “AI” tasted quite bitter—I’m hoping all of you who use Figma did, unless of course you want that. These choices really ought to be opt-in, although of course then most people obviously wouldn’t, and they’d have far less training data.

## A New Home for Drafts

Something that flew a bit under the radar was a change in how “Drafts”, i.e. files not pertaining to a particular project and not affected by the limits of free plans, are handled.

There used to be just a single “Drafts” section in the Figma dashboard where all drafts were stored. That made a lot of sense, in my opinion. By design, drafts are meant to be something that pertains to you as an individual user, not to a team. Well, not any more.

In an effort to “simplify” and “streamline” things, each team you are part of now has its own “Drafts” section. Apparently, they’re still private and scoped to you as an individual by default, they’re just spread over potentially multiple teams now. How useful! Especially when you’re trying to find something in a pinch.

I hope the sarcasm comes across. It seems like such a weird choice to me, especially since I have two Figma accounts: one for my day job at an agency, where I’m a member of a team and which is a paid account, and a second one that I’m using for my own private projects and aforementioned icon design. That second account doesn’t use any of Figma’s paid features, which are all centred around collaboration and teams. It’s literally just me working on my files.

But with this new drafts system, I had to actually create a team. For just myself. So I could keep working on my files. By myself. A team of one. Certainly takes me back to those school assignments where I did all the work while my “team” focussed on more fun things.

It just doesn’t make sense. Except it does—from a business perspective. Because now I’ll be perpetually reminded that I’m working in a “Free” team by a badge that shows up in every file. Initially, there were even some classic “free plan” limitations on those “new” drafts, although that thankfully got fixed quickly.

So what, big deal, I had to move all my drafts into a newly created team and I can carry on as usual. True, but it just feels like the first step in making a great product a little shittier.

## Sliding into Disappointment

The Figma Slides product is something I got really hyped about when I first read about it. Not only have I been using Figma to design my presentations for a bunch of years now, I’ve also been on a mission trying to convince my colleagues and management that maybe, just maybe, we could save ourselves a lot of frustration if we just switched from having designers build presentations in PowerPoint to designers building presentations in an actual design tool.

Sure, that won’t work for most corporate customers which usually want an editable PPT from us, but at least for internal presentations and our own work that would be great, right? Especially since Figma already stores our design system.

I had high hopes when I first opened that new type of file. Proper presentations with an easy to use “simple” mode for PMs and copywriters, and a switch to get into proper design tools for designers? It sounded too good to be true, which unfortunately, it was.

Granted, the product is in its infancy. I hope, it’ll get better over time. It could be so useful! But when I tried it, there were just too many points of friction. From creating text styles, over importing parts of our design system, to the janky presenter mode, it just wasn’t very fun. And there’s already a price tag looming in the distance! It’s only “free while in beta” after all, so I’m guessing by the time it becomes truly usable and perhaps even slightly polished, it’ll be another $3 per month per seat on our company team. As opposed to PowerPoint, which is part of the Office subscription we’re already paying for anyway.

I’m hoping that at least for individuals, it’ll stay free, just like FigJam did. That way, I could perhaps use it for the slides of the university courses I teach. But until I know for sure, I think I’m going to stick with keeping the slides in my Figma files like I have been doing.

## The New UI

Using Slides, I had a chance to experience the new UI before it got enabled for my regular Figma files as well. It looked really sleek! I liked it.

Then I got it for the design files and while I still think it looks good, it just doesn’t feel as intuitive to use as the previous iteration did. Things seem to require more clicks than before, when using Auto-Layout, it has become harder to see the actual width of an element (although that’s going to be fixed in a future update, apparently). Functionality that used to be easily discovered is hidden behind overflow menus and cryptic icons.

Also, the toolbar is at the bottom now. Why? The bottom of my screen is the part where I look *least* often. It makes the UI look like a blown-up mobile app. Desktop applications are supposed to have their toolbars at the top of the window, right? At least that’s how most other desktop software works.

And why is everything floating now? What was wrong with having distinct areas for pages / layers, canvas, and inspector? Now my design peeks through at the corners of the screen. It offers nothing, but more distraction. Until you enable rulers. Then it also offers a slight headache from facepalming and frustration.

How is the ruler for the content on the canvas *left of the layers panel*? It makes no sense. My eyes and cursor have to travel farther to check the position of my selection and to drag out a guide. I have to drag said guide *over* the layers panel. That just feels wrong.

To be fair, after using the new UI nearly daily for a while now, I’ve got used to most of it. It’s alright, and some of the friction I’m experiencing is because I was just so used to the old one. But as far as redesigns go, maybe they could’ve just stopped at the new icons and input fields.

## So it’s all bad?

No, of course not. This post has got a bit rant-y, but I’m sure there are plenty of little improvements I haven’t consciously noticed. There’s also the new “Suggest Auto-Layout” feature, which can be a real time saver when it works. It has been a bit hit-and-miss for me, but it certainly has that magical *something* to it. Not to mention that Auto-Layout has become such a staple in my work that I do miss it in other software.

For now, I’ll stick with Figma. It is still a great and powerful tool, but I’m getting wary. It doesn’t hurt to keep one’s eyes on alternatives, doesn’t it? That way, I’ll hopefully be ready to switch when the inevitable Config rolls around that will change *everything*. Or you know, when some billionaire sweeps in and buys Figma outright, now that Adobe couldn’t.

If you’ve made it this far, thank you for reading through my rambles! What are your thoughts on the new UI and the direction Figma seems to be taking? Feel free to let me know over on [Mastodon](https://mastodon.design/@amxmln). Take care!
