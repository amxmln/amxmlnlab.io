---
title: Embracing Abandonment
tags:
  - pwas
  - side projects
  - maintenance
blurb: >-
  I love building things—but I’m just a single person with a pretty busy life.
  So sometimes, some of my projects stop getting the love they deserve—but does
  that mean they have to be deleted?
coverImage: null
date: '2024-02-18T18:15:00+01:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
As I grow older, I’m realising more and more how little time I have left besides work, social life and sleeping because I’m always tired. When in the past I could easily publish an app or write a novel a year, these days that’s just no longer realistic.

In addition to that, every new app I create is another responsibility. Apps need to be maintained, they rot and disappear if they don’t get their share of proper attention every year—or do they?

## Should an app without users exist?

Historically, I have not tracked at all how many people were using my apps, let alone what devices my users were using or *how* they were using my apps. Recently, I have added some very light, privacy-respecting analytics, but I’m not entirely sure to what degree the data they report is accurate, it probably is heavily skewed by my implementation and privacy tools built into the browsers of my users.

So I don’t in fact know if my apps are used by anyone beside me at all. I would hope they are, but I cannot know—so would it be fine to shut an app down because I no longer have the bandwidth to properly maintain it if I stop using it? It could potentially rob a lot of users of an app they enjoy and depend on. So to this day, I have not removed an app I had previously published publicly.

## PWAs are just Websites

All my apps (except the legacy version of Qami) are progressive web apps. Websites that get progressively enhanced with more features as those features become available in the browser they’re running in. There are websites out there that haven’t been updated in decades!

PWAs are cheap, or even free to host. They don’t take a lot of bandwidth, they don’t use a ton of power—at least mine don’t. They are just websites with some additional JavaScript and metadata to make them work and feel like native apps. (Unless you’re in the EU and on iOS 17.4 and later—but that’s a blog post for a different time. 😒)

So why should a PWA that is feature-complete and works well not stay online even if it doesn’t receive regular updates any longer?

## But Security!

Yes, security is probably the single most important reason apps need constant maintenance. It would just be irresponsible if they weren’t patched to prevent any exploits that might come up in the future.

That’s why, in my apps, I try to minimise security risks as much as possible. My apps are local-first, don’t store data anywhere besides the user’s device, and most don’t fetch data from the internet at all once they have cached all files necessary to run the app. The only user-generated content that exists within them is that the user created themselves, and they’re very welcome to steal their own data. They’re the only ones who should be doing anything with their own data in the first place!

In fact, all my apps besides Untold Stories, which is more of a social network than an app in the first place, don’t have a backend at all. They’re just static files on a web server. That’s it. No server-side functions or components, nothing that could be broken into and manipulated. Just files on a well-maintained server.

## I Want to Keep Building

Building applications is something I love to do. It doesn’t really matter, to me, how many people use my apps. If they’re useful to even a single person, they deserve to exist. Sometimes, that single person is just myself, sometimes it might be some stranger on the other side of the planet after I’ve long moved on.

I think it’s fine if I leave that app running for that single person for as long as I can. I don’t think there’s any harm in it. Not only that, but I might even feel like coming back to it after a couple of years to add a new feature or fix a new bug!

However, this perspective also limits the kinds of apps I can build. I think that’s why I tend to favour static apps that don’t depend on third parties or external services and APIs. Because those change and break—and I would never want one of my users to see a broken app because I didn’t have the time to update it in time. That’s probably also the reason I’ll never build that Mastodon client I keep dreaming of, even though it would be so cool!

For now, I have more project ideas than I have the time to build, so I don’t think that limit will be much of an issue. And if I do ever end up building something that requires more maintenance than I can give it, there’ll always be the option to release it as an open-source project, so others can pick up the reigns if they want to.

What about you? Do you take side-projects offline if you lose interest in them or don’t have the time to maintain them properly? What do you think about abandoning software in the pursuit of newer, better projects? Feel free to let me know on [Mastodon](https://mastodon.design/@amxmln).

As always, thank you so much for reading—and look forward to my thoughts on the whole Apple vs. PWAs case once I actually get to feel the horror that looms over the iOS 17.4 release. 😉
