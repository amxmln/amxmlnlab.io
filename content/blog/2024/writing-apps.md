---
title: Writing Apps
tags:
  - app development
  - productivity
  - procrastination
  - creative work
blurb: >-
  Me musing about procrastination and creative writing and how that relates to
  my tendency to create new creative writing apps instead of writing creatively.
coverImage: null
date: '2024-11-30T09:45:00+01:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
I just realised: I have only ever written a single novel in my creative writing app, [Qami](/projects/2018/qami/). A single novel probably sounds like a lot to someone who has never written a novel before, but I have written six (or more, depending on how long a story has to be to count) novels already, most of them by hand. So to me, a single one doesn’t sound like much at all. Especially considering how long I spent working on Qami.

And this realisation made me think: what if writing writing apps is just another form of procrastination for me?

## Productive Procrastination

I’ve long been a fan of [procrastinating productively](/blog/2020/productive-procrastination/) by using the hesitation that is typical for long-form projects to instead work on other things. In a way, writing an app to do more creative writing *instead* of doing the actual writing part is the epitome of that.

So that’s what’s happening, right? I’m just procrastinating and instead of watching a movie or playing a video. Answer found, article done.

Except I’m beginning to think it’s not quite that easy.

## Fear of Something New

There’s this lingering thought at the back of my mind that I’m not actually procrastinating the *writing* part of creative writing. I’m avoiding trying something new. And usually if I’m avoiding something, I’m doing so because it’s either uncomfortable, or I’m afraid of it.

You see, designing and developing creative writing apps is something I’m really comfortable with. I’ve been doing it since I started my journey as a designer/developer. Arguably, it’s part of what started that journey in the first place.

By now, I know (or better, I think I know) what makes a good text editor for long form writing. I’ve designed and built several and with each iteration, they’ve become more sophisticated and polished. I’ve tried different libraries and settled first on Quill and later on Prosemirror, the latter being far more complex than the former, but also much more flexible and engineered in a way I’m learning to appreciate more and more.

So why not keep iterating? I’m sure I’ll get back to creative writing and developing other kinds of apps once I’ve built the *perfect* text editor, right? *Right?*

## Writing Writing Apps is Fun

Besides, feeling comfortable with a paradigm and process means I can relax and play with it. The basics are set and working, now I can focus on making things better, adding details and touches that differentiate my solution from the billion others out there.

Put simply, designing and developing creative writing creative apps is something that’s just fun for me. So yes, I’d much rather do that than something else, like figuring out why the inspiration for my latest novel just kind of fizzled out. Or why I haven’t even touched it for half a year, hardly noticing how much time passed since I did.

There’s just one problem. I started working on the next iteration of Qami three years ago, and even went as far as publicly announcing it and overhauling the website two years ago. And I have been working on it! Off and on. Every couple of months or so.

I’m definitely not done developing Qami, but I’m definitely not doing enough to really push the project forward. Maybe I’m just not that into creative writing and creative writing apps any more.

## Growth

Interests change and shift over time. Creative writing has been with me for longer than any other interest. In a way, I feel like it’s a core part of my identity. Perhaps that’s why I’m so hesitant to let it go. I’m that guy who would rather sit in the classroom during recess, working on his novel instead of playing soccer with his friends in the afternoon sun, after all.

Except I’m obviously not that guy any more. I’m no longer in school. I’m no longer filling every moment of spare time with creative writing. I’m no longer finishing a new novel every couple of years.

Sometimes, I get nostalgic and wish I still was that person, but it’s time to accept that I’ve grown past that. And that’s okay.
