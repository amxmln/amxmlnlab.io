---
title: Goodbye Fable
tags:
  - fable
  - software
blurb: >-
  Software I like keeps going away and I don’t have enough time to create
  everything I want to use…the latest entry in this unfortunate saga: Fable, the
  animation app that could’ve been the Figma for motion design…
coverImage: null
date: '2024-10-27T17:30:00+01:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
It’s that time of year again when it becomes time to say goodbye to great tools. Like [Atom a couple of years ago](/blog/2022/goodbye-atom/) (yes, I’m still not fully over that), Fable is going to be shut down next month. And Arc just released a very concerning video.

## Fable, Arc, old video games or what?

Fable is (soon was) a great animation tool built on web technologies. The Figma of motion graphics, so to speak. I wrote [a review of it](/blog/2023/animating-with-fable/) on this very blog. So no, it has nothing to do with the video game series from a while ago.

Arc, not to be confused with Ark the game with the dinosaurs, is a novel web browser which got a bunch of hype over the past few years for its invite-only system, its departure from classic browser UI paradigms and self-archiving tabs.

## Common Ground

Both these projects don’t have much in common, other than perhaps the fact that Fable ran great in Arc (but then again, Arc uses the Blink rendering engine, so that’s not a surprise).

Well, that and the fact that both seem to be going away. [Fable announced](https://www.fable.app/blog/fable-is-winding-down) this month that the app is discontinued and will be shut down completely on November 15th. Arc [released a video](https://www.youtube.com/watch?v=E9yZ0JusME4) on their YouTube channel announcing that the development for Arc 2.0 would be stopped in favour of a completely new app and Arc would continue in maintenance mode (i.e. likely abandoned and eventually shut down).

The reason? Not reaching enough users. Or in other words: not making enough money for their investors. More on that in a bit.

## Fable, the dream come true

Ever since 2017 or 2018 I’ve dreamt of building an animation tool for the browser, using all the goodies of modern web technologies to bring static vector drawings to life. After Effects always seemed like not the right tool for the job and besides, it is prohibitively expensive.

I’m already building cool animations in code with SVG and CSS, they seem the perfect technology for a graphical user interface to do the same more intuitively.

Fable was that dream tool. Well, at least 90% of it. It didn’t use SVG and didn’t even have an SVG export, but it still ran great in the web browser, wasn’t very expensive and allowed me to do nearly everything I wanted with not much effort at all.

I really enjoyed working with it, it really felt like the Figma for animation, the two pieces of software were even well integrated with each other.

When I was using Fable, I didn’t feel like I’d have to create my own version of the app. It was that good! And now it’s going away forever.

## Arc, the browser I wanted to switch to

Arc on the other hand didn’t convince me straight away. I got my hands on an invite a while back (when the app was still invite-only), tried it out, and then left it. It didn’t support PWAs after all, and I wasn’t unhappy with Chrome.

But it did have a lot of UI niceties, polish in unexpected areas. Some genuinely innovative and useful features, like the ability to Shift + Click a link to open it as an overlay over the currently opened tab or split view. A really straightforward and friendly privacy policy. So I kept coming back to it.

Especially on my work laptop, I used it basically daily and so as this year moved along, I began thinking of switching to it permanently. There was a big security issue, which took me aback for a moment, but they seemed to handle it gracefully.

But then they released that video, and now I don’t trust them any more.

## Money, Money, Money (and AI)

Both Fable and Arc leaned heavily into the “AI” hype of the last years. For Fable, that meant a way to create animations in different styles from a simple animated sketch. For Arc, it meant everything from renaming browser tabs to summarising websites and “browsing for you”.

These companies have to go with the times, I get it. And some ideas seem useful, at least superficially. But they’re not that useful in the end, and people certainly won’t be willing to pay a premium for them when they’re used to getting most of it for free with perhaps a little more effort.

I don’t have an insight into these companies’ numbers, but I can imagine that monetising a free, privacy respecting web browser and an animation tool with a very generous free plan in a world addicted shackled to Adobe products is nearly impossible.

Perhaps it would be enough to create a sustainable business out of it, at least in Fable’s case, but in any case not enough to satisfy the thirst for skyrocketing infinite profits expected by VC investors.

And so when even the holy grail of “AI” fails to deliver the desired monetary results, the project gets killed and the users are left with nothing.

## Lost in the Void

I’m not going to lie, I’m pretty bummed about Fable going away. There was a lot I wanted to still do with it, and people both at work and on Mastodon seemed to enjoy my animations. Sure, there are other tools, like [Rive](/blog/2021/rive-animations-on-the-web/) and perhaps [Phase](https://www.phase.com/), but they’re both not Fable, not the nearly perfect animation tool that I am dreaming of. And I just won’t have time to create my own.

No matter how bummed I am, I can only imagine how devastating the announcement must’ve been for the team at Fable, for the people who did put in all this time and effort into the UI design, the code, everything that made this piece of software run. I hope they didn’t lose their jobs!

But what they’ve built most certainly is lost to the void. I doubt any of that code will be released to the public, as nice as that would’ve been. It’s an asset after all, and giving it away would diminish its value, so it’s better to lock it up and seal it away, who knows, perhaps it can be sold another day. Just like those companies who would rather destroy their product than to give it away for free.

## The Ideas Remain

At least with Arc, the software will still be useable for the foreseeable future. They haven’t said they’d abandon it, at least not directly. But we all know how these things go.

Arc, and perhaps even Fable, have already inspired other tools. Their code might not endure, but the ideas can live on. Perhaps there will be alternative projects, like [Zen](https://zen-browser.app/), that could even surpass their sources of inspiration.

I wish I could believe that.

It’s just that I keep seeing great software die, and more and more often it seems to die because of greed. That’s quite disheartening.

So with that, it’s goodbye Fable. I loved using you. About Arc, we’ll see. It just doesn’t seem particularly wise to switch to it fully any more.

Have you lost software you felt really connected to? Feel free to let me know on [Mastodon](https://mastodon.design/@amxmln) and keep creating wonderful things.
