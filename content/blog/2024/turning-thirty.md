---
title: Turning Thirty
tags:
  - personal
  - life update
blurb: null
coverImage: null
date: '2024-05-30T17:00:00+02:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
I turned thirty this month and even though everyone insists on age being just a number, everyone around me also insists on what a big deal this specific birthday is. So I guess it’s only natural that I take a step back and reflect a little bit.

In this past decade, I’ve watched the internet turn from the place to be into a steaming pile of SEO-optimized, LLM-generated rubbish. I’ve met a love of my life and lost her, I’ve forged new friendships and lost others. I learned to love again, earned two university degrees, published half a dozen apps, wrote two or three novels, started working full-time—and still, everything feels hollow.

*How can that be?*

## Building for Machines, not People

Recently, I’ve started thinking about where I want to go in life, what my goals are or should be. Whether I’m on the right path. I have a job, I’m working in an industry I like—*or am I?*

Pushing pixels and writing lines of code is still fun, still something I very much enjoy. But as it’s been taking up more and more space in my everyday life, I’ve also started wondering about the results of all that design and coding work. The end result, not just the journey there.

Some days, it feels like I’m only building the same products every time, making the same decisions over and over, only to increase a company’s reach, their profits. Often, their goals are at odds with what would be good for the actual users, it’s no longer about them, it’s always about the other companies, the ones who control the floodgates of traffic, the ones who pay for clicks. SEO-optimised rubbish, not built for humans, but machines.

I wonder how long it will take companies to realise that it doesn’t matter how high they rank on search indices with their highly optimised pages if the people they’re supposedly trying to reach no longer look at these indices because they want an answer to their question, not ten paragraphs of meaningless blubber stuffed with keywords.

And don’t get me started on how LLMs make these issues exponentially worse.

## The World is Burning

These days, one piece of bad news seems to chase the last. Genocides, record temperatures, yet another mega corporation being caught lying for years. Artists, creatives all around the world are getting their work stolen—something people were fined into oblivion for in the past, but when companies do it at scale, it’s suddenly fine?

I guess what I’m trying to say is: I’m looking for something with purpose in a world that increasingly feels arbitrary.

During my interview as part of the entry exam of my design studies, I was asked what my goal was, my reason for wanting to study design. I said that I wanted to make people’s lives easier by building great user interfaces. Since then, that goal has transitioned into wanting to create real solutions to current problems, giving people a chance to use great software regardless of the size of their wallets, making the world a little better, one app at a time.

I appreciate people pushing the boundaries of science, working on these great problems, thinking ahead ten years into the future, but I’m not one of them. I believe there’s a ton of little problems right here, right now, that individuals like me can take on.

Helping people keeping track of their time. Helping people write great novels and poetry. Helping people keeping their own, independent websites updated.

Those apps won’t revolutionise the world, but they’re still important. Even if only a handful of people ever learn to love them, that’s enough for me.

## Work is Necessary

Unfortunately, that’s not enough for me to live off, though. Ideals don’t fill empty stomachs. I still have to work to pay my bills, to buy groceries that seem to get more expensive every day, to allow myself a little treat here and there.

So I do work, and I try giving it my all. That work seems to be recognised as well, I went from working student to Head of Technology in just five years after all. But what does that even mean?

It seems that as my position gets more important, I get to work less and less on the things that are important to me. It means that I’m more exhausted every night, after more hours, more stress, more responsibilities. And so, there’s even less time to work on what I love—so much so that at times, I’m not even sure of what I love any longer.

## Time, Time, Time

I started playing with web technologies sometime between 2008 and 2009. Back then, it was just for fun, but around 2012, I started taking it more seriously. I had plans for apps, social networks. And over the coming years, during my twenties, I built them. I’ve gained tons of experience in the field, but still, there’s overwhelmingly much yet to learn. Every day there’s something new and not enough time to learn it properly.

Honestly, sometimes I’m worried that I’m unlearning things. I have no idea how I achieved some of the feats I managed in the last decade. Sometimes when I look back at the code, it seems like somebody else wrote it. Someone less tired, less worried, more focussed, more driven.

And it makes sense, many of those achievements happened before I started working full-time in 2022. When I could stay up into the early morning hours to program. When there were endless summers of time to learn, draw and be inspired. I feel like I haven’t created anything meaningful since then.

I keep trying, I really do. But whenever there’s so much as a spark of inspiration, it quickly fizzles out, is quickly smothered by this wave of exhaustion and disillusionment. So I stand before a pile of unfinished work, which is starting to dangerously loom over me. It increasingly feels like I can’t pick up a tiny part of it to work on because it’s just all so overwhelmingly much—and I simply don’t have the time.

I wonder how others do it. I see them, out in the remnants of the internet. Building apps in a single day, being creative, driven, awesome. But I, I feel like I’ve peaked already.

And it makes me feel sad.

## Birthday Blues

For a long time now, I’ve felt sad on my birthday. Sad, because it’s a reminder that yet another year passed and the list of things I want to do, I feel like I have to do, only grew and I achieved nothing.

I know that that’s not true, but it doesn’t make it less sad—and it doesn’t bring back those hours, days, weeks, months. I have maybe forty, fifty years left if the earth lasts that long, and I’m expected to work for at least thirty-seven of them. Can this really be it?

So at the start of this new decade, I find myself facing an uncertain future yet again. Honestly, my twenty-year-old self thought I’d have my life figured out by now. My thirty-year-old self is beginning to think that I never will.
