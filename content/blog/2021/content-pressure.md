---
title: Content Pressure
tags:
  - writing
  - content
  - seo
  - life
blurb: >-
  It’s the end of May and I didn’t want to break my streak of posting something
  every month—but also didn’t have the time for a more elaborate piece, so
  here’s something a bit more emotional…
date: '2021-05-30T13:26:49.803Z'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
I’ve spent far too long thinking about what to call this post—it might be because of the headache I can feel coming, or the fact that I’m not entirely sure what to write to make this read worth your time. Then again, perhaps calling it by what I’m feeling right now is exactly the right way to go—the pressure to create content.

I fancy myself a maker, a creator, and I like building things and solving problems. I even like to teach and share what I’ve learned with others, be it through speaking or writing. Recently, however, I have been so swept up in my [BA project](https://mattrbld.com) and work that I haven’t really had the time for much else.

## External and Internal Causes

When I first launched this website and added a blog to it, I didn’t really have a set schedule in mind, but it just so happened that I ended up writing roughly one post a month and when I noticed that May was almost over, I couldn’t help but feel bad about breaking that rhythm.

SEO resources, social media, and colleagues constantly make it sound like the only way to stay relevant and be noticed is a consistent amount of new content. *Quality* content, mind you, but I’ve always found that hard to quantify on personal blogs such as this one. What doesn’t change, be it a personal blog or a company news page, is the fact that there’s a certain pressure to produce and publish something constantly. Pressure that pushed me to write this post.

I’ll leave it up to you to judge whether this pressure is a good or a bad thing, but it’s not the first time I’ve had to deal with it. I have a second blog in the depths of the internet that I used to keep updated almost daily at its peak. I’m not even sure anymore how I’ve managed that, but I guess I just had a lot more to say. Ever since around 2016, however, my posts over there slowed to a trickle and by now have stopped almost completely—and yet, I still feel bad about not creating and posting more.

## On Changing Times and Preferences

Perhaps it’s the child in me that hasn’t quite given up on the dream of eventually becoming a professional writer that’s nagging at my conscience, perhaps it’s a twisted sense of duty or nostalgia. The truth is, I’m simply not making time to create writing anymore. These days, I create apps and websites and that’s so time consuming that I’d rather spend what little time remains to just chill, watch some TV or play a game every once in a while.

It’s a reality I have to face and that I’m sure will still change and evolve over the next few years as my life transitions from one stage to the next. And yet, the content pressure prevails and on some Sundays I find myself sitting down and producing something after all, just to be able to stick to my schedule. It’s just how I am. I’ll leave it up to you to decide whether it’s worth your time—but know that I’ll do my best to have something more substantial next month.

Until then, thank you so much for dropping by!
