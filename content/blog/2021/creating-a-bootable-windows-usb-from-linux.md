---
title: Creating a bootable Windows USB on Linux
tags:
  - tutorial
  - operating systems
  - os
  - linux
  - bootable usb
  - windows
blurb: >-
  As a full-time Linux user, I rarely have to create Windows installation media,
  but sometimes I do. While it's definitely possible to do without having a
  Windows machine around, there are some pitfalls I'd like to spare you from if
  I can.
coverImage: null
date: '2021-11-22T18:11:11+01:00'
edited: '2021-11-22T18:18:26+01:00'
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
I’m writing this post mostly as a reminder to myself, since I’ve run into this issue twice already and could’ve saved myself quite a bit of time had I remembered to document the process the first time, but I hope it can also be useful to someone else, since most of the information I’ve found so far wasn’t exactly accurate or accessible.

It’s no secret that I’m a full-time Linux user and only very rarely find myself in need of creating a bootable Windows installation medium. While most official support documents I came across seem to suggest otherwise, it’s very much possible to create such a medium without needing access to a device already running Windows.

Here’s how I managed to do it.

## Prerequisites

You will need the following:

-   A USB stick with at least 8gb of capacity
    
-   A PC running a recent Linux distribution (I’m running Ubuntu 20.04 at the time of writing this)
    
-   A Windows 10 ISO
    
-   The `wimtools` package installed
    
-   Patience
    

## Formatting the USB Drive

If you haven’t already, you need to format your USB drive to have a single fat32 partition. It’s **important that it is fat32** and not exFat or NTFS, because in both those latter cases, the PC would not boot from that drive.

Use whatever tool you’re most comfortable with for doing that. On Ubuntu, I like using the “Disks” application that comes preinstalled.

*Be careful when you format your drive, this **will delete everything on it**, so double check that you’re working on the right device!*

## Mounting the ISO

Next, mount the Windows ISO using your file manager so you can access its contents. We will need to copy these files to the newly formatted USB, but since one of the files is larger than the 4GB limit on fat32, it’s not as simple as the good old “select all, copy, and paste”.

For now, you can safely copy all directories and files **except the** `/sources` folder and paste them at the root of your USB drive. While you’re there, make sure to also create a new, empty `/sources` directory on the USB.

Then open the `/sources` folder of the ISO, select all files **except the** `install.wmi` file and copy them into the `/sources` folder of the USB drive.

## Splitting install.wmi

Next, you’ll need to copy the `/sources/install.wmi` file out of the ISO to a location on your computer. **Don’t skip this step, because splitting the file on the ISO itself won’t work!**

Once you’ve done that, you’ll be able to split it with the `wimsplit` command of the `wimtools` package. You can either do it in your local machine first and then manually copy the files later, or do it all in one fell swoop:

```bash
$ wimsplit /path/to/install.wim /path/to/usb/sources/install.swm 3000
```

There are two noteworthy things about this command:

1.  Notice how the file on the USB drive is called `install.swm` instead of `.wim`—this is intentional and actually a requirement for this split to work
    
2.  The number 3000, which denotes how big the largest chunks of the file should be, setting it to 3000 makes sure they’re smaller than the 4GB fat32 limit
    

Also, you should obviously make sure that you copy the split files into the `/sources` directory of the USB, not the root.

## Be Patient

Even on a reasonably modern PC the splitting process took a while—with no obvious output on the console. Don’t lose patience, it might look like it’s hanging, but it most likely is not.

Make yourself a cup of chai and do a breathing exercise, it’s the second to last step.

## Unmount the Drive Safely

Once the splitting command has completed successfully, close all programs that might still be fiddling with your USB drive (Terminal, Disks, etc.), just to be safe and then unmount the drive and wait for your OS to tell you it’s safe before unplugging it.

If everything went right, you should now have a working Windows 10 installation medium that you can use to reinstall Windows if you ever need to.

## Credit Where Credit is Due

Although I don’t remember where I first came across this method, [this Gist by dragon788](https://gist.github.com/dragon788/26921410d8de054366188c5c5435ae01) that I found today holds all the information in this post and more. If you’re interested in a more technical look at things, or are curious how to do this on macOS, feel free to check it out.

Otherwise, I hope this article was useful to you and as usual, thank you for stopping by! 😊
