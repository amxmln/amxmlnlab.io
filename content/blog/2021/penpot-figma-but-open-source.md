---
tags:
- design
- screen design on Linux
- app review
published: true
date: 2021-02-04T12:30
title: "Penpot: Figma, but Open Source?"
blurb: I accidentally stumbled over Penpot, an open source, browser-based UI design
  tool that recently released its first public alpha. Here are my thoughts and first
  experiences with the product.
edited:
---

I’ve always felt like there was no real UI / Screen Design tool for Linux. When
I first got started, I used [Inkscape](https://inkscape.org), then switched over to [Gravit Designer](https://designer.io) for
brief time before falling in love with [Figma](https://figma.com) and sticking with it for the past
three years or so.

A while ago, [Akira](https://github.com/akiraux/Akira) was announced. Unfortunately development is moving very
slowly and while it certainly would be a valuable addition to the Linux ecosystem,
it seems to be following in the footsteps of Sketch, which in my opinion feels
rather clunky when compared to something like Figma. Interestingly enough, however,
it was through Akira’s Twitter account that I learned about [Penpot](https://penpot.app) and that it would be
launching its first public alpha on Feburary 2nd 2021.

Penpot is an open source screen design tool for your team. It is web-based, like
Figma, and builds upon SVG as the file format, like Inkscape—and the comparisons
to the two don’t end there. If I had to explain Penpot in a single sentence,
I would say: it’s what would happen if Figma and Inkscape had a baby.

![The Penpot User Interface](/content/uploads/2021/penpot.jpg)

## The UI

If you’ve ever used Figma, you’ll feel right at home in Penpot’s UI. From the first
screen you see after logging in, to the actual workspace, the components and styles
system and especially that colour-picker, it’s all *very* familiar. Assets and
Layers to the right, design and prototyping properties of the currently selected
object on the right. In the centre a nearly infinite canvas with full support for
artboards (called Frames in Figma).

Unlike Figma, the tools are at the far left of the screen, making good use of the
additional horizontal space on today’s widescreen displays – that is if the top
bar wouldn’t still be there to show who’s currently working on the file. This
way it seems like the arrangement is taking up more space than it needs to. One
thing I appreciate a lot over Figma, however, is the dark-themed UI. While I got
used to Figma’s stark whites eventually, I still would love to have an option for
a dark theme there…

There are a lot of rough edges, but considering it’s an alpha release, those were
to be expected. I feel like in general fonts could be slightly bigger and while
there are some animations in some places, they all still feel too abrupt and incoherent.
The UI also seems to mix native input fields and custom ones in places like the
font-family and -size selectors, which felt quite jarring.

I was impressed, however, how smoothly I could move the canvas, zoom in and out,
and transform objects. That arguably really important aspect worked very well.
I also liked the little onboarding shown after logging in for the first time,
although it should probably show something different than reiterating the contents
of the landing page in the future. Having a couple of files to play around with
from the get-go was also a nice touch—just like that cute little loading animation
when opening a file.

## The Tools

Once I started trying out the different tools, the fact that it was still an alpha
really started showing. While there are basic rectangle, ellipses, pencil and pen
tools, they only manage the bare minimum. I found no way to round corners of a rectangle
individually, or only draw a half-circle (the lack of boolean operations making
this shape impossible to achieve without drawing it with the pen tool). The pencil
tool had an odd offset to it and while the pen tool worked as expected the lack
of a preview while manipulating the control handles of a node made it very hard to use.

There was also this strange quirk that while creating a new shape, holding the
**Ctrl** key would lock the aspect ratio, but when resizing an existing
object, it was the **Shift** key. I also didn’t find a way to resize an object from
its centre.

I might be spoiled by Figma’s excellent tools, and I’m certain that going with
SVG instead of a proprietary file format presents a challenge, I won’t be able
to use Penpot for any serious work until its tools get improved (which I’m sure
they will over time).

## Colours and Effects

I was impressed to see an option to have strokes set to the inside or outside of
a shape instead of it being centred, which is notoriously hard to do in SVG.
Unfortunately, it seems like the centred stroke is simply being masked when it’s
set to display only on the inside, causing its width to be halved.

The colour picker seems very solid and gradients seemed to work well, the handy
colour palette at the bottom of the screen bringing back some strong Inkscape
vibes from back in the day. I also appreciate the option for a drop shadow, although
it unfortunately does not support a negative spread value.

## Closing Thoughts

The open source world, and Linux especially need better screen design tools.
Inkscape is really powerful, but it leans itself more towards illustration and
schematic drawing than UI design—it’s definitely possible, but it takes unnecessarily
much time. A component-based approach (which Penpot seems to fully support, including shared libraries!)
lends itself well to the way modern applications are built and designed. And while
Inkscape keeps getting better and better, it doesn’t support this workflow yet.

I have also long believed that SVG should be the format used for screen designs.
Not only for its compatability with the web, but also for its open nature. I know
that I can open an SVG in almost any vector drawing application, which leaves me
the choice of which tool to work with. Penpot manages to do things with SVG that
I didn’t think possible, like supporting multiple pages in a single document and
having individual artboards on a page!

I believe Penpot is a great step in the right direction and while it’s true that
it can’t hold a candle to Figma, I think it’s only a question of time, if it keeps
improving and the open source community steps up—just look at what Blender turned
into in the past few years.

I am a little sad that it’s a hosted application that requires a server to run
properly, but considering their focus on a team-based workflow, that’s understandable.
However, it also means that you’ll have to [create a free account](https://design.penpot.app/#/auth/register)
(you can also sign in with Google) if you’d like to try Penpot out for yourself—and you definitely should.

---

*I am not associated with Penpot in any way or form and was not asked to write
this article, nor did I receive compensation for doing so.*

Thank you for reading! If you have any thoughts you’d like to share, feel free
to reach out to me on [Twitter](https://twitter.com/amxmln).
