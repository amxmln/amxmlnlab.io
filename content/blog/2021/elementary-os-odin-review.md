---
title: elementary OS Odin Released! Rejoice?
tags:
  - os review
  - elementary OS
blurb: >-
  elementary OS is one of those projects that I keep coming back to. I really
  want to like it—but there’s always a “but”. I’ve been running their latest
  release for almost a week now and here are my thoughts on it…
coverImage: null
date: '2021-08-16T09:36:26+02:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
Before anything else, let me preface this by saying that I really appreciate what the elementary OS team is trying to accomplish. Their pay-what-you-can model is something that really resonates with me and I wish more services would allow for that. I’ve been a long-time fan of the project since the *Luna* release and have recommended the distribution to family members and peers alike. I even ran it as a daily driver for a while back in the *Luna* / *Freya* days.

I respect all the hard work the developers are putting into it and realise that it’s a small team with lofty goals. I have tried every release (and beta-version), but unfortunately there were always things that kept me from fully switching to it in the past few years. Although some of my words in the following review might sound harsh, I don’t mean any disrespect towards the developers, team and larger community around elementary OS, I’m just sharing my honest thoughts and opinions about their latest release: Odin.

## What is elementary OS?

With that out of the way, here are my experiences with elementary OS 6 and some general thoughts on the project. I’ve been debating whether or not to start with the negative aspects, but I feel like after that introduction we all want to hear some good things first. 😉

For those unaware, [elementary OS](https://elementary.io) is a Linux distribution with a strong focus on an inclusive and cohesive user experience. It’s often (and unjustly) likened to Apple’s macOS because of its looks (there’s a dock at the bottom and the window decorations are a silvery-grey) and slight “walled garden” approach—more on that later. In fact, elmentary don’t see themselves as “yet another Linux distribution” but as an independent operating system built on top of Linux, at least as far as I can tell. In short: it’s a replacement for Windows, macOS, or other Linux distributions geared towards desktop users.

## A New Release

Unlike other Linux distributions, elementary OS doesn’t follow a strict release schedule, instead opting to release software “when it’s ready” and not shying away from adding new features in the middle of a release cycle. This seems to work well for them, as every new major release so far has been surrounded by quite a bit of hype and interest, while releases from other projects such as Ubuntu generally don’t cause quite as much of a stir.

Their latest release, version 6 codenamed “Odin” was just released, following a short open-beta phase and close to a year of pay-walled early-access. I installed it on my laptop on release day (August 10th) and have used it for the past six days for some general browsing and a bit of productivity.

## The Good

elementary OS has always had quite a few things that I’ve always really liked about it and this newest release managed to squeeze in even more. These features and philosophies are enough to make me really want to love the OS and are the reason I keep up with the project and try out every new release, despite not running it full-time.

### Excellent Communication

One of the stand-out aspects of elementary OS to me has always been their communication. They have excellent write-ups about their design and development process over on [their blog](https://blog.elementary.io) and even on Twitter they post regular smaller updates and teasers, which get users excited about the project.

This form of open and transparent communication makes me feel like I’m always in the loop without having to rely on third-parties and their interpretations of the team’s actions. I wish more software projects followed this approach instead of stuffing their blogs full of shallow marketing content.

### One to One Multi Touch Gestures

A new feature in Odin are multi-touch trackpad (and touchscreen) gestures for navigating between workspaces and opening and closing the workspace overview. Setting those up (using the excellent [libinput-gestures](https://github.com/bulletmark/libinput-gestures) package) on previous releases was always one of the first things I did, but now they come pre-configured and they are one-to-one, i.e. the content moves with the finger, instead of animating *after* the gesture was completed.

This gives the experience a smooth and polished look that rivals the often praised gestures on macOS. After trying out the newly-introduced gestures in GNOME on Fedora, I was worried they’d feel a bit jumpy and not quite right as well, but a few stutters aside, the gesture experience on Odin is better than I’ve ever experienced on a Linux distribution.

Unfortunately, this experience is also severely limited, as only very few gestures are supported out of the box and even those are not very consistent when it comes to individual applications.

### Sensible Defaults

When I first started out using Linux, I was still in school and had plenty of time to tweak everything to my liking for hours on end. As I grew up, however, and adult life with all its responsibilities caught up to me, I started appreciating defaults more and more.

Installing a Linux distribution nowadays already feels so much quicker and easier than installing any other operating system (and elementary’s new installer makes it even more snappy), but in many other distributions I still have to set up and tweak basic things to make it work the way I want to, which can take quite a while.

Maybe it’s because my preferences align with those of the elementary developers, but I find that the defaults of the operating system, especially the configuration of bash and the Terminal in general, as well as the lack of bloatware, just make sense to me. I can comfortably sit down, install elementary OS and be up and running in less than two hours without using any custom scripts or automations. That’s something I cannot do in say Ubuntu, even though I have many more years of experiences in setting it up to my liking, since I tend to do a fresh install instead of version upgrades.

Case insensitive tab-completion and showing asterisks for password inputs may seem like small things to power-users, but as a power-user myself I wouldn’t want to miss them and that they come pre-configured on elementary OS is great.

### Accent Colours

Another new feature in Odin are the customisable accent colours, which help making the distribution feel a bit more like home without requiring much effort. As someone who stopped using custom themes and opting to stick with the defaults a while ago, I appreciate that, because a change in wallpaper and primary accent colour can go a long way in making an OS seem new and fresh after months or years of daily usage.

And since it’s a built-in feature, I don’t have to worry about it breaking with every update or the new packaging formats for applications, which is great. On that note, the new dark-mode is also appreciated, but unfortunately too inconsistent at the moment, since many apps (even some of the first-party ones) don’t seem to honour it.

### Picture in Picture

I don’t remember when it was added, but the ability to simply hit `Meta` + `F` and clicking on a window or selecting an area of a window to get a small floating preview of it that is always visible as long as the main window is not (which is a nice touch in itself) is still a feature I wish was copied more. It’s the small things that make a difference.

### AppCenter and Flatpak by Default

I love the idea of Flatpaks. They make it easy to install software and keep it up-to-date without having to rely on PPAs and other methods, which is especially important on a distribution like elementary OS, which is based on Ubuntu LTS and as such will not often receive the latest software from the “official” repositories.

Snaps offer a similar promise, but in my experience they cannot deliver and instead cause slowdowns and unwanted (non-hidden) folders in my home directory. So I appreciate that elementary chose to go all-in on Flatpaks with Odin.

On top of that, I think AppCenter is awesome because not only does it make it easy to install software from a place that doesn’t feel slow and bloated, but it also serves as the central place for system and app updates, be it traditional .deb-packages or Flatpaks. It just feels right and makes me want to rely less on the terminal for these tasks, which is a good thing.

Unfortunately, AppCenter is also where the good things quickly turn sour.

### The Not So Good

While some of the complaints I have about elementary OS certainly are purely based on personal preference and habit, others affect many other users as well. I understand some of the decisions on a technical and rational level, but I still disagree with them, because they actively hamper productivity and make it seem like the team is focused on the “wrong” aspects.

That being said, your mileage may vary and at the end of the day the elementary OS team is pursuing their own vision and has no obligation to make that vision fit mine.

### Odin Looks Less Crisp

Speaking of purely subjective things, I can’t shake the feeling that Odin looks less crisp than previous versions. This undoubtedly has to do with the completely rewritten stylesheet and new choice of font, but I’ve always appreciated how the subtle use of shadows and highlights managed to make elementary look crisp on even the worst quality displays, which in my eyes doesn’t seem to be the case anymore with the newest version.

The new stylesheet may follow current trends more closely in being flatter and using the much-beloved “Inter” typeface, but in doing so lost some of its personality in my opinion. On the other hand, it does have much more contrast, which should benefit some users and seems to have been one of the goals of the redesign.

### Practically Invisible Running App Indicators

What seems odd in this regard, however, is that the indicators for running apps on the dock are basically invisible like they have been for ages (in fact there have been multiple bug reports about it). For a distribution which prides itself on accessibility, it seems hypocritical that even people with normal eyesight can’t really tell which applications are active and how many instances there are.

It’s nice that a user can simply scroll an icon to switch between its open windows, but that’s something that isn’t easily discovered, as is using the context menu. I feel like panels like the Ubuntu Dock or the GNOME Dash-To-Panel extension offer better solutions in that regard.

### Missing System Tray

Now this is a long-running and quite controversial one. I totally get that app indicators and the system tray are a legacy technology with many issues that should go away. I get that removing support for it is a way to force application developers to use other, better methods for their apps.

But unless Windows and macOS abandon this pattern, I don’t see how it will ever disappear. Too many third party apps still rely on it to deliver their expected user experience.

Telegram Desktop for example cannot run in the background if there’s no system tray available, which means that the window has to be open on a workspace somewhere if I want to be able to receive messages. Insync on the other hand runs in the background, but offers no way of showing sync status short of opening the app and checking. And many other apps might be running in the background without giving the user a way to close them, since that option is in the tray indicator, which is not available due to the missing system tray.

I know from personal experience that lobbying application developers to switch to more modern APIs has little to no effect, because even the Linux distributions with a significant part of the userbase still support some form or other of system tray.

There are hacks to get the tray back into elementary, but that requires effort and makes the entire system unstable. It’s just not the way to go. However, the developers have made it abundantly clear that there’s going to be no “official” workaround, so if you rely on the system tray, elementary OS is not for you.

### Third-Party-Apps as Second Class Citizens

The team at elementary is working very hard to create a fully integrated ecosystem of applications that are written specifically for their operating system, similar to how Apple does the same on their mobile platforms. The idea and concept behind that is great and a well designed and developed application written specifically for elementary OS will surely improve the user experience over all, but the truth is that elementary OS is not big or popular enough to be able to rely solely on apps written for its ecosystem.

In Odin, the AppCenter only displays first-party-apps and apps submitted and reviewed by the elementary team by default, causing many new users to be shocked by how limited their options are (especially now shortly after release when most of the native apps from previous versions are not available yet).

Experienced users are able to install anything from the official Ubuntu repositories via the terminal, or download a .flatpakref from Flathub and installing that with Sideload (which is a great app, credit where credit is due), which causes other apps from Flathub to show up in AppCenter, but that’s not something a first-time user or a user switching from macOS or Windows would know.

Personally, I think a distribution that is all about accessibility, openness and inclusivity should not discriminate against applications that are not explicitly written for it. It bothers me when I get the feeling that there’s yet another walled garden being constructed. We have precious few professional applications (outside the development sector) on Linux as it is and I think we should embrace all of them.

However, that’s not the way elementary OS wants to move forward, and I respect that.

### Scope Creep and Inconsistency

elementary started out as a theme and icon set and grew into a fully blown Linux distribution over the past few years. They develop more and more apps, along with their own desktop environment and services. On the surface, this looks and feels great.

Diving deeper, not so much. There are a myriad of inconsistencies, long standing bugs and missing features that seemingly are lost in the ever expanding scope of the project. They make me worry that while the front is kept looking pristine and fresh, the back will start crumbling eventually, which would be a shame for the users that take the plunge and commit to elementary OS as their daily driver.

It ranges from little things such as some applications supporting two finger gestures to go back while other, even first party ones don’t, to larger aspects such as the *release version* of Odin failing to properly unlock the screen after it was locked or the device went into standby, something that could cause real loss of work. In my case, no video would play in the pre-installed video app, possibly due to missing codecs. Notification bubbles don’t reliably open the app that sent the notification when clicking on them and quickly clutter up the notifications indicator with notifications that have been interacted with (a bug that I had already noticed in the previous version and which has been reported multiple times). When waking up from standby, the entire workspace including all open application flashes long enough to see details before the lock-screen kicks in, which could be a privacy issue when working in a public place…and more.

## Final Thoughts

The elementary project has lofty and admirable goals. As I’ve said before, I agree with most of their vision and I appreciate many of their ideas, apps and features. Unfortunately, I’m also worried that the vision and the rate at which the scope is expanding will be too much to shoulder for a small team that doesn’t have seemingly unlimited funding.

In my personal opinion, user experience is much better if it holds up in depth instead of breadth. A consistent, polished desktop experience with sensible defaults is worth more to me than a dozen first party apps which might be well integrated, but fail at their basic purposes or are crippled by bugs and missing features. I’d prefer less apps and to an extent even less flashy features if it meant that what I do get is rock solid, consistent and reliable.

I really want to love elementary OS, but I just can’t. I’m less productive with it and all the little flickers, inconsistencies and hoops I need to jump through to make it work when doing more with it than appreciating (truly) amazing gestures and sensible defaults make me feel like I cannot trust the system to work when I really need it to.

That’s just me, though, so I encourage you to try it and find out for yourself if elementary OS 6: Odin is the right fit for you!

Thank you for reading! If you have any questions or comments, feel free to reach out to me over on [Twitter](https://twitter.com/amxmln). 😊

---

*I am not affiliated with elementary OS in any way, nor was I asked to or received any form of compensation for writing this article. It’s purely based on my own experiences and opinions of the project.*
