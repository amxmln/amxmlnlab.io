---
title: '2021: A Year in Review'
tags:
  - year in review
  - '2021'
  - personal
blurb: >-
  A brief review of 2021; the good, the bad, the ugly…and perhaps a look at
  what’s ahead.
coverImage: null
date: '2021-12-21T22:00:00+01:00'
edited: '2021-12-21T21:48:19+01:00'
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
2021 was the year everything was supposed to be fine again. Vaccinations were rolling out, giving a false sense of security in face of the global pandemic, I was going to create an amazing BA project and get my degree, so I could finally start proper adult working-life at the side of a beautiful partner. The future seemed full of hard work—but bright.

Well, it was not.

## Not Too Much About the Virus

Covid is being Covid, others have talked about it aplenty and I think as long as we don’t all start pulling in one direction, we’ll never be rid of it. It’s just going to be the new normal. I’m vaccinated and I’m wearing my mask where it’s recommended, and I’ll probably get my booster shot once six months have passed since my second round of vaccine.

## Work, Work, Work

[My BA project](/blog/2021/i-m-building-a-thing) at UAS Munich was ambitious from the start: a fully fledged Git-based headless CMS that ran as a progressive web app right in the user’s browser. I knew it would take everything I had to design, develop and document it all in time for the deadline, but I knew I could do it. I *wanted* to do it.

So I made a schedule and stuck to it. It was tight, with me also working two jobs, but my partner initially supported me as best she could and I’m grateful for her input and patient proof-reading and editing of my writing. I didn’t mind working from morning until well into the night nearly every day of the week, with a break around lunch to recharge a bit, it was fun.

Sometimes I had to take a couple of days, maybe even a week, off to refresh my motivation or just step away, but all in all things proceeded neatly. It wasn’t until I had shifted from coding to documenting that I first started noticing something being off.

## Consequences

My focus on the project had reduced my social life basically to zero. I had stopped attending the regular meetings of the [writer’s club](/projects/2019/untold-stories) I’m part of, meeting friends or people in general became a rare occurrence. And when I took the first two weeks of August off to have some sort of summer holiday with my partner, she had already made plans without me. I hadn’t realised until then how much she had withdrawn and still couldn’t see that our relationship wasn’t as perfect anymore as it felt to me.

She left me on the first of September, after almost six years of relationship and living together for most of that time. In one fell swoop I didn’t just lose my partner, I lost my best friend, my roommate, my editor, most of my other friends, and all the motivation and inspiration I still had left. The surprise, the guilt, the pain, they broke me.

## Pushing Through

The breakup threw a wrench in my schedule, but somehow I still managed to get the most important parts done. I was able to hand in my project on time and score the highest marks, despite the technical documentation not being fully done.

At the same time, I did my best to come to terms with this new situation. I started picking up the pieces and while I’m still recovering, I think I’ve managed to put what’s left back together, somehow. I still have a lot of reflecting to do, but I want to learn from this experience and grow from it, if that’s possible.

## Looking Forward

Next year I will be starting my full-time position at [Flavour Communication](https://flavour-kommunikation.de), which I’m very grateful for. Leaving student life behind is a big step and while I’m a bit worried about not having enough time and energy left for all the projects that still swirl around in my head, maybe that change will help me continue my journey and find back to happiness.

Things will be difficult for a time still, but perhaps 2022 can become what 2021 could have been.

## Closing Words

If you celebrate Christmas, I wish you all the best for the holidays, and a happy new year to everyone!

I am very grateful for anyone who reads these posts of mine and will do my best to keep up a monthly schedule next year as well. As usual, feel free to reach out via [Twitter](https://twitter.com/amxmln/) if you have any thoughts or opinions you’d like to share about this post.
