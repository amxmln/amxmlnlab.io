---
title: On Analytics and Tracking
tags:
  - tracking
  - umami
  - privacy
  - usage statistics
blurb: >-
  I have long been against tracking my users, but recently I realised that at
  least some anonymous usage data could come in handy. Some thoughts—and a
  solution.
coverImage: null
date: '2021-10-27T13:00:00+02:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
Tracking is a tricky topic. I understand the need for data, but I also feel like it is wrong to collect all sorts of information even when it’s not necessary. Does everything really need to be customised to each and every user?

I guess it does if a company’s profit depends mostly on ad revenue. Or when a company wants to directly target potential customers and provide services tailored specifically to their needs even before contacting them.

Personally, however, I’ve always thought tracking users of my own apps and websites was too much of a hassle compared to what I could gain from it. My very first blog had a StatCounter tracker on it way back in 2012, but I’ve long removed even that.

## Status Quo

This website also doesn’t use any form of tracking at the moment. So I have no idea who or how many people actually read these posts. It doesn’t really matter to me, I just hope they can be useful to someone, someday. None of my apps use tracking, either, and until recently, I was okay with that as well.

I built those apps primarily for myself. If others found them useful, I was glad for it, but I built them because I couldn’t find anything on the market that worked and looked the way I wanted. They are also small utility applications that aren’t exactly geared towards a more professional use.

## Some Data can be Useful

The issue is, that some of these apps have lost their use to me and as such I have stopped maintaining them. I’m even considering shutting some of them down or replacing them with non-backwards-compatible rewrites. At the same time, I have absolutely no idea who besides me is using them, so I would feel bad to break somebody’s workflow by taking these apps away from them… Had I used some basic form of tracking, I would be able to gauge if it would be worth to put in the effort to keep these apps running or provide some sort of migration solution.

And then there’s [Mattrbld](https://mattrbld.com/), the headless CMS [I built as part of my BA thesis](/blog/2021/i-m-building-a-thing). It’s a fully fledged product that is intended to be used for private use-cases, but also on a more professional level. Here I definitely need to know how many people are actually using it, on what platforms and where they are from, so I can make better decisions about the project’s future. So I knew I wanted some form of basic tracking / analytics for that.

## Common Solutions

Google Analytics was never an option, and even an open-source solution like Matomo felt like it would be collecting way too much data that I really didn’t need. I also looked at SimpleAnalytics and Fathom, and especially Plausible since I used that for a client once and liked the design. All three of those services put a pretty hefty price on the privacy they provide…while still tying me to a third party, which is something I dislike.

I briefly considered building my own solution, but that was a lot of effort, so instead I did some more research and stumbled across [Umami](https://umami.is). It’s a self-hosted, open-source website analytics solution that allows me to collect only a minimum of anonymous data.

## Going with the Tastiest Option

Setting it up and hosting it was a breeze and now that it’s running I have the ability to add as many sites to it as I need. I also like that I have granular control over what pages of my single page apps need to be tracked and what types of events I would like reported. The easy switch to honour users’ “do not track” preference is also much appreciated.

For Mattrbld, for example, the URLs of projects can contain sensitive data such as the name of the project, which should obviously only stay on the user’s device. As such, I only track the visits of the dashboard of the application to gain insights into how many people are actively using the app (you always end up on the dashboard when opening it), what OS they are using and what country they are from. That’s enough data for me.

On top of that, I’ve set up some events for when users successfully import projects into the app, be it during onboarding, from the dashboard or via an invite link. All I learn is how many times it happens—I might expand it in the future to also include errors if something goes wrong, but I would only feel comfortable doing so if I could ensure that these errors contained no sensitive data.

## Closing Thoughts

All in all, I’m pretty happy about my experience with Umami so far and I’m considering using it for this website and some of my other apps as well—obviously I will update the privacy policy accordingly if I ever do 😉. If you find yourself in a similar conundrum about not really wanting to track your users, but needing at least some basic data so you can make informed decisions, feel free to give Umami a try!

---

*I am not affiliated with Umami in any way, nor was I asked to or received any form of compensation for writing this article. As such all my experiences and opinions about it are my own.*
