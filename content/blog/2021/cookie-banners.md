---
title: "Cookie Banners \U0001F621"
tags:
  - rant
  - cookies
  - poor ux
blurb: >-
  Google broke my workflow with their new cookie banners, and I’m fed up with
  them. Here’s a little rant on the matter and a solution for my workflow
  problem…
coverImage: null
date: '2021-07-03T13:05:32+02:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
Okay, this partially may be my own fault for almost exclusively browsing in incognito / private mode—a habit I formed to run into less caching issues when developing apps and websites—but these cookie banners are getting out of hand.

## Broken Workflow

One of the most common workflows I have is to hit `Ctrl` + `Shift` + `N` to open up a new incognito window in Chrome and then type a search query right away and once I’m done, close that window with `Ctrl` + `W`. That worked like a charm for years, until recently Google added a full-screen cookie banner / privacy notice. Now I either have to move my hands off the keyboard and reach for my mouse, or tab through a set of controls with **no focus hints** 😒 until I get to the “accept” button. That makes something that used to work in the blink of an eye into a hassle.

This problem is even worse on mobile, where I use Firefox Focus and have to scroll multiple times until I reach the button. Bye bye quickly looking something up.

## Switching to Startpage

Thankfully, Google isn’t the only search engine out there, so a while ago I switched to [Startpage](https://startpage.com) on my phone—which has no cookie banner and promises a more private search *using Google’s search results*. That last part matters to me because I had tried [DuckDuckGo](https://ddg.gg) before, but found its results inaccurate and lacking. Now Startpage’s results aren’t exactly the same as Google’s and sometimes I still find myself going with Google after all, but they are a lot better than DuckDuckGo’s in my experience. And not having to deal with a ridiculous cookie banner that blocks access to the content is a big plus, for sure.

## Cookiepocalypse

I haven’t quite made the same switch on my desktop yet, but I’m getting closer every time that modal window slaps me in the face and takes me out of my flow. I appreciate the intention behind these banners—to force companies to make their users more aware of how they’re being tracked—but the execution is horrible. I doubt there’s many people reading these banners / modals and since the rejection options are often hard to find or causing a lot of hassle, they’re probably just instinctively clicking those “accept all” buttons without much of a second thought. In short, it’s exactly the same as before the cookiepocalypse, except with more hassle for people like me. I mean, there’s even extensions that automatically accept all cookies for you… 😭

Here’s to hope that future legislation changes will make this more manageable—or that users start voting with their clicks and just use other search providers / sites more that offer better solutions to this problem.

---

*I am not associated with Startpage in any way or form and was not asked to write this article, nor did I receive compensation for doing so.*

Thank’s for reading! What are your opinions and ideas on cookie banners? Feel free to let me know over on [Twitter](https://twitter.com/amxmln).
