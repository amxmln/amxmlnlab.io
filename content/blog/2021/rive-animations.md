---
tags:
- app review
- 2d animation
- animation
- vector animation
published: true
date: 2021-03-06T14:15
title: "Rive: Animations on the Web"
blurb: While reading up on the Flutter 2.0 release, I discovered Rive, a web-based
  animation software currently in beta. As someone who always thought AfterEffects
  is too bloated for quick motion graphics and 2d animations, I was immediately intrigued…
edited:
coverImage: /content/uploads/2021/rive_interface.jpg
---

Last month, I told you about [Penpot](../penpot-figma-but-open-source/) and thought
that’d be the last app review in a while, but a couple of days ago I stumbled
over [Rive](https://rive.app), a web-based animation software currently under development.

I am by no means a professional animator, but I like doing the odd motion graphics
project here and there and so far always leaned on [Blender](https://blender.org) for that. Since Blender
is a 3D animation software, however, it’s just overkill most of the time and severely limited
when it comes to vector graphics animation (at least until their Grease Pencil workspace
supports SVGs properly).

On the web, I’ve always fallen back to coding my animations for micro interactions
and such by hand. That works, but it’s time consuming and thus hard to iterate on.
Sometimes having a graphical user interface to play around with really is the best
way to get ideas out quickly.

![Animation morphing a play into a pause button](/content/uploads/2021/testmorph_flat.gif)

## Web-based animation, finally!

Given how powerful the web is and how well it supports SVG, I’ve long wondered why
hardly anybody tried making a sophisticated software for animating SVGs using web technologies.
I only knew about [svgator](https://svgator.com), some plugins for Figma, and found out
about [Spirit](https://spiritapp.io) while researching the topic for this post, but that was it.

Fun fact, I’ve actually considered building such an application myself, but given
how little I truly know about animating and how much work that would be, I’ve never
made it past a couple of mock-ups.

So knowing all that, I’m pretty sure you can understand my excitement when I came
across Rive while reading an article on the release of Fluter 2.0. It seemed like
a dream come true, at least judging from its excellent landing page. Just drag-and-drop
in an SVG, or design one using the app’s “Design”-Mode and start animating just
about every property necessary, including stroke start, end, and offset, which
is a pain to do in Blender if you want [rounded line-caps](https://medium.com/@amxmln/creating-caps-for-animated-curves-in-blender-2-8-d303b0025eba).

## Figma for animations

Intrigued by their website and learning materials, I made a free account and started
playing around. Like with Penpot, the interface will be very familiar to anyone
who has used a screen design tool in the past few years. There are a couple of
tools at the top for creating and manipulating shapes, a layers-panel on the left
and a properties panel on the right with an infinite canvas for artboards taking
up the remaining space in between.

![The Rive UI in animation mode](/content/uploads/2021/rive_interface.jpg)

The magic happens when switching over to “Animation”-mode, when the bottom part
of the application transforms into a timeline, which will look familiar to anyone
who has ever worked with a video editing or animation software before.

## Intuitive animation experience

I found animating pretty intuitive once I got past setting the first key, which
I still haven’t found out how to do for properties that don’t show the little
“insert key” diamond next to them without changing them. The ability to select any
keyframe and adjust its easing feely definitely is a plus—although the editor can
be a bit finicky at times and some more presets besides “Linear”, “Cubic” and “Hold”
would be nice.

This experience extends to pretty much all other parts of the interface—it works,
but it’s finicky or unintuitive at times. For example, panning the canvas is possible with
the right mouse button, but at other times it’s supposed to open a context menu—why
not use the middle mouse button for panning and the right mouse button for the context menu
like in pretty much every other application?

## Shortcut confusion

Similarly, the keyboard shortcuts (of which there could be much more in my opinion, especially
for navigating the timeline quickly), sometimes don’t seem to make sense mnemonically,
like how `M` is used for inserting **A**rtboards. I’ve also noticed that some of
the shortcuts mentioned in the documentation don’t match the ones in the application—which
brings me to my next point.

## Marketing nit-picks

As it happens quite often, the marketing materials (and in this case documentation)
over-promise on what’s already there. It’s not as bad as in some other cases,
but the landing-page and some screenshots in the documentation at least seem to suggest
that some planned features are already available. Things like rendering out
an animation as a video, or dedicated tools for rotation and scaling for example, are not available yet.

When launching Rive, it is very clearly labelled as a Beta, so it’s not surprising
that some features aren’t implemented, but I think it could be made more clear
to potential users that they’re signing up for an early-access version before they do so,
not after. Maybe that’s just me, though.

These nit-picks aside, I am quite impressed. Of course, I have yet to do anything
more complex than the animation pictured in this article, but I feel like this application
has a lot of potential to be useful—especially considering there seems to be a Figma-like
multiplayer experience to work on the same file at the same time (although I couldn’t test that).

##  Wishful thinking

The current pricing model of Rive is completely free and unlimited for individual
users, which I think is amazing, but also too good to be true. The project doesn’t
seem to be open source and I can imagine that the infrastructure to host it all isn’t exactly
cheap. Developers have to eat, too, and so I wonder if the pricing model is going to
change after the full release—although Figma is proving that a generous free-tier
definitely seems to work, too, so who knows… I just hope that in the end it will
be fair to its users and sustainable for Rive.

Another thing I kept wishing for while using the app was a tool for inserting and
animating text, as I tend to do that quite often in my motion graphics projects and
converting text to curves in another program seems fairly limiting while iterating.

The design mode as a whole could use a few more bells and whistles, like boolean operations,
snapping to nodes of other objects (for easier morphing), but since this software
seems primarily for *animating*, not *designing*, I think other areas deserve more
developer focus for the time being.

## Final thoughts

Rive has the potential to become an excellent solution for animating SVGs—as long
as you’re not counting on getting an SVG back out of it, since the software seems
to have gone the route of something like Lottie for AfterEffects in giving you
a designated file format that then has to be played back with one of their runtimes.

Being able to export an animated SVG that can just be dropped into a website without
the need for an external library would be the cherry on top, but I’m sure that would
limit some of the features available (like the ever important flexibility in stroke animations :wink:).

![A heart icon being drawn with strokes and then doing a popping animation](/content/uploads/2021/heart.gif)

Yes, in its current state, Rive is a bit rough around the edges, but from my limited
usage experience, it runs well enough to get a few little animations here and there done with it.
I only experienced one crash and that was when closing a file—so I didn’t lose any work there.

If you’re interested in a tool like this (especially if you’d like one that also runs on Linux),
then definitely give [Rive](https://rive.app) a shot and see whether it’s for you! I for one will definitely
keep my eyes on the project and am very excited to see where it’ll go.

---

*I am not associated with Rive in any way or form and was not asked to write
this article, nor did I receive compensation for doing so.*

Thank you for reading! If you have any thoughts you’d like to share, feel free
to reach out to me on [Twitter](https://twitter.com/amxmln).
