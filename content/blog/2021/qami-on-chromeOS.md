---
title: Qami on ChromeOS
tags:
  - creative writing
  - qami
  - pwa
  - tutorial
  - chrome os
blurb: >-
  Last night I set out to install Qami, my creative writing software, on
  ChromeOS. This post summarises my experience and presents some thoughts on the
  future of the app…
coverImage: null
date: '2021-06-03T08:30:31.583Z'
edited: null
___mb_schema: /.mattrbld/schemas/Blog-Post.json
published: true
---
[Qami](https://qami-writer.com) is my text editor for creative writing. It strives to provide a distraction free writing experience that is optimised for long-form content, but I also have seen it used as a way to take notes in class, compiling poetry collections and more. In Qami the user is able to set a daily writing goal, which allows the app to track their progress and intelligently suggest adjustments to the goal in order to keep motivation up.

At the moment, Qami is built using Electron, which I chose for multiple reasons back in 2016 when I started work on that particular incarnation of the application. However, this makes it hard to release new updates and features and also makes it much less accessible than my other apps, which are all Progressive Web Apps. For example, there’s currently no way to run Qami on a phone or a tablet—but at least there’s a way to run it on ChromeOS until I finally get to releasing a version rewritten as a PWA.

## Requirements and Installation

For this to work, your ChromeOS device will have to have Linux support enabled, which should be getting out of Beta sometime in the near future. You can learn more about how to enable Linux support [here](https://support.google.com/chromebook/answer/9145439?hl=en-GB).

> Unfortunately, if your ChromeOS device doesn’t support Linux apps, you’re out of luck for the time being, I’m sorry. 😔

Once you’ve got your Linux container up and running, you can head over to the [download page](https://qami-writer.com/download/) and download the ‘.deb’-version of Qami. Once it has finished downloading, double-click the file and ChromeOS should show you a window with some information about the package and allow you to install it.

> Please note that you may have to reboot your ChromeOS device after enabling Linux support for everything to work properly.

If everything worked correctly, you should now see the Qami icon in your app launcher and be able to search for it and launch it from there.

![a screenshot of Qami’s welcome screen running on ChromeOS](/content/uploads/2021/qami-on-chromeos-welcome.jpg)

## Setting Up Google Drive Sync

If you’re only planning to use Qami on your ChromeOS device, you can skip this step and just complete the initial setup and start writing. 🎉 If you’ve already been using Google Drive to sync your Qami-folder from other devices, or plan to do so in the future, you have to share that folder with the Linux container first.

To do so, navigate to your Qami-folder in the Files app and right-click it. There should be an option to “share this folder with Linux”. Click on that and you should be good to go.

> If you had started Qami in the last step, you might have to close and restart it before the folder becomes visible in the file-picker.

Once in Qami, the folder you’ve shared will be under `/mnt/chromeos/GoogleDrive/MyDrive/path/to/your/folder` (look under “Other Locations” → “This Computer” to find it). You should be able to select it as your Projects Folder (or restore your previous configuration from it) just like with any other folder, but since it’s on your Google Drive, any change to it should be synced in the cloud automatically.

## Limitations

I’ve found the shared folder to react quite slowly, so loading times might be above average depending on the components of your ChromeOS device. As such, the restoration process of my config stored in my Google Drive appeared to have frozen, but a quick restart of the app proved that it had in fact worked as intended.

A similar thing happened the first time I opened my project overview, but after waiting for a bit, all the projects showed up and it was subsequently much more responsive.

I’ve also noticed scrolling performance to be a bit sluggish, I’m not sure whether that’s because of my trackpad (which has been wonky in the past) or because of something going on with scroll acceleration / performance in the Linux container.

And last, but not least, it seems as if symbolic links don’t seem to be working, which means that notes linked between different projects don’t show up correctly. I haven’t found a way around that yet, so please be aware of this limitation.

## Closing Thoughts

These limitations aside, I found Qami to work well enough on ChromeOS for the time being. It even picked up on the dark window decorations after switching the editor theme to dark and restarting the app.

![a screenshot of Qami’s writing UI in the dark theme on ChromeOS](/content/uploads/2021/qami-on-chromeos-dark.jpg)

Obviously, it’s not a replacement for a truly “native” app. I’ve been meaning to rewrite Qami from scratch for a while now, but unfortunately haven’t found the time for it just yet. While I’m confident that with everything I’ve learned in the past few years, it wouldn’t be too hard a task, I also want to modernise the UI a bit and have yet to come up with a good solution for syncing content as well as how to deal with existing content (since I’m planning on using a different format for text content in the backend). I wouldn’t want users to lose everything they’ve written in Qami so far just to be able to use a new version.
