---
tags:
- mattrbld
- personal project
- cms
- headless cms
- gridsome
- pwa
- progressive web apps
published: true
date: 2021-04-04T13:30
title: I’m Building a Thing
blurb: I’m building a thing—to be fair, I’m almost always building something, but
  this one is special in that it’s my final project for my BA in communication design.
  What is this thing? Well, that’s what I’ll try to explain in this post.
coverImage: /content/uploads/2021/amxmln-mattrbld-social-image.jpg
edited:

---
I’m building a thing—to be fair, I’m almost always building _something_, but this one is special in that it’s my final project for my BA in communication design. Work on it started a while back, but things really have picked up in March, because I can now dedicate a little more time to it—and probably also because my deadline is the middle of July (for now). What is this thing? Well, that’s what I’ll try to explain in this post.

![A screenshot of the projects settings screen in Mattrbld, showing various configuration options for a page titled Mattrbld Website](/content/uploads/2021/faster-in-parallel-settings.jpg)

**tldr;** I’m building my own CMS, check it out at [mattrbld.com](https://mattrbld.com)

## Static Websites Rock!

I’ve been building websites for a long time now, but I was never happy with monolithic content management systems like WordPress and I have a deeply rooted dislike for WYSIWYG site builders like Wix et. al.—probably linked to some PTSD from when I had to work with JIMDO for a while a couple of years back and found myself constantly fighting their constraints.

On the other hand, I’ve always liked writing my own markup. That’s something I really enjoyed about Tumblr for example (back when it was still relevant): it allowed me to write my own code for my blog page, using a simple templating language mixed in with native HTML and CSS to display content. In my book, a simple, static HTML page is often the best solution for most people if they need a small website, blog, etc.

Writing my own markup also leaves me in control. I can do what I want and how I want to do it. I don't have to fight any rigid sets of rules and features and can use my full creativity to solve problems and adopt new technologies. I understand that that’s not always practical, or economic, so it’s not for everyone, but it’s the way I enjoy building websites the most.

Static site generators, like [Gridsome](https://gridsome.org) which I’m using for this website and many of my other recent projects, are perfect for this: they allow me to write my own markup, but they also help alleviate a lot of the pain and annoyance that comes with manually writing a static website. The best of both worlds, so to speak.

## The Problem…

Well, the problem is that not everyone is a developer, and even developers don't always have a code editor at hand—at least I don't. So it would be nice to have a more visual way to manage content, on the desktop **and** (especially) on mobile. That's where headless content management systems come in.

They’re called _headless_, because they don’t try to be smart. All they do is give you a way to manage content, no matter how it’s going to be presented. It’s not even that new of a technology, big media outlets have used them for years, because they typically don’t just have a single outlet for all of their content, but say a website, a magazine, and an app. So they need one central location for their content that doesn’t make any assumptions about how it’s going to be displayed to the users.

There are a lot of headless CMS out there, but generally speaking there is two kinds: some that work via an API and some that are based on Git. API-based headless CMS run on a server somewhere, with their own database and provide you with an API to query your content, which your static site generator can hook into. Even WordPress can run as such an headless CMS, although I haven't tried that yet myself.

Git-based headless CMS on the other hand commit the content into your Git repository, so it can live alongside your code—which is what I think is the better solution for most of my use-cases. I like keeping everything that belongs together, well _together_. I also appreciate that my content stays under my control and I’m not relying on other peoples’ promises about its safety. In the end, I think it just makes sense to keep content and code together in one repository simply because there’s two cases when I want to re-generate my site: when I change the source code, or when I change the content. With so many Git providers now also providing easy CI/CD setups that fire whenever the content of a repository is changed, it seems like a no-brainer.

While the selection of Git-based headless CMS is smaller than the API-based sector, there are still quite a few options, but none of them really work the way I want them to. So far, I have settled on [Forestry](https://forestry.io), and while I do think it does a good job, I’ve run into a couple of bugs and fundamental problems with it (although, to be fair, your mileage may vary).

What I dislike most, are two things:

* It requires full access to _all_ of my Git repositories (full read access via the API of my Git provider)
* It creates a new push whenever I change something and save that, no matter whether I just did a quick save before taking a break, or I simply uploaded a picture for later use. That quickly starts eating into my monthly build-minutes and I wish there was a way to group changes

There are more issues, but these are the main ones and they started bothering me enough to want to do something about them.

## The Solution: Build My Own

I have this tendency to build my own tools so I can make them work exactly the way I want them to (just like how I like to write my own website markup) and while this project is definitely bigger than building my own to-do, or even creative writing app, I feel like it’s a good fit for my BA thesis project. So I’m building Mattrbld now—and it’s not just a simple copy of existing headless CMS solutions either (at least as far as I am aware of).

![The Mattrbld logo, a diamond composed of some rectangles, half of which are filled with purple, while the other half is only visible as purple outline](/content/uploads/2021/mattrbld-logo-text-and-icon.jpg)

Instead of running a server that interfaces with Git providers through their various APIs, I want to leverage modern browser technologies to communicate with them straight through a protocol they already universally understand: Git. I won’t go into the technical details here, you can learn more about that on the [project website](https://mattrbld.com), but simply put, I’m building a CMS that for the most part runs in your browser, or even better: right on your device, installed as a progressive web app.

What sets Mattrbld apart from other Git-based CMS besides that is its focus on a delightful user experience, starting with the ability to control when and if changes are pushed into the central repository, over the built-in support for multiple languages and draft content, to the live content preview and the carefully crafted UI components that are meant to deliver a consistent and predictable experience.

I’m not building Mattrbld to dethrone monolithic CMS like WordPress, or site builders like Wix—those will always have their place, but I think that there are people and agencies out there that would greatly benefit from a solution like Mattrbld, giving them full control over their markup, _as well as_ the ease of managing content visually, whether they do it themselves, or their clients.

At the end of the day, I want to create a product that I’d love to use myself and that can be useful to others. It  already is a challenging project, but I’m learning a lot from it, and hope I will continue to do so. If you’re interested in learning more about it, feel free to check it out at it’s [website](https://mattrbld.com), where I’m also posting bi-weekly devlogs on the current state of development.

As always, thank you for reading!
