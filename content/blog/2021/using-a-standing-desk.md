---
title: Thoughts on Using a Standing Desk
tags:
  - review
  - workplace health
  - workplace ergonomics
blurb: >-
  I’ve been using a height-adjustable desk for the past two months and am really
  happy with it! Here are my thoughts and experiences (and a little update why
  there was no post in September)…
coverImage: null
date: '2021-10-08T10:40:30+02:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
If I have learned one thing in the past month, it’s that life is truly unpredictable. Nothing is certain, aside perhaps from the fact that everything can change at a moment’s notice. I’d like to say the reason for this post being more than eight days late is that I was very busy [working on my BA project](https://mattrbld.com), but the truth is that my entire life was turned upside down on September first when my partner of almost six years left me from one day to the next. It came as a complete surprise and it is only in hindsight that I think I might be able to identify some signs and symptoms that the relationship hadn’t been working for her anymore.

Instead of dwelling of that unfortunate series of heartbreaking changes, for this post I want to focus on a good change instead. At the end of July, I finally took the plunge and got myself a height-adjustable desk, which I have been using almost daily since the day it arrived and I feel like enough time has passed by now that I can share my thoughts about it with you.

## The Hardware

After some research, I went with the 160x80cm Yaasa Desk Pro II in the “Oak” colour, although it seems like they have since added a black / dark grey version that I might have preferred if it had been available at the time. It’s freely adjustable between 62 and 128cm of height and allows storing two height-presets for easy access.

The design is clean and minimal, especially with the two legs being rather slim for being motorised and the rounded corners give it a modern appeal. What intrigued me most, however, was the fact that the pad for adjusting the height and selecting presets is integrated directly into the tabletop instead of being attached at the bottom. This really makes it seem like a regular desk at first glance and has no risk of catching something protruding from the bottom of the tabletop with knees or the armrests of an office chair.

The oak print of the desktop is slightly rough and soft to the touch, feeling pleasant and not sticky, while also not showing fingerprints or dust that easily. I haven’t had any issues with using my mouse directly on it so far.

Aside from the desk itself, I also got their official felt cable management solution, which seems slightly overpriced, but also well engineered and perfectly made for the table. The tabletop came with pre-drilled holes that made it easy to attach and use. The only reason why my cable management might not look as clean is the fact that my outlets are too far from my desk and I haven’t had the chance to get longer cables yet. 😅

I wish Yaasa also offered a way to attach my PC to the bottom of the desk, but they do not and suggest getting a third party accessory instead. A reply to a review on their site mentioned, however, that they are working on an official solution, so perhaps there will be one someday.

## Unboxing and Assembly

The desk itself was delivered quickly (and at no additional cost!) to my apartment in two large and heavy boxes (+ one for the cable management solution). I was slightly worried because the packaging was damaged, but everything seemed in order once I unboxed it. One package held the desktop, while the other contained the entire frame with the legs and the motors, screws and even a small screwdriver, which would do in a pinch, but you should probably keep your own tools ready if you have them.

Assembly itself was reasonably easy, there’s both an included set of instructions and some video tutorials provided by Yaasa themselves. It can be done by a single person if you are able to lift the frame by yourself, but I was glad to have help nonetheless. The thoughtful packaging really helps as well, turning the process basically into screwing in a couple of screws, popping in the controller unit and attaching the frame to the tabletop, which has all necessary holes pre-drilled and even some pre-applied alignment marks.

The only thing that proved a bit difficult was managing the cables for the controller, but after some fiddling around it was reasonably easy to figure out. Once plugged in, it immediately worked and I was able to adjust the height right away.

## The Experience

As somebody who spends a lot of time at their desk, especially now that I’m working remotely most of the time, I was really getting worried about my health. I am somebody who gets up and paces a lot while thinking, so I was getting a bit of movement in, but it was far from being enough. Being able to stand while working doesn’t fix that issue, but it allows me to have a healthier posture and be at least slightly more active while working, since I tend to shift positions quite a lot. It also makes it easier to quickly walk away and stroll around while figuring out a solution. 😉

The Yaasa Desk Pro II moves up and down reasonably quickly and without being too loud, despite carrying the weight of my PC, two 27" monitors and more. I still wouldn’t want to adjust the height while on a video call without being muted, though. The two preset options are enough for me, since I just have one set for my standing and one for my sitting height. While dialling them in, however, I did notice that it was hard to make precise adjustments since there seems to be a brief delay and some sort of easing to the motors. Going from 111cm to 110cm of height can be finicky.

While on the subject of the controls, I have had multiple times where pressing (and holding, as soon as you lift your finger the mechanism stops) one of the saved presets wouldn’t fire and I’d have to lift my finger and try again, which can make it a bit annoying to switch modes quickly. I don’t know if this is caused by something I’m doing, or just a quirk of the software, since the table technically does have a protection against collisions and a way to lock the controls.

Compared to my old desk, which had four legs, the table also does seem a little less stable, even while sitting down. It’s not extreme, but I can see the water in my glass sloshing back and fourth while I’m typing, for example, and I don’t feel confident leaning my full weight on the table. That might be good for posture, but sometimes I just feel like doing it. 😅

These issues are only emphasised once the table is extended into standing height. Typing then also makes my screens wobble and just putting my arms on the tabletop makes it move. It does not bother me as much as I thought it would, I guess I got used to it, but it’s something you should keep in mind if you’re sensitive to things like that. I wouldn’t describe the desk as fragile, but it definitely feels less sturdy than other, more traditional desks I have used in the past.

Since this is my first height adjustable desk, I cannot say what the experience would be like with other such desks—but I would imagine that something with four legs would always be more stable than something with two.

These aspects aside, I love using the desk. Working while standing up has been really refreshing and pleasant and I have noticed that I am using it mostly while standing up these days and only lower it if my legs get tired in the afternoon. We spend so much time sitting already, while watching TV or eating at the kitchen table, it’s good to be standing more.

On top of that, switching between standing and sitting modes has also proven to be a great way for me to switch modes mentally. Watching a YouTube video while standing up feels off—while working seems completely natural. So lowering the desk after an intense programming session or after figuring out a complicated design problem is an excellent way of winding down and giving myself a break.

## Conclusion

All in all I am really happy with my purchase after roughly two months of using it almost daily. It makes me feel more active and healthy, helped with some nasty neck tensions and on top of everything else also looks much neater than my old black glass desktop that showed a myriad of fingerprints and dust the second after being cleaned.

If you are thinking about getting a height-adjustable desk for yourself and you have the money to spend on good materials and a well thought-out design, the Yaasa Desk Pro II might be for you!

I’d be happy to answer any questions you might have over on [Twitter](https://twitter.com/amxmln), otherwise thank you very much for reading! 😊

---

*I am not affiliated with Yaasa in any way, nor was I asked to or received any form of compensation for writing this article. I paid for the product myself and as such all my experiences and opinions about it are my own.*
