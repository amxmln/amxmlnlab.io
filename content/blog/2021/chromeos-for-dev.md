---
tags:
- web development
- ChromeOS
- chromebooks
- devtips
published: true
date: 2021-01-04T12:30
title: Chrome OS for Development
blurb: Over the Holidays, I tried using a Chromebook for some light development work
  and have some thoughts on that matter…
edited:
---
I’ve long wondered, whether Google’s Chromebooks are truly a replacement for Linux
laptops (and perhaps even desktops) for web developers on a budget, especially
ever since Google announced their “Linux (Beta)”, which allows users to run
native Linux applications in a container without having to put their device in
Developer Mode.

So over the Holidays in 2020, I brought nothing but my Asus Chromebook while I
stayed with my parents for Christmas with the intention of finding out if I could
do some light development work on it. If you follow me over on [Twitter](https://twitter.com/amxmln),
you will have seen some of my notes on the experience. This article fleshes those
bullet points out a bit more.

![Atom running on ChromeOS](/content/uploads/2021/atom-on-chromeos.png)

## Me and Chromebooks

I’ve always loved Chromebooks since I first got my hands on the Samsung Chromebook
303C (also known as Series 3) way back in 2012. They are (for the most part) cheap
and easy to use devices that run mostly on the technology I love: the web.

Since those early days, when they were nothing but a webbrowser in a laptop shell,
they’ve come a long way. As the web grew more integrated with the operating system
and native APIs, apps were no longer web-pages that relied on an internet connection
to work, they became powerful tools that range from graphic design and photo editing
to word processing and games.

The only thing that was always missing for me was the integration of development
tools. There are some online IDEs that promise a similar experience, but they don’t
work offline and have some hefty subscription costs associated with them.

There’s always been the option to run a full blown Linux distribution on a ChromeOS
device via some hacks and workarounds, but they always involved putting the device
into Developer Mode, compromising the otherwise excellent security and ease of use.
So that was out of the question for me.

But Google kept at it, integrating Android apps and eventually, full Linux app
support (albeit in a containerised fashion). With the ability to run Node.js, Git
and my code editor of choice (Atom) nothing seemed to be standing between me and blissful
coding—at least theoretically.

My poor old Samsung Series 3 didn’t live long, unfortunately, as I stepped on it
and cracked the display in a very unfortunate late-night-accident. Back then replacing
the display cost almost as much as getting a new device, so was serving
as my parents’ YouTube-machine connected to their TV until I got them a Chromecast.

My next device was a Toshiba Chromebook 2, which I upgraded to an Asus Chromebook
Flip C434 in 2019. I generally use my Chromebooks for some writing, university
work and the odd YouTube video before going to bed. The excellent battery life
allows me to keep it in standby for long periods without having to charge it.

## Setting Up a Dev Environment

But enough gushing about Chromebooks, let’s talk about how useful they are for
software (more specifically web) development.

Enabling the Linux integration was pretty seamless: all I had to do was activate
it in the settings and wait while the installation wizard did all the heavy lifting.
What I didn’t realise in my excitement, however, was that a full restart of ChromeOS
after the Linux integration was enabled could apparently reduce some issues that
crept up along the way.

After updating the Debian container to the latest version, I was pleasantly surprised
to learn that Git was already installed and so I quickly set it up before
installing Node.js via the Nodesource PPA, which worked flawlessly. I then proceeded
to install Flapak and added the Flathub repository for some easy software installation,
which is where I hit my first bump in the road: while installing some Flatpaks
such as Telegram Desktop worked fine, installing Atom did work, but then refused
to launch without as much as an error message (after some investigation it turns
out it was launching, but not showing any window, making it pretty much useless).

So after wasting quite a bit of time on that, I instead opted for installing Atom
the old fashioned way via the official repository. This worked and I finally
got a window, but unfortunately the entire UI kept flickering whenever the cursor
blinked or moved, which made it very hard to use to say the least.

## The Mysterious Over-Night Fix

I was ready to start experimenting with another code editor I like: Micro, but
called it quits for the time being and went to do other things until later that
evening, when I chose to give Atom one last shot after reading an article on how
to get Eclipse running on ChromeOS.

I’m still not sure if I managed to somehow fix it with one of the many random commands
I used, or it was simply a matter of restarting the Chromebook—or perhaps even
just the fact that the automatic Night-Light feature had activated itself, but
when I next opened Atom, it was no longer flickering and I could move on to my
second much less deal-breaking, but nontheless annoying gripe with it: the glaringly
white title bar.

One of the very few issues I have with ChromeOS is that it doesn’t remember the
screen brightness between reboots, which means that I get blinded whenever the
Chromebook restarts. Atom’s dark UI paired with a stark white title bar had a similar
effect, but luckily there’s a fix for that.

Recent versions of Atom allow completely hiding the window decorations from the
settings, which makes plugins like [Title Bar Replacer](https://github.com/sindrets/atom-title-bar-replacer)
work without any strange hacks—which is also why it looks like I have Apple-like
window decorations in the screenshot above.

After installing some other plugins I like to use and cloning one of my projects,
I was finally ready to start coding.

## Getting Some Work Done

Once everything was set up, working with it was surprisingly pleasant. There are
still some UI flickers happening when Linux-apps overlay native ChromeOS windows,
or I open a context menu in Atom, but all in all it was definitely workable and
I got some light work done.

Of course performance is far from what I’m used to, albeit comparable to a similarly poorly
specced Netbook running Linux, and I sorely missed having two displays, but in
a pinch working on the Chromebook is a definite possibility.

The only two aspects I worry about are battery life and storage space (since we
all know those `node_modules` folders can get quite big). Storage hasn’t been an
issue just yet and since I’m not planning on using it as my main machine, I doubt
I’ll run into any problems, but it’s something to consider if anyone was to switch
their entire setup over to ChromeOS. Battery life, on the other hand seems to have
taken quite a hit.

Of course I’m doing more than typing in Google Docs and watching YouTube, and
perhaps the fact that I was also using the Chromebook to charge my phone during
my stay away contributed to it, but having to charge a device that usually lasts a week
twice in three days still seems a bit odd.

I’ll have to keep an eye on that and perhaps do a follow-up with some more conclusive
testing, but for now I must say, I’m pretty impressed with the experience. This
should make the world of development much more accessible for a lot of children
and young adults who got a Chromebook as their first own computing device aside
from their smartphone.

If you have similar experiences or any thoughts on the matter, feel free to reach
out to me on [Twitter](https://twitter.com/amxmln), I’d love to hear them! Otherwise
*thank you for reading*.
