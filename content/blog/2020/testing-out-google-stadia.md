---
tags:
- gaming on linux
- game streaming
- google services
- ux review
published: true
date: 2020-12-08T17:45
title: Testing Out Google Stadia
blurb: I wanted to try out Google Stadia on Linux. Here’s how that went…
edited: 2020-12-11T14:30

---
I’ve never been a very avid gamer. I’ve also been using Linux as my operating system of choice for Linux for most of my life, which until recently wasn’t exactly well off in the gaming department.

Despite that, I like to dabble into one or the other game from time to time and while Steam’s Proton and the WINE project have opened many doors, I’ve still found that playing games on “native” hardware like consoles (or that dreaded Windows Dual-Boot that I feel tempted to set up every once in a while) provided the best experience.

And then came the Game Streaming Services (GSS—is that a thing?).

## Curiosity Peaked

It sounds great on paper: just open up your browser, click a nice looking card and be right there in the game. No updates, no fans spinning up, no loading times. Get started for free, be roped into a monthly subscription later.

Well, that’s the marketing talking. Of course you need an account. And you can only play certain games on certain services, so you probably need multiple accounts, even multiple subscriptions, perhaps. Oh, and also, you need a really good and fast and stable internet connection.

Then again, I knew that it wouldn’t be as shiny as it sounded on paper from the get go. That’s just how things are in 2020: 89% marketing, 10% product, 1% blame-the-pandemic. I still wanted to give it a shot.

## Picking Stadia

I chose Google Stadia as my GSS for two reasons:

1. I already have a Google account (because who doesn’t)
2. I wanted to check out Ubisofts “Immortals: Fenyx Rising” and there was a free Demo on Stadia

So I went to the Website, selected “Sign Up” and chose my Google account from the list, thinking I would be able to jump right into the game—as advertised. I couldn’t have been more wrong…

## Setting Up an Account Within the Account

The Google account is just the starting point. Before seeing games or anything gaming related, you’re forced to set up some sort of profile with a big warning at the top: “These settings cannot be changed later” (or something along those lines).

Make sure it’s the right Google account. Accept our Privacy Policy and Terms of Service. Oh by the way, all the game publishers will be able to collect data on you, so please take the time to read through their privacy policies as well. Choose a profile picture (okay, that one was a neat one, they have a weird burger-pug cross that looks adorable). Choose a gamer tag (**not your real name!**—I don’t know why they would need to point that out so prominently, but they did).

At the end of those steps there was a nice reward: 1 month free Stadia Pro. Yay, that’s nice and totally not a marketing thing to get me to sign up for a 9,99€ subscription that I will forget to cancel in time. The only button on that page: **Try it out for free**

## The Forced Free Trial

Ideally, I would’ve liked to just skip that free trial. Yeah, it’s nice, but it’s not something I needed in that moment, I just wanted to try a free demo of a game I was interested in. I looked for a “Skip”-Button, I really did, I even checked behind that cookie banner at the bottom of the page. But there wasn’t one.

The page was labelled “Finished”, so I figured surely I would just need to go back to Stadia’s landing page and everything would be fine? Nope, clicking on “Sign In” just brought me back to that last step with the free trial.

So I figured there’d be a skip button if I clicked on the “Try it for free”-button? Nope, that just asked me for my payment information, because a free trial is only free if they can charge you 0,00€ for it and then make sure they can charge you the full subscription price once you forget to cancel the free trial in time.

By that time, I was frustrated. But I also still wanted to try out that game. So I googled how to get past that forced free trial. There were no results that told me it was possible, except for an Android workaround that I really didn’t feel like trying.

So I went out on a whim and headed straight for the marketing page of _Immortals: Fenyx Rising_ and clicked on the “Play Now” button there. And would you look at that: I landed back on that final step, but the button had turned into a “Buy Now” button. Even though I wasn’t sure what I was buying or how much it would cost, I clicked it and finally, that account-within-my-Google-account odyssey came to an end.

## Immortals at Last

I landed on what I assume to be the Stadia-listing of the game demo I was trying to play and there was a “Play now” button somewhere among all the “buy the full game” and “buy the gold edition” buttons. Full of hope, I clicked it and things started happening.

There was a pretty pop-over with another play button, which I eagerly clicked again. My browser went into full screen mode, the game’s splash screen animated in, surely I’d be playing in an instant, right?

Well, no.

## Splash Screen of Death

A spinner appeared in the lower left of the screen after a while. A little later, a label reading “Checking your connection”. It didn’t look like anything was going to happen anytime soon, so I decided I’d start writing this blog post.

At this point in the review, about 45 minutes later, I still can see nothing but the splash screen, the spinner, and the label. So much for instantly jumping into a game, no loading times.

If I had to guess, something went wrong, and a refresh of that browser tab would fix things. Let’s try.

## Finally, a Loading Screen

A refresh did indeed fix things and I was able to play through the entire demo (after a brief loading screen). There were no crashes or any other technical issues, so that’s good I guess. Still, the experience wasn’t exactly fun. There were a lot of stutters and lags, visual artefacts and times when my input seemed to get stuck—something I’m pretty familiar with when running games through WINE.

When it worked well, it felt like playing a game on a low-end computer. The framerate wasn’t exactly fluid, but not unplayable either. The input lag noticeable, but not awful. The game itself seems fun, but I probably won’t buy it. It certainly looked pretty when it wasn’t a blurry mess.

## Conclusion

The issues I experienced while playing were most likely due to my WiFi-connection, which has been hit or miss. I did see a pop-up by Stadia more than once that my connection seemed to be unreliable. So I won’t judge the service based on that. Game streaming is probably nothing I’ll pursue further unless I move to a city with better network (or find a way to route a cable connection to my PC).

What I am a little shocked by is the user experience from first signing up through starting a game. I get that they’re trying to make money and marketing is…well marketing. But not offering an obvious option to skip a free trial that requires payment information when the base version of the service is touted as being free? Not to mention that my computer would’ve happily sat on that first “Splash Screen of Death” forever (and that the only way to get out of it was using the F11 key on my keyboard because all other input was being swallowed by it). A timeout would’ve been appreciated.

In general, it probably would have been a much nicer experience, if I had been allowed to just play that demo instantly as advertised and only after had been asked to set up my Stadia profile. Clicking on a “Buy now” button to access a free demo certainly felt weird.

People interested in a game streaming service at this moment in time probably are pretty tech savvy. Still, I feel like many of them would’ve dropped out much sooner than I did—and that definitely won’t earn anyone any money.