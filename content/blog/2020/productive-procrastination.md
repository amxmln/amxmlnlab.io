---
tags:
- productivity
- creative work
- procrastination
published: true
date: 2020-11-08T18:03
title: Productive Procrastination
blurb: 'Working in a creative field such as design or software development isn’t always easy. It often feels like I’m giving 110% for prolonged stretches of time, to then remain completely drained…'
edited:

---
Working in a creative field such as design or software development isn't always easy. It often feels like I'm giving 110% for prolonged stretches of time, to then remain completely drained for a couple of days, not being able to focus on the tasks at hand.

That's probably not the healthiest lifestyle and work-ethic, but that's a discussion for a different post. What I want to talk about today is procrastination, something I feel like we're all deeply familiar with, especially in those periods of feeling drained.

## What is Procrastination?

Google defines it as “the action of delaying or postponing something”. Perhaps that something is one of our chores, perhaps it's working on a project we're feeling stuck in.

Basically, it's coming up with excuses as to why we can't or won't do something in a particular moment. Everyone deals with it differently, some people play video games, others clean their desk, and others again keep formatting the text document instead of starting to write that novel they've always wanted to write.

It's a hard thing to admit, but I've been procrastinating as well, although it took me a while to realise.

## Enter Productive Procrastination

While there's plenty of people who would argue that playing a video game is a productive task, it tends to make me feel guilty for _wasting my time_ when I could've made some progress in one of my projects instead. So I don't play video games when I have tasks to finish, because I could simply not enjoy it.

Instead, I work on other tasks or other projects than the one I'm supposed to be working on (or write blog posts about how I'm procrastinating 😅). It makes me feel productive and I get stuff done, but it's still procrastination nonetheless.

## An Example

I'm in the middle of a rather substantial university project at the moment that forces me to learn a ton of new technologies as well as pushing my design skills out of their comfort zone—while I'm also working three part-time jobs and have other classes. I should be dedicating a significant portion of my free time to that project if I want to get it done without wearing myself out, but I'm stuck after finding out that my go-to rich text editor library isn't suitable for this project and trying to learn [Prosemirror](https://prosemirror.net) is scary.

So at first I was procrastinating by reading through Prosemirror's quite dense documentation twice instead of learning by doing and when that started to feel too much like being unproductive, I switched over to working on a different project...

I've been part of the writers' club [Untold Stories](https://untoldstoriesmuc.de) since it's inception in 2014 and have built and am actively maintaining a small social network for its members. I've just published a  new major version of this network last year (which reminds me that I really should list that in my portfolio)  and have been adding more and more features to it ever since.

## COVID-19 Challenges

Before the Coronavirus outbreak in early 2020, the members of Untold Stories would meet in person in a weekly or fortnightly rhythm to discuss their writing and do some creative exercises for inspiration. In this post COVID-19 world, however, we've switched entirely to online meetings.

Discussing our writing works just the same remotely, but our writing exercises always had the additional hurdle of requiring the right external tool for them. So what better use of my time could there be to extend our platform by adding an entire new "Inspiration" section with writing prompts, creative exercises and one of our groups classics: the Sentence Game, which deserves a write-up of its own.

It took me two weeks to implement these new features in the evenings and on the weekends, but I had a blast doing so. It was the first time that I had to build a real-time multiplayer online game, but in the end it worked out and I learned a lot of new things. I hope these new features will make our remote meetings more like, or even better than, our in-person ones.

I'm proud of what I was able to achieve, I feel like I've been very productive—but then I realised that the semester is almost halfway over and I could've spent all this time working on my university project instead.

## Productive Procrastination is still Procrastination

It didn't feel like I was procrastinating, but that's clearly what I've been doing. Instead of taking on a challenge that I already knew was going to be tough, I sought out a new one which promised short-term rewards.

I willingly postponed a task in favour of another, less essential one, the very definition of procrastination.

In the end I don't know if procrastination, especially productive procrastination, is inherently bad—it still produces results, after all—but it is still procrastination, which means that potentially very important tasks don't get closer to completion.

I think that's an important lesson to keep in mind and I'll try to be more mindful of it in the future.
