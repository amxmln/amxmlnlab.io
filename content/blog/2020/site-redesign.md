---
title: New Site!
date: 2020-09-26T00:00
edited:
tags:
- design
- gridsome
- vue
- markdown
- gitlab
- ux
- ui
- static site generation
published: true
blurb: In this piece I’d like to outline my thought-process behind my recent site
  redesign and share some behind-the-scenes-information…

---
Welcome to my new website! Well, technically it’s my old website, but I’ve
completely rebuilt it from scratch to offer a better experience and new features
like this blog! :blush:<!-- more -->

## Reasoning

While my old website wasn’t that old yet (it was created in late 2018), I always
felt like it could benefit from an overhaul because the home-page was basically
just a collection of links that took the user to where they actually wanted to
be.

So that became my priority for this redesign: put everything essential right on
the home-page. Since my website is mostly my portfolio, that meant that my
home-page needed to show off my work and allow me introduce myself—two critical
features which had their own pages on my old design.

On the other hand, I started to feel like the visual design of the old site had
become stale and while I think it was certainly unique, I felt like it didn’t
exactly represent me well (the logo being unwieldy to use was another reason).
With the redesign basically being a _necessity_ because of the usability reasons outlined above, I figured why not take the opportunity and overhaul my identity as well. New logo, new colours, new typography—all on top of a brand new technical foundation and user experience.

## What Changed?

What I wanted to retain from my old website and identity was the _uniqueness_ as
well as the shape of a triangle, because it not only resembles the first letter
of my first name, but also because it’s a wonderful shape all around. So the
triangles and the not typical cookie-cutter layouts stayed—where it made sense.
Pretty much everything else is different, though.

### Logo

The names **Amadeus** and **Maximilian** are actually my first and second names,
my last name being **Stadler**, which I’ve never really liked because it sounds
just so _German_ and boring. So while Stadler doesn’t leave much room for interpretation (also because there’s multiple major brands using it already)— and my contraction “AMXMLN” seems to be difficult to appreciate for anyone other than myself :sweat_smile:—I kept my full first two names and combined shapes from both of them into my new logo, which consists of two layered triangles forming an “X”, while the lower one resembles an “A” and the negative space between the two forms the letter “M”.

### Colours

The new logo lends itself to a duochrome representation and I wanted to quote
the green-and-purple colour-scheme of my old identity, so I created two
accent-colours: <code style="background-color: #1cbabd; color: white;">#1cbabd</code>
and <code style="background-color: #ff00b8; color: white;">#ff00b8</code> paired
with an off-white <code style="background-color: #f8f8f8; border: 1px solid #ccc;">#f8f8f8</code>
for backgrounds and a not-quite-black <code style="background-color: #312951; color: white;">#312951</code>
for the text.

### Technology

My old website was created with the wonderful [Vuepress](https://vuepress.vuejs.org),
but I felt like trying out something new this time and had wanted to look more
into [Gridsome](https://gridsome.org) for a while—especially for its integrated
image optimization and lazy-loading. Since I’m very familiar with Vue, it seemed
like a logical choice to go with Gridsome as my static site generator. Having no
prior experience with GraphQL and Gridsome being rather young it certainly was a
challenge, but I’m glad that I took it on because I learnt a lot and I’m looking
forward to using these technologies for professional projects soon.

### Hosting

One cannot work with the JAM-Stack and not come across [Netlify](https://netlify.com).
They provide an excellent service for static site hosting, build-on-push and
much more paired with an excellent free tier and a clear pricing path for later
scaling. So far, I’ve had an excellent experience with them, but for this
iteration of my portfolio, I wanted to simplify things a bit.

[GitLab](https://gitlab.com), where I host the source of this website, has an
integrated CI/CD service that allows me to run the build script for this website
whenever I push code to the repository. Couple that with their free
Pages-Service—and I have everything in one central place with as few middle-men
as possible.

At least that was what I thought when initially planning all of this. However,
since I have my apex domain served over Netlify DNS it was less than trivial
to activate this domain on GitLab Pages (maybe I was too impatient) and on top
of that, GitLab still doesn’t have a privacy policy for pages hosted on their
Pages service (the [related issue](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/46267)
has been closed for a long time without being resolved as far as I can tell). So
in the end, I decided to stay with Netlify for the time being.

### CMS

Another thing I’ve been wanting to try was integrating a headless CMS for a more
visual content creation process. Don’t get me wrong, I like the control I have
when creating content in a code-editor—but sometimes I have an idea for a
blog-post on the go and GitLab’s UI isn’t that well suited for writing code on
a mobile device. Plus, a lot of clients want a content-editor-friendly way of
managing their content without having to be a developer.

I chose [Forestry](https://forestry.io) over [NetlifyCMS](https://netlifycms.org)
because I liked its cleaner aesthetic and support for Gridsome. On top of that
the sign-up and connection workflow just appealed to me more, despite Forestry
being a commercial, closed-source service. That said, I’ve hit some bumps and
limitations with Forestry in some other projects, which might make me want to
reconsider and try another headless CMS in the future.

That’s one of the many benefits of the JAM-Stack: I can simply exchange bits and
pieces whenever I grow unhappy with one of them. 🎉

### There’s a Blog Now

Over the years I’ve often wished I had a central location to publish articles
and thoughts, perhaps even a tutorial here and there. For a while I thought
[Medium](https://medium.com/@amxmln) would be the ideal place for that, but
their restrictions for non-paying users and the fact that it was effectively yet
another social network to be active in quickly made me reconsider.

With this new system, it’s almost trivial to add a pretty fully featured blog,
so I did and I hope to be able to keep it reasonably updated over time.

In addition to simply expressing myself, many of my smaller apps and projects
also don’t have dedicated websites, so I thought this would be a great place to
update users on project developments and updates. Reducing complexity once again. 😅

## Challenges

As much as I think that Gridsome is an awesome project, I feel like its
documentation could benefit from an overhaul. Perhaps I’m just spoilt by the
excellent Vue documentation, but there were many times I had to browse through
source code, GitHub issues, or other people’s write-ups on Gridsome to figure
out why something wasn’t working the way I expected it to.

### The Image Issue

For example: Gridsome uses source-plugins to create its content structure and
since my content for projects lives in JSON-files while my blog-posts are
written in Markdown and live in the same repository as my source-code, I used
the filesystem-source-plugin. This website being my portfolio, I naturally have
a lot of image-content, but for some reason Gridsome couldn’t pick up the paths
to said imagery.

It wasn’t until much later when I found out the filesystem-source-plugin had an
undocumented option `resolveAbsolutePaths` that would allow me to set an
absolute path to my image files.

Other times the existing documentation was simply outdated, but I understand
that the project is young and since it’s open-source, I should complain less and
actually contribute to the documentation. Gridsome is awesome and I can highly
recommend it from my (limited) experience so far, just know what you’re getting
yourself into.

### `<noscript>` Woes

Perhaps it’s something I’m doing wrong, perhaps it has [nothing to do with
Gridsome](https://github.com/vuejs/vue/issues/8996), but for whatever reason, my
site would just not work correctly if there was a `<noscript>`-Tag anywhere in
one of my page components that contained a block-element, or an element set to
display as a block. It’s a weird issue that caused me quite a bit of a headache
trying to debug—and eventually led me to working around it by just using CSS in
a `<noscript>`-tag to show a message to those two users with disabled JavaScript.

## Closing Thoughts

In the end, I’m happy that I took the time to build this new version of my site.
It’s not perfect yet and I’ll surely have to adjust some things here and there,
but for now I want to focus on adding new content. If you have any feedback (or
find any bugs :flushed:), feel free to let me know via any of the channels
linked below.

**Thanks for reading!**
