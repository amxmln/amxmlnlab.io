---
title: The Web is the Future
tags:
  - web development
  - pwa
blurb: >-
  I love the web and I firmly believe that it is the future for all sorts of
  applications. And with recent announcements from Apple, the web is getting
  even better—or at least, even better for Apple users, which have been left out
  for the past few years. Some thoughts.
coverImage: null
date: '2023-06-25T11:15:00+02:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
I don’t think it’s a secret that I *love* the web. I mean, pretty much everything I build runs on the web! For years now, progressive web apps have worked pretty much like native apps on Android and desktop operating systems using Google Chrome. Yes, there are some drawbacks, but they’re getting less and less and in my opinion, the ease of development, deployment and innate shareability of applications running on the web are well worth some tradeoffs.

After this year’s WWDC event, I think it’s clear that even Apple recognises this (which is somewhat ironic since Apple was a big proponent of web apps in the early days of iOS, weren’t they?). There have been numerous updates to WebKit and from macOS Sonoma onwards, you’ll be able to install PWAs (and even other websites) as “apps” to your device through Safari.

## The Web is Powerful

And it does make sense. The web has become a powerhouse in recent years, just look at some of the apps that were built with web technologies:

**Figma,** which is a blazing fast UI design tool that stays responsive even when opening large files and in my experience works much better and more reliably than something like XD. (Probably one of the reasons Adobe bought Figma.) They even just announced a whole slew of interesting new features bringing even more features of the modern web to the application, such as variables and, yes, flex wrapping! (Shoutout to PenPot for introducing that feature first!)

**Spline,** which is a full-blown 3D modelling and animation tool. It isn’t as smooth as Figma and obviously not as powerful as something like Blender, but impressive nonetheless, and I’m sure it will keep getting better as time goes on.

**Fable,** which is basically After Effects, but on the web! It’s the animation tool I’ve been dreaming of building forever. And now it just…exists. Try it out if you haven’t yet!

Of course, these are just some examples. There’s even entire game engines like Godot running in a web browser. I think it’s very safe to say that we have moved beyond simple to-do list applications and the then so very impressive Google Maps—I even remember how awed I was at the first version of Google Docs!

There are so many different web apps and PWAs out there and sure, some are higher quality than others—just like on the native app stores. I firmly believe that more and more tools and companies will move to web-based applications in the future, despite the vocal “native-first/native-only” crowd. It just makes sense.

## The Web is Free

I think one of the primary reasons for this is that the web is truly free. If I choose to release the billionth simple to-do app, nobody can stop me. There’s no app store policies to follow or violate. I own what I publish and if I choose to charge for it, I don’t owe 30% of my earnings to some big corporation.

I am also not tied to a particular platform. Web apps run everywhere—so long as they are built properly. If I want to switch from one mobile OS to another, I can take them right with me, at least in theory. And they work on my larger screen devices as well!

The reality is that despite all its freedom, the web has another kind of dependency: the browser. Safari has long been lagging behind Chrome in regard to many PWA related APIs and Firefox…well they chose not to support them at all (another bit of irony, considering FirefoxOS was supposed to be populated entirely with web apps).

So there has been a kind of platform lock-in and while it is practical that most users use Chrome anyway, it isn’t necessarily a good thing.

## Even Apple is Embracing it

That’s why I think it is so great that Apple finally seems on-board. And not just in a “well, let’s sort of support this” way, either. They seem really serious about it.

Just in the past few months, Safari on iOS gained many new APIs that finally allow for push notifications and app icon badging for web apps that were added to the home screen, taking them one step closer to feeling more like native apps.

And then there’s the feature coming to desktop Safari: the ability to install web apps. Not only is this kind of special because hardly any browser other than Chrome seems to support that, but also because the way they have implemented it is designed so carefully and with attention to detail, truly integrating with the rest of the OS.

I’m very curious to see where this is headed and if they will be doing their own thing or truly embracing the protocols and procedures like handling of the `beforeinstallprompt` event for showing a custom installation prompt to the user. I have a bit of a sinking feeling that it will be the former rather than the latter, but anything is better than nothing—and one can always hope, right?

## The Web is the Future

If the web has taught me anything, then that there’s always a way. It might be frustrating and not very straightforward, but I truly feel like there’s nothing that can’t be built on the web *somehow*. The web is free, the web is powerful. For me, this makes it the ideal platform to build my applications on.

I love the web and don’t intend to stop loving it. I’m working on some really cool projects, like the next versions of [Qami](https://qami-writer.com) and [Magistan](/projects/2019/magistan/), as well as a new project called Applause. Which I can’t wait to tell you more about them in the future.
