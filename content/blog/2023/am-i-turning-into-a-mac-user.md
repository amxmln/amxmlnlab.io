---
title: Am I Turning Into a Mac User?
tags:
  - macOS
  - linux
  - os
blurb: >-
  A bit of a reflection on my recent computing habits and the creeping insertion
  of the Apple ecosystem into my life.
coverImage: null
date: '2023-03-26T12:00:00+02:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
This thought has crept into my mind lately—and it’s bothering me. I’ve made no secret about the fact that I’ve been a very [happy Linux user](/blog/2022/my-os-of-choice-linux/) for most of my life, some may say a bit of a Linux enthusiast or even evangelist. I love the usability and design of GNOME and the flexibility and freedom that come with using free and open source software. Recently, however, I’ve been actively noticing the times I actually used my Linux PC instead of them being just ordinary parts of my day-to-day life. Where my tower used to be the first thing I turned on in the morning and the last I switched off in the evening, it now remains silent on most weekdays.

## A Change in Habits

At work, I use a Mac. I actually used to switch between a Linux box and an iMac for a while, but when we dissolved our physical office, I just had no room for a second Linux desktop in my home office. To be honest, even before that, it was tedious to have to switch between two different systems for design and development.

So all my work for my day job is done on a really beefy MacBook provided by my employer. I’m not the biggest fan of macOS, but I do have to use software that doesn’t exist on Linux and save files in formats my coworkers can work with without having to learn a new piece of software. Plus, there’s no denying that the hardware is pretty amazing.

## New Hardware

Speaking of hardware, until last November, I had only ever owned a single MacBook, my MacBook Pro from 2018 that I had bought during my BA in Design. It was a neat device, but I hardly used it unless I was in need for some Apple specific software, especially after the pandemic started and I attended all my classes from home. The only times I found it to be really useful was when I went on holiday somewhere, and I wanted a reliable device with me that was sturdy and had good battery life. I guess those were the first tendrils of this change in my habits.

Last November, I sold my MacBook Pro and bought a new MacBook Air instead. Better screen, better battery life, even better performance thanks to the amazing new chips from Apple. Plus, a better look as well, in all its midnight glory. While I was still firmly in the “Apple is ridiculously overpriced and everyone should just use Linux” phase of my life, I used to joke that the only way I’d ever consider buying a MacBook was when they’d release a dark one. Well, I guess I finally made good on that promise.

What my younger self would be shocked to learn, however, is that this new MacBook Air has basically taken over most of my private computing. I do my freelance work on it, I watch YouTube on it, I do some writing and work on my personal projects on it.

Why?

## Convenience

It has fantastic battery life. Not only that, but its standby mode is unbeatable. Using it for about two hours every day and letting it sleep the rest of the time, I have to charge it once a week, and it’s always instantly on when I need it. No Linux laptop I’ve ever had could offer me that same experience.

Of course, I could just use my PC, which doesn’t even need a battery and thus could sleep indefinitely. While it’s a bit ironic that I’m confining myself to using a tiny 13-inch screen when I could instead use two 27-inch ones, my PC has one fatal flaw: I can’t use it sitting on the couch or while working in the kitchen, so I can chase away the pigeons that keep landing on my balcony.

I work at my desk for at least eight hours a day. I can use it standing up and sitting in a well-made office chair. It’s far from uncomfortable, but after all that time spent there, I just need a change of scenery to get away from the thoughts and challenges of my day job. I couldn’t do all my work from the comfort of my couch, but being able to do at least some of it from there has been great.

I do have a Linux laptop I could use for that, but it’s old and tiny and a little bit unreliable. And besides: even when fully turned off it loses power, there’s no way it would even last a single day in standby mode. I love it for experimenting with different distros, trying out the newest features of GNOME and Linux as a whole and doing some odd development things that just are much easier to set up in Linux—but I can’t make it my primary personal machine. Not when there’s a better and more convenient option available.

## A Loss of Idealism

This train of thought would’ve been unthinkable for my younger self. Back then, I’d happily trade a little convenience for true freedom and the peace of mind that I was using and supporting my operating system of choice. Perhaps I’m just getting older, or more disillusioned.

Linux keeps getting better and better every day. In my opinion, it’s much more intuitive and has far superior window management compared to macOS. It doesn’t lock me in the way Apple does, with its proprietary formats and those keyboard shortcuts and layouts that make it hard to switch back to a more “traditional” layout after working on a Mac for the entire day.

But it’s less convenient, at least for the time being, and for some reason I’ve told myself that it’s okay to trade this convenience for my ideals. I am content with noticing the beauty of GNOME, the large screens and the unforgettable feel of typing on a mechanical keyboard the few times a week when I do end up doing something on my Linux PC.

Even at the risk of sounding hypocritical, I do fully stand behind Linux and the incredible work the community is doing to build this operating system. I keep up with what’s happening, I do participate in discussions and discourse. I love my Steam Deck and sometimes still have a hard time believing it’s real. I’ll always be a Linux user, perhaps even an enthusiast or evangelist.

Here’s to hoping that that’s enough to not end up replacing my desktop machine with a Mac Mini or Studio in the near future—because the reality is that while being a Linux user, I’ve definitely turned into a Mac user as well, whether I like it or not.
