---
title: Cosy Games
tags:
  - game design
blurb: >-
  With all the cosy games releasing in August and September, I feel reminded of
  my own wish to create one. This post details where that urge is coming from
  and what made me fall in love with the genre.
coverImage: null
date: '2023-08-15T12:30:00+02:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
I still remember that Christmas day lunch after my parents had finally given me a Nintendo DS Lite and a copy of Animal Crossing Wild World. I had seen ads for the game for months and from the first moment on, it exerted an inexplicable pull on me. More than Pokémon and Mario, Animal Crossing was the game that made me want to own a console (well, handheld) for the first time in my life.

I still don’t know why, but the idea of a chill and calm second life in my pocket fascinated me. I loved the art style, the tiny looking spherical world, the link to real time, the quirky animal characters. And so, while my family and I were waiting for our order at the restaurant on Christmas day, I proudly showed off my new handheld and the game to my cousins.

It was hard to not feel heartbroken when one of them said something along the lines of, “I don’t get it. If you want to pick fruit, why don’t you do that outside?”

Many of my friends didn’t understand it either, they were more interested in slaying monsters in World of Warcraft, shooting at people in CounterStrike, or building bases and kingdoms in Command and Conquer and Age of Empires. It seemed as if the only people that shared my passion were a couple of female classmates and my sister.

And we adored that game! I don’t know how many hours I put into it, but it was a lot. I stayed up late and got up early to complete my insect and fishing collections. I was so immersed in the game that I dreamt of finding secrets within it that expanded the world and gave me more to do.

Because if there was one thing I didn’t like about the game, it was that there wasn’t enough of it. The world was too small, I longed to explore more. I wanted to customise more, be rewarded for sticking with it for years.

But there were no more secrets to discover. No hidden places, no secret page to the fishing collection that opened up two years into the game. So eventually I was done, I sold my DS Lite and the game to a neighbour and moved on—well, at least I tried to move on.

I couldn’t, and I still regret that sale. Animal Crossing: Wild World had sparked an idea within me that hasn’t left in all the years that have passed since then. I wanted to build my own version of such a game. A cosy game of my own, but back then I didn’t even know that there was an entire genre of these games.

Instead, I built a couple of prototypes in the Blender Game Engine that never really went anywhere. I even learned how to use Love2D and how to program in Lua, looked into Godot when it became free and open source. And I got incredibly hyped when Animal Crossing: New Leaf was announced (I never had the chance to play Let’s Go to the City).

I finally had enough of my own money to pre-order the game and buy myself a Nintendo 3DS to play it on. I waited eagerly for both of them to arrive and thoroughly enjoyed playing the game—although for some reason it could never reach the amazing immersion and feelings of joy that Wild World gave me.

The game was bigger, there was more to do, more to customise, but the world seemed less alive, the characters flatter and more boring. It also felt much easier than the previous one, and so I was done playing it much sooner. It was not the perfect game I had been looking for—despite it even getting a small expansion eventually, that added a couple new mechanics.

Interestingly, however, I kept meeting more and more people during that time who not only knew what Animal Crossing was, but who also shared my love for it. They were generally very lovely people to be around, and the first hints of a greater community of cosy game enthusiasts.

To fill the void left by finishing New Leaf, I first dabbled into the wider ecosystem, especially when Stardew Valley was released. I hadn’t played any of the Harvest Moon games, so I was completely new to the farming game subgenre, and while I enjoyed playing the game and watching others play it, it never really drew me in. I disliked the graphics, they were too busy for my eyes, and the in-game clock that ran separate from real life just felt…wrong to me. There were other games as well, and in 2016 I even bought a Nintendo Wii U in anticipation of an Animal Crossing game that would release on it—which never came.

But there were whispers. Whispers of something big to come. An Animal Crossing game like no other before it. Bigger, better, and with lots of content to be added in the years after its release.

The first trailer and gameplay footage of Animal Crossing: New Horizons blew me away. It was so pretty! I loved the wind and rain shaders, the hundred little details, the sound design—and the UI design, which had always been cool in the series, but now felt incredibly refined and modern.

So even though the game wasn’t scheduled to release for quite some time, I got a Nintendo Switch. I was incredibly hyped for it and my then partner and I watched every Nintendo Direct eager for more information.

When it finally released, I played it every single day for more than a year. It was a great game that, paired with the timing of its release, catapulted the series and the cosy game genre into the minds of many more people. What had felt like a niche for most of my adolescence, became mainstream.

Unfortunately, New Horizons wasn’t without its faults. It felt too much like the promise of further updates meant that the initial release had been trimmed down to allow for these updates. There was a lot to do, but the things quickly became repetitive. The villagers were shells of their former selves, and unlocking everything that wasn’t gated by the real time clock felt far too easy. The updates over the next year added most of the features that the other games launched with and some new things, even capitalising a bit on the booming farm game market, but in the end New Horizons only rekindled my urge to create my own cosy game and after a break-up slowly faded away.

Today, in 2023, the cosy game market is booming, perhaps even becoming saturated. In the wake of Animal Crossing and Stardew Valley, hundreds of games have been released or will be released in the near future that are putting their own spin on the genre (or are trying to capture that initial spark so many of us felt when we first came into contact with one of these games). Content creators play, test, and rank them. There’s news content around every whisper, every rumour. The community seems boundless.

I’m excited for some of these new releases as well, I’ve been playing Palia lately and am very much looking forward to trying out FaeFarm in September. Both games seem to be doing something new and while Palia is a bit of an unfinished mess right now, it still made me sink quite a few hours into it already and still has me hooked for the time being. FaeFarm on the other hand looks incredibly polished and like a worthwhile experience on its own.

Somehow I have the feeling, however, that I will never quite experience the same magic again that I felt with Wild World. Too much has changed since then: expectations, partners, time constraints. I do still want to build my own cosy game, though. I have heaps of notes and concepts and I feel like I have the skills required to write and program it at least. What I’m missing is enough time to build it—and talented artists, sound designers, composers and everybody else required to pull it off properly.

So for now this urge and spark of inspiration will stay a dream—a smile-inducing, warm, cosy dream.
