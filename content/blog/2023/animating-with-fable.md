---
title: Animating with Fable
tags:
  - app review
  - 2d animation
  - animation
  - vector animation
  - review
blurb: >-
  After keeping up with Fable for a while, I finally got to use it for a small
  project at work and had a good time with it! So after looking at Rive before,
  I feel like it’s time for another app review.
coverImage: null
date: '2023-10-29T16:30:00+01:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
This week at work, I had the chance to animate a small Instagram post with Fable, a web-based motion graphics tool. It was a lot of fun! A while ago, I also [wrote about Rive](/blog/2021/rive-animations-on-the-web/), a similar application, so I think it would only be fair if I shared some thoughts about Fable as well.

## The Missing Piece of the Design Stack

Despite its somewhat dreamy name, the stories you can tell with Fable are anything but old-fashioned. It is a collaborative animation app that runs in the browser. The Figma for motion graphics, so to speak—and that’s not where the comparisons end, but more on that later.

Fable brands itself as “the missing piece of the design stack”. A tool for creating beautiful animations and micro interactions with an intuitive and highly polished (at least on the surface) interface. And to be honest, this feeling of polish and a touch of design starts on their website.

Everything is carefully crafted—and animated, of course. Even the help resources got a facelift recently and look much more in line with the general design language. This is something I think a lot of designers can appreciate. It’s much more fun to work if the software you’re using is beautiful, right?

However, this begs the question: is the beauty more than skin-deep?

## Getting Artwork Ready

At work, I was tasked with creating an animation that conveyed the concept of micro interactions and incorporated some elements we use on our agency’s website. I had a rough idea for it in my head and created a small storyboard in Figma, since that’s become my go-to software these days, even if it’s just for quickly sketching out an idea.

Of course, having all of our design tokens such as fonts and colours in Figma already helped me be quite quick and efficient about it. Especially, since Fable offers a Figma plugin, which allows you to quickly copy assets from Figma and paste them into Fable, retaining most properties and optionally even the ability to keep text editable, instead of converting it into curves. Only effects, such as drop shadows and blurs, caused individual layers to be rasterised into images.

Yes, individual layers—the plugin even keeps the layer hierarchy intact when copying assets from Figma into Fable. Since I don’t have access to the plugin’s source code, I cannot be sure, but I assume this is implemented by making use of Figma’s SVG export. Selecting layers in Figma and using the “Copy as SVG” option in the context menu, and pasting SVGs copied from the web works as well, so it seems likely.

This makes it really easy to get started, since I can just design vectors in software with a more robust set of design options and quickly get them onto a canvas in Fable, ready to be animated. On a side note, it doesn’t stop there: Fable also supports working with raster graphics and video files, although those aren’t available as instantaneously as vectors, as Fable has to process them after they were imported.

## Getting those Shapes Movin’

Now, I’m far from a professional motion designer, but I’ve always dabbled in the area, and so I think I have a pretty good understanding of how to animate and work with easing. Fable makes that incredibly easy!

While the rest of the interface resembles a screen design tool such as Figma, the timeline is where Fable really shines. At first glance, it reminded me a bit of Adobe After Effects, however it is simpler and more intuitive to work with in my opinion.

![A screenshot of the fable UI with a toolbar at the top, a work area in the center, a layer stack and timeline on the bottom and an inspector panel for properties on the right](/content/uploads/2023/fable-ui.png)

Selecting any keyframe allows tweaking its easing curve, which then also gets represented visually on the timeline. And while working with many different layers can get a bit chaotic, the option to colour them differently and only have them show up in the timeline for the sections they are relevant for helps a lot.

There’s even something akin to components in Figma, which Fable aptly calls “Scenes”. They are reusable blocks of animated assets which have a common parent. If that parent is tweaked, those changes propagate to all its instances. Editing a Scene happens in its own tab, away from the main animation, which makes things even more manageable.

## A Feast of Features

Scenes can also expose values as “Variables”, which allows further customisation of instances, another parallel to Figma. Something that can come in extremely handy when for example working with a stack of cards that all have the same motion, but different motifs.

There’s also the ability to easily clip layers using other layers, masking either everything outside or inside the bounds of the mask. They’ve even added support for non-destructive boolean operations—although sadly they cannot be used to fake a liquid / gooey effect as in Figma, since border radius can only be applied to individual shapes, not the combined shape.

Another aspect worth mentioning are the predefined “Transitions and Effects”, which are a library of pre-built motion presets ranging from trim-path effects to animating the letters of an editable piece of text. They make it incredibly fast to get some motion going and even enhance it with things such as grain and various filters.

This is just scratching the surface, there’s so much more to explore from particle systems to duplicators—although that button seems to be missing in the latest version. I’m definitely looking forward to spending some more time with the application.

## Wait, what about Rive?

The last time I used the term “Figma for animations” was when I wrote about Rive, another web-based animation tool. There’s no denying that the two are very similar, although I feel like while Rive is pushing to be the next Flash, providing interactivity and state machines, Fable is striving to be the motion graphics tool to finally let After Effects be just a compositor.

Personally, I found working with Fable more enjoyable than Rive, the UI just seems more polished and more reactive. That being said, Rive recently has gained many of the features (like text and motion paths) that Fable has had for longer, and provides its own, much more performant runtimes to display the animations on the web and other places, while Fable only offers the export to Lottie files.

Both tools have their place and while both can be used for basic motion graphic and animation tasks, they do have very different feature sets for advanced users, so it’s less a question of which to use over the other in general, and more about what features a specific project requires. Is it a social media post that will be uploaded as a video and needs to be done in a reasonable amount of time? Fable is probably the right tool. Do you need skeletal animation for a cut-out style character? Go with Rive. Do you want to build out an extension to your design system, including templates and guidelines for motion? Fable offers strong collaborative features and reusable scenes and variables. Do you want to give reactivity and interactivity to your animations? Rive’s event and state machine systems are up for the task.

## Too Good to be True

Using Fable often feels too good to be true, especially considering the generous free plan—you’re missing out on some of the collaboration and organisation features and can only work with 1080p timelines no longer than 30 seconds if you’re not paying the monthly subscription—but that’s plenty to get started and see whether this tool can be useful to you. It really is an enjoyable experience, until you bump into some odd issues that make the illusion of perfection crumble.

For one, the search feature in the documentation doesn’t work for me. I simply cannot click into it to allow me to enter text, which was an issue when I was looking for the reason why the duplicator functionality had gone missing when I really could’ve made use of it. Speaking of the documentation: while the newest UI design update was lovely, most of the videos linked in the documentation show the old UI, which can be a bit confusing. Fable have realised that themselves and added a note, so I hope they’re working on updated versions of those videos.

Other times, strange bugs come up, like when I was trying to add some curving to motion paths (which are a great tool most of the time!) and it simply refused to let me. I’ve also had some issues with the undo-stack, sometimes when I undid (or worse tried to redo steps), it simply wouldn’t change anything.

On the animation side, I found that trying to animate presets like the box-shadow effect often yielded unexpected results or just wasn’t possible, while others, such as the type in text transition, lacked enough control to make the motion feel good.

Sometimes, the quality of the exports also wasn’t particularly high, although that seems to have got better in recent releases. I do wish there was an option to export an animated SVG, but unfortunately there isn’t, although the Lottie-export might be enough for now.

And while I’m nit-picking, it would be awesome if Fable supported keyboards other than the American QWERTY-style. Some of the shortcuts just aren’t supported on mine, something that also bothered me about Figma for a long time, but they managed to remedy it. There also isn’t a desktop app / installable PWA version, which I’m sure some people would appreciate.

## A Fable Worth Knowing

Despite those shortcomings, which surely can be worked on in the future, I believe Fable is a worthwhile tool to keep in a designer’s arsenal. It really does make it incredibly easy to create good-looking motion graphics, even without tonnes of experience in the field. The free plan and the fact that it runs in a browser regardless of the operating system make it accessible to students and anyone else wanting to dabble in the field of 2D animation.

Personally, I do also prefer it over Rive for the time being because of its sleek UI and steady improvements. Rive has become quite loud recently, trying to build hype around every update and new feature, even though they muddle the focus of the application in my opinion. Fable, on the other hand, is like a good story: surprising, but comfortable. Calming, trustworthy, yet exciting at the same time without needing to shout about it.

If you haven’t yet, and you’re interested in animation, I strongly recommend you give Fable a try at [fable.app](https://fable.app)! As always, thank you for reading! If you have any questions or comments about Fable or anything else, feel free to reach out to me over on [Mastodon](https://mastodon.design/@amxmln). I’ll be back with another article next month.

---

*I am not associated with Fable in any way or form and was not asked to write this article, nor did I receive compensation for doing so.*
