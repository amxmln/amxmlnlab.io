---
title: '2023: A Year in Review'
tags:
  - year in review
  - '2023'
  - personal
blurb: >-
  2023 was the year I wanted to consume less and create more—here’s how that
  went and where I see things going in 2024.
coverImage: null
date: '2023-12-29T17:00:00+01:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
I started out 2023 with the intention of consuming less and creating more. Looking back now, I’m not sure how well I managed to turn that intention into reality. I feel like I hardly created anything this year—there was the release of [Ka-Ching](https://ka-ching.amxmln.com) in early January and that was it—however it’s probably not as simple as that, because there was a lot more going on.

## Freelancing for Real

Shortly after releasing Ka-Ching, I started work on the biggest freelance project I’ve done to date. It was a complete Website for a pretty large company that had been designed by an agency a friend of mine was working at, and she had asked me if I was open to implementing her designs. It seemed like a great opportunity, so I agreed.

Building it was fun and the client great to work with—but adding another two hours after a full work day *and* sacrificing some of my Saturdays was a challenge and didn’t leave much room for personal projects. Nonetheless, [the site](https://papp-logistics.de/en/) was released on schedule and positively received by everyone involved. I have since been hired to do a couple of small updates, but with a much smaller scope.

Looking back, I’d say the most complex parts of the website are the fact that its content exists in three languages, it has a dynamic full-screen video on the home page and three different types of contact forms, which need to be able to send not only text, but also binary uploads to a custom backend.

## Helping out my Sister

This year, my sister started her own business as a veterinary practitioner and nutritionist and I agreed to help her out by designing a logo, building a website and creating some business cards around Easter.

Working with and for family can be extremely frustrating and exhausting, which I learned the hard way. While I was pretty happy with [the result](https://erbicura.pet/), the already strained relationship between my sister and me was stretched to the point of breaking, leading to hour long phone calls and draining inner disputes of whether to just throw the towel or not.

In the end, we made up, and I hope our relationship is stronger for it—but especially now in the last weeks of this year, I notice how much energy and creativity that process cost me.

## Creating a(nother) Portfolio

Summer came with a nomination for a German Design Award: Newcomer award, which required me to create and submit a PDF-portfolio to be surveyed by a jury. Another task that took up most of my evenings after work, because I wanted to make sure to use this incredible opportunity to its fullest.

Creating portfolios has always been [challenging for me](/blog/2023/creating-portfolios/), but I had a pretty good feeling about this one—however, unfortunately, it apparently wasn’t good enough for me to make it into the final round. Nonetheless, I’m grateful for this honour and opportunity! It makes me feel recognised as a designer and motivates me to keep growing.

## Mattrbld and Other Updates

I will write a more detailed post about it on the [Mattrbld](https://mattrbld.com) website, but between work and my other projects, I was also able to finally finish the documentation for Mattrbld and release a bunch of smaller quality of life updates for my custom content management system.

Around May, I also transferred [Untold Stories](/projects/2019/untold-stories/) to a new host and got to implement some much-needed improvements to the social network. [Hydrt](/projects/2022/hydrt/) also saw a small upgrade in summer.

I had a lot of work to do in September because of a pitch and November and December were crazy because of the [Xmas-experience](https://flelfen-helfen.de) we built at work, so I had to work 10 to 11 hours a day, eating up pretty much all the energy I had.

## Teaching Again

Despite that, however, I also took on a teaching position at my old university. So I spent late summer and early autumn revising the slides of my introductory class on web design and built a new example website from scratch for my students, before starting to teach in October.

I have already taught this class before in 2020 / 2021, but this year it was the first time I taught it in person, which was a challenge in itself and came with a commute, something I’m not quite used to any more now that I work primarily from home. When I was still a student, I never minded the train rides to uni, but with such a packed schedule, those half-hour trips there and back again suddenly meant yet another hour missing in my day.

## Feeling Exhausted

There never seemed to be enough time for my personal projects this year, and even when there was, I felt too exhausted to work on them. Sure, I did manage to design a couple of new screens for Qami X here and there, and I did some research into how I could implement a [decentralised syncing solution](/blog/2023/a-generic-sync-server/) for my applications, but I do feel like I didn’t get anything done this year.

Especially since the start of winter, this has been weighing heavily on me and I can feel my mental health taking a beating from all the stress. I’m overly critical of everything I do and often struggle to find the motivation and purpose in doing anything just for myself and the joy of doing it.

I did manage to stop watching TV while eating dinner, but I still ended up in front of the screen after because I simply had no energy left after work. So did I manage to create more and consume less? Honestly, despite the list of achievements I’ve just written, it doesn’t feel like it.

The year started out well and with the best intentions. I was reading a lot more again, which made me happy, but somehow I ended up taking on too many different responsibilities and tried to make the most of too many good opportunities. As soon as the stress became too much, the good intentions broke down. I felt under the weather for a long time, but kept pushing, which meant that I stopped exercising regularly and this guilty feeling of *not doing enough* started settling in.

## Looking into 2024

This is something I definitely want to turn around in 2024. I need to find out where this deep unhappiness I’m feeling is rooted and try and work it out. My university class is going to end in January and I haven’t lined up any new projects outside of work yet, so I want to use the time to do just that and hopefully create a better work-life-balance in the process.

I want to keep reading more, because it helps me wind down much more than watching a sub-par series on a streaming service—and yes, I want to create more things I can be proud of again. But I also want to have fun while doing so and not have them be another source of stress. I’ve been thinking a lot about what it means to create apps and experiences on the side, while also working two or three jobs at the same time, and I might pack those thoughts into their own article.

For now, though, I want to wish all of you a happy new year. May all your wishes, plans, hopes and dreams come true! Thank you for the time you spend reading my articles, I’ll be back with more next year. Until then, take care! :blush:
