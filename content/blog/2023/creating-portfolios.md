---
title: Creating Portfolios
tags:
  - design
  - portfolio
  - presentation
blurb: >-
  Creating a portfolio is hard—even harder when it’s a static PDF as opposed to
  a website. Here are some of my thoughts on the struggle.
coverImage: null
date: '2023-07-31T20:30:00+02:00'
edited: null
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
If there’s one thing I struggle with in design, it’s actually curating and presenting the bulk of my work as a portfolio. It seems to be something that other designers struggle with as well, as I just witnessed once again during the year-long process of rebuilding the website of the agency I work at—and which finally [soft-launched](https://flavour-kommunikation.de) earlier this month!

There’s a lot of content out there dealing with how to collect and structure your work into a compelling portfolio. There’s lots of emphasis on keeping the reader’s attention, steering them to some sort of final goal, but in my experience, it’s usually the same advice over and over:

> -   Don’t show all your work, even though you might want to
>     
> -   Keep it concise, start strong and end on the best work
>     
> -   Make sure to express your intention: do you want to get hired? Or are you just documenting what you worked on in the past?
>     
> -   Clearly show what your contributions were in team-projects
>     
> -   Be detailed enough, but don’t waste the reader’s time
>     
> -   Adapt the portfolio to your audience
>     

While this certainly isn’t bad advice, it’s not what I struggle with, personally.

## My Problem with Portfolios

What I struggle with most is actually preparing the graphics for my portfolio. It’s hard enough on my website, where at least I can add some interactive elements to try and convey a hint of the final experience, but it gets outright impossible when I have to hand in a PDF somewhere, like I did a week ago.

How am I supposed to give enough context for an arbitrary person looking at my work to understand that those pretty images are only snapshots of a larger experience? How do I convey motion and *feel* on a static PDF page that might not even be looked at on the screen size it was made for?

Most “cool” portfolios I see these days are black rectangles with minimal white text and expansive device mock-ups, which actually show very little of the work that is meant to be the focus. While they certainly look impressive at times, I often wonder if all that fluff is meant to distract from the work itself. A bit like the many marketing websites for new hip software projects now in early access™️.

So what I tend to do is show my work as rectangles with rounded corners, sometimes with some slight borders or shadows to hint at the fact that this may be running on a phone or a tablet, or within a browser. It works, it shows my work—but it doesn’t exactly look fancy. It won’t impress anyone, at least that’s what I think.

## A Question of Purpose

In the end, it’s probably like with most other user-facing *things*: the target audience should influence the presentation. What do they want to see? What do I want them to take away? How can I grab their attention?

I find it a little bit sad that fancy graphics somehow count for more than content (which in my case is fancy UI graphics). I believe I still have a lot to learn about how to present what I do, be it digitally or in PDF form. For some reason, I always seem to get stuck when trying to, however.

Perhaps it’s some sort of mental block because I’m overthinking things. Making a portfolio seems to be prone to that. Your own work is deeply personal and has a lot of meaning, and I think a lot of designers would want to present their work in the most perfect way possible—which is probably also why rebuilding a portfolio website (or agency website) is such a long-winded and daunting task in my experience.

## Practice Makes Perfect

Working on my PDF portfolio for the past month made me realise how outdated my work on this website is—how outdated the website is as a whole! One of my biggest projects, [Mattrbld](https://mattrbld.com), isn’t even shown, and the tech stack is slowly dying under it (bye bye Gridsome 😢).

I think it’s time for a rebuild, coupled with a slight redesign, to bring things back up to snuff. This iteration of my website is three and a half years old, after all. I’m happy how things held up, but I think I can do better—and it will once again be a way for myself to practice presenting my work in a fun, engaging and ultimately informative manner.

Do you also struggle with creating portfolios? What are your strategies when presenting your work? I’d love to know! As always, you can reach out to me on [Mastodon](https://mastodon.design/@amxmln). Thank you for reading!
