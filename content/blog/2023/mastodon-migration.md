---
title: 'Mastodon Migration, Among Other Things'
tags:
  - social media
  - mastodon
blurb: >-
  I moved Mastodon instance at the end of last year. Here’s some thoughts on my
  experience, as well as a couple of little other things that have already
  happened in 2023.
coverImage: null
date: '2023-01-18T21:15:00+01:00'
edited: '2023-01-18T21:18:22+01:00'
published: true
___mb_schema: /.mattrbld/schemas/Blog-Post.json
---
Twitter is a drama that just keeps giving, isn’t it? In this month’s issue: third-party clients blocked for no reason. Or, well, perhaps because they don’t make Twitter any money because they don’t show Twitter’s ads and don’t mess with people’s timelines? We’ll probably never know. What seems pretty sure, at least as I’m writing this, is that this was no accident. No spam-prevention script gone rogue. There was no communication from Twitter’s end, and the issue lasted for more than 24 hours—with the official status page showing everything working fine.

Well, this has led to some developers of pretty big third-party apps to put up a notice for their upcoming Mastodon apps, which I find hilarious—in the best sense of the word. Conveniently, this also functions as a pretty neat segue to the fact that I switched Mastodon instance for the first time at the end of last year.

## Why Switch?

I initially signed up for mastodon.social because at the time I knew next to nothing about the Fediverse, and it seemed smart to sign up for the “official” server since it wasn’t likely to disappear any time soon and would get the most recent software updates in a timely manner. At first, it was great! There was a decent amount of people around and for the first time in a while, I actually felt *heard* on social media. Then things started going even more awry over on Twitter and the first big user influxes happened, slowing everything down to a crawl.

Many people thought like me and picked one of the official instances, so they soon buckled under the load and the local and federated timelines I liked to watch became too hard to follow. I wasn’t too bothered by it, but then I noticed that engagement with my Toots was lowering and learned that some of the big instances had been blocked because of their lax moderation policies. Some of those instances had people on them that I wanted to interact with—so it became time to look for an alternative.

## A New Home

Mastodon.design seemed like an obvious choice, since I do tend to follow mostly design and art related accounts and definitely don’t post enough art to be on mastodon.art. I had to apply for an account there by contacting the admin, which is something I like, it combats spam and is another hurdle for trolls to get past. My application was approved relatively quickly, and suddenly, I had to actually migrate my account.

I love that Mastodon has a built-in feature to switch instances—that’s awesome. All I had to do was go to the settings, mark my old account as the one to be transferred from and the new one as the one to be transferred to, confirm that I really wanted to do so, and the progress got kicked off in the background. I knew I wouldn’t be able to take any of my old posts with me, so I exported them for safekeeping and requested a backup of my media files, which took a while to reach me, but eventually did.

I also thought I’d lose the old account and somebody would be able to sign up using my username, but no, since the account knows I transferred servers, it went into a locked state and points users to my new instance instead! Very clever.

## After the Switch

As with everything on Mastodon, the whole interaction felt a bit clunky and rough around the edges, but all in all, it went really smoothly. I liked how welcoming my new instance was and that it even greeted me with a little note by the server admin on recent server stats. I felt more at home amongst so many other designers and almost immediately noticed a slight uptick in engagement.

Other than that and a much friendlier and more interesting local timeline, nothing changed about my interaction with Mastodon. I didn’t lose any followers during my move, still saw all the Toots by the people I followed.

I feel like that’s very important because I really like the concept of seeing different Mastodon instances not as different servers, which obviously they technically are, but as different communities. When signing up for Mastodon, you don’t choose your server, you choose the community you primarily want to be part of—and because the Fediverse is awesome, you get to interact with all the other communities as well as a bonus on top!

If you’ve been wanting to switch communities, definitely give it a shot! You will not regret it—and even if you should, a new one is just one switch away. 😉

## Elk for Mastodon

Speaking of giving things a shot, [Elk](https://elk.zone) is a new client for Mastodon built in Vue.js that’s been gaining some traction lately. It has a very Twitter-esque design that may appeal to some recent switchers, and in general feels very polished. I’ve been using it as my desktop client and have been very happy with it so far.

## A New App and an RSS Feed for the Blog

In other news (and in line with my resolution to consume less and create more), I’ve recently published a little game that you can use to test and train your ability to do calculations in your head. Perhaps it’ll also make you appreciate people working at cash registers more. I’m certainly very impressed by the work they do! [You can play it for free here](https://ka-ching.amxmln.com).

And last, but not least, if you’re an avid reader of my blog or just like to keep up with what I’m up to, I’ve added an [RSS feed](https://amxmln.com/rss.xml), so you can follow it with your favourite reader-app. It’s the first time I’m working with RSS, so things might be a little wonky still. Please let me know if I can improve anything.

As always, thank you for your time and don’t hesitate to reach out if you have any questions or comments! 😊
